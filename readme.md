
$doc:rst miRdf.js

A collection of libraries to create dynamic web pages based on [RDF](#rdf).

The libraries are made in typescript. Javascript builds can be found
under the `build/` folder.


miRdfa template engine
----------------------

[miRdfa](doc/md/miRdfa.md) is a template engine to create dynamic html pages based
on templates with tag attributes similar to rdfa. The library converts
[RDF](#rdf) to an hierarchical html page. The intention is to be able to
use the same data format the whole way from server data storage to
client template engine.

 * `build/miRdfa.js`


miRdfRel data storage
---------------------

[miRdfRel](doc/md/miRdfRel.md) is a library to store data in [RDF](#rdf)
on a client. It is heavily used by [miRdfa](doc/md/miRdfa.md).

You can use it to store and retrieve resources and their relations and
properties. It has a query language to follow multiple relations and
filter on properties. And it is possible to add watchers for changes
that trigger when matching data is added or removed.

 * `build/miRdfRel.js`


miMirdf data format
-------------------

[miMirdf](doc/md/miMirdf.md) is a file/data format to store or transfer data in
rdf.

It can be used to store a graph, or express changes or queries.

   * `build/miMirdf3.js`


RDF
---

RDF is a format based on tripplets and their relations, subject,
predicate, object. Please check out `https://www.w3.org/RDF/`.

Subject and predicate is each a resource, a unique identifier. Object,
may be a resource or a value of some kind (these libraries use only
strings).

The resources are placed in namespaces ad looks like
`http://www.w3.org/2000/01/rdf-schema#label`. Common practice is
to replace the namespace with a prefix, in this case `rdfs:label`.

The resources in these libraries are always defined as prefixed entries.

There are standard namespaces (rdf, rdfs, dcp, etc) trying to
standardise definitions so it is possible to join different data sets.
Check f.ex. <https://schema.org/>.

```
mi:Martin
    a rdfs:Class
    rdfs:label \"Martin
    foaf:knows mi:Agne

mi:Agnes
  a rdfs:Class
  rdfs:label "Agnes
```

Ideas
-----
Under [Ideas](doc/md/Ideas.md) you can find thoughs of improvements, possible issues and how to fix them.
