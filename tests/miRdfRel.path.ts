/// <reference path="test.ts" />
/// <reference path="../miRdfRel/miRdfRel.ts" />

/* */
test.run('A Test paths', function(){

    let aIn:string = '/some:res/some:rel/some:filter='
    let aPath:miRdfRel.path = miRdfRel.pathFromString(aIn);
    let aOut:string = miRdfRel.pathToString(aPath)
    test.eq(aIn, aOut)

    let bIn:string = '/some:res'
    let bPath:miRdfRel.path = miRdfRel.pathFromString(bIn);
    let bOut:string = miRdfRel.pathToString(bPath)
    test.eq(bIn, bOut)

    let cIn:string = 'some:res==a/prop/'
    let cPath:miRdfRel.path = miRdfRel.pathFromString(cIn);
    let cOut:string = miRdfRel.pathToString(cPath)
    test.eq(cIn, cOut)

    let dIn:string = 'prop='
    let dPath:miRdfRel.path = miRdfRel.pathFromString(dIn);
    let dOut:string = miRdfRel.pathToString(dPath)
    test.eq(dIn, dOut)

    let eIn:string = '-some:thing/some--less/some++more/-other'
    let ePath:miRdfRel.path = miRdfRel.pathFromString(eIn);
    let eOut:string = miRdfRel.pathToString(ePath)
    test.eq(eIn, eOut)
})
/* */
test.run('B Watch absolute resource path', function(){

    let s1 = miRdfRel.getAddRes('B:s1')
    let p2 = miRdfRel.getAddRes('B:p2')
    let s3 = miRdfRel.getAddRes('B:s3')
    let p4 = miRdfRel.getAddRes('B:p4')

    let u1 = miRdfRel.watchPath('/B:s1', test.triggerAdd)
    test.triggered([s1], [])
    u1 && u1()

    miRdfRel.watchPath('/B:s1/B:p2/', test.triggerAdd)
    test.triggered([], [])

    s1.addRelTo(p2, s3)
    test.triggered([s3], [])

    miRdfRel.watchPath('/B:s3/-B:p2/', test.triggerAdd)
    test.triggered([s1], [])

    miRdfRel.watchPath('/B:s3/-B:p4/', test.triggerAdd)
    test.triggered([], [])

    s1.addRelTo(p4, s3)
    test.triggered([s1], [])

    s1.rmRelTo(p2, s3)
    test.triggered([], [s1, s3])
})
/* */ /* */
test.run('C Watch relative path', function(){

    let s1 = miRdfRel.getAddRes('C:s1')
    let p2 = miRdfRel.getAddRes('C:p2')
    let s3 = miRdfRel.getAddRes('C:s3')

    s1.watchPath('C:p2/', test.triggerAdd)
    test.triggered([], [])
    s1.addRelTo(p2, s3)
    s1.rmRelTo(p2, s3)
    test.triggered([s3], [s3])

    s1.watchPath('C:p2=', test.triggerAdd)
    test.triggered([], [])
    s1.addProp(p2, 'test')
    s1.rmProp(p2, 'test')
    test.triggered(['test'], ['test'])
})
/* */ /* */
test.run('D Watch filter then props', function(){

    let s1 = miRdfRel.getAddRes('D:s1')
    let p2 = miRdfRel.getAddRes('D:p2')

    let u2 = miRdfRel.watchPath('/D:s1/D:p2==ja1/D:p2', test.triggerAdd)
    s1.addProp(p2, 'ja1')
    test.triggered(['ja1'], [])
    s1.addProp(p2, 'ja2')
    test.triggered(['ja2'], [])
    s1.rmProp(p2, 'ja1')
    test.triggered([], ['ja1', 'ja2'])
    u2 && u2()

})
/* */ /* */
test.run('E path collapse relations', function(){

    let s1 = miRdfRel.getAddRes('E:s1')
    let p2 = miRdfRel.getAddRes('E:p2')
    let s3 = miRdfRel.getAddRes('E:s3')
    let p4 = miRdfRel.getAddRes('E:p4')
    let s5 = miRdfRel.getAddRes('E:s5')

    let u1 = miRdfRel.watchPath('/E:s1/E:p2/E:p4/', test.triggerAdd)
    s1.addRelTo(p2, s3)
    s3.addRelTo(p4, s5)
    test.triggered([s5], [])
    s1.rmRelTo(p2, s3)
    test.triggered([], [s5])
    u1 && u1()

})
/* */ /* */
test.run('F path collapse filter', function(){

    let root = miRdfRel.getAddRes('F:root')
    let child = miRdfRel.getAddRes('F:child')
    let c1 = miRdfRel.getAddRes('F:child1')
    let c2 = miRdfRel.getAddRes('F:child2')
    let val = miRdfRel.getAddRes('F:val')

    let u1 = miRdfRel.watchPath('/F:root/F:child/F:val++1', test.triggerAdd)
    root.addRelTo(child, c1)
    root.addRelTo(child, c2)
    c2.addProp(val, "2")
    test.triggered([c2], [])
    c1.addProp(val, "2")
    test.triggered([c1], [])
    c2.rmProp(val, "2")
    c2.addProp(val, "2")
    test.triggered([c2], [c2])
    c2.rmProp(val, "2")
    test.triggered([], [c2])
    c1.rmProp(val, "2")
    test.triggered([], [c1])
    u1 && u1()

})
/* */ /* */
test.run('G Watch same relative path', function(){

    let s1 = miRdfRel.getAddRes('G:s1')
    let p2 = miRdfRel.getAddRes('G:p2')

    let u1 = s1.watchPath('G:p2==A', test.triggerAdd)
    let u2 = s1.watchPath('G:p2==A', test.triggerAdd)
    test.triggered([], [])
    s1.addProp(p2, 'A')
    test.triggered([s1, s1], [])

    u1 && u1()
    test.triggered([], [])
    let u3 = s1.watchPath('G:p2==A', test.triggerAdd)
    test.triggered([s1], [])

    let u4 = s1.watchPath('G:p2==A', test.triggerAdd)
    test.triggered([s1], [])

    s1.rmProp(p2, 'A')
    test.triggered([], [s1, s1, s1])

    u2 && u2()
    u3 && u3()

    s1.addProp(p2, 'A')
    test.triggered([s1], [])
    u4 && u4()
    s1.rmProp(p2, 'A')
    test.triggered([], [])
})
/* */ /* */
test.run('H Watch only props', function(){

    let s1 = miRdfRel.getAddRes('H:s1')
    let p2 = miRdfRel.getAddRes('H:p2')
    let s3 = miRdfRel.getAddRes('H:s3')

    let u1 = s1.watchPath('H:p2=', test.triggerAdd)
    let u2 = s1.watchPath('H:p2=', test.triggerAdd)
    s1.addRelTo(p2, s3)
    test.triggered([], [])
    s1.addProp(p2, 'test')
    test.triggered(['test', 'test'], [])

    let u3 = s1.watchPath('H:p2=', test.triggerAdd)
    test.triggered(['test'], [])

    s1.rmProp(p2)
    test.triggered([], ['test', 'test', 'test'])

    s1.addProp(p2, 'test2')
    test.triggered(['test2', 'test2', 'test2'], [])
    s1.addProp(p2, 'test')
    test.triggered(['test', 'test', 'test'], [])

    s1.rmProp(p2)
    test.triggered([], ['test2', 'test2', 'test2', 'test', 'test', 'test'])

    u1 && u1()
    u2 && u2()
    u3 && u3()
})
/* */
