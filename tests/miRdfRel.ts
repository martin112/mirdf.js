/// <reference path="test.ts" />
/// <reference path="../miRdfRel/miRdfRel.ts" />


test.run('miRdfRel get resource', () => {

    test.eq(miRdfRel.getRes('A:s1'), undefined)

    let s1 = miRdfRel.getAddRes('A:s1')
    test.eq(miRdfRel.getRes('A:s1'), s1)
    test.eq(miRdfRel.getRes(s1), s1)

    let s2 = miRdfRel.getAddRes('A:s2')
    test.eq(miRdfRel.getRes('A:s2'), s2)

    test.eq(miRdfRel.getRes('A:s3'), undefined)

    s1.rm()
    test.eq(miRdfRel.getRes('A:s1'), undefined)
    test.eq(miRdfRel.getRes('A:s2'), s2)
    test.eq(miRdfRel.getRes('A:s3'), undefined)
    s2.rm()
})

test.run('miRdfRel watch add remove resource', function(){

    miRdfRel.watchRes('r2:a', test.triggerAdd)
    let resa = miRdfRel.getAddRes('r2:a')
    test.triggered([resa], [])

    let resb = miRdfRel.getAddRes('r2:b')
    test.triggered([], [])

    miRdfRel.watchRes('r2:b', test.triggerAdd)
    test.triggered([resb], [])

    let resaName = resa.name
    resa.rm()
    test.triggered([], [resa])
    test.eq(miRdfRel.getRes(resaName), undefined)
})

test.run('miRdfRel replace resources', function(){

    let r3 = miRdfRel.getAddRes('r3:')
    let rel = miRdfRel.getAddRes('rel')
    let r3a = miRdfRel.getAddRes('r3:a')
    let r3b = miRdfRel.getAddRes('r3:b')
    let r3c = miRdfRel.getAddRes('r3:c')

    r3.addRelTo(rel, r3a)
    r3.addRelTo(rel, r3b)
    test.eq(r3.relTo(rel), new Set([r3a, r3b]))
    r3.replaceRelTo(rel, r3c)
    test.eq(r3.relTo(rel), new Set([r3c]))
    r3.replaceRelTo(rel, r3a)
    r3.replaceRelTo(rel, r3b)
    test.eq(r3.relTo(rel), new Set([r3b]))
    r3.rmRelTo(rel, r3b)
    test.eq(r3.relTo(rel), new Set([]))
    r3.rm()
    r3a.rm()
    r3b.rm()
})
