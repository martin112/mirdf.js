/// <reference path="test.ts" />
/// <reference path="../miRdfRel/miRdfRel.ts" />

test.run('A watchAddRelTo', () => {
    let s1 = miRdfRel.getAddRes('A:s1')
    let p2 = miRdfRel.getAddRes('A:p2')
    let p3 = miRdfRel.getAddRes('A:p3')
    let s4 = miRdfRel.getAddRes('A:s4')
    let s5 = miRdfRel.getAddRes('A:s5')

    let unwatch = s1.watchAddRelTo(p2, test.triggerAdd)
    test.triggered([], [])
    s1.addRelTo(p2, s4)
    test.triggered([s4], [])
    s1.addRelTo(p2, s5)
    test.triggered([s5], [])
    s1.rmRelTo(p2, s5)
    test.triggered([], [s5])
    s1.rmRelTo(p2, s4)
    test.triggered([], [s4])
    unwatch()
    s1.addRelTo(p2, s4)
    test.triggered([], [])
})

test.run('B watchAddRelFrom', () => {
    let s1 = miRdfRel.getAddRes('B:s1')
    let p2 = miRdfRel.getAddRes('B:p2')
    let s3 = miRdfRel.getAddRes('B:s3')
    let p4 = miRdfRel.getAddRes('B:p4')
    s1.watchAddRelFrom(p2, test.triggerAdd)
    s3.watchAddRelFrom(p2, test.triggerAdd)
    s1.addRelTo(p2, s3)
    test.triggered([s1], [])
    s1.addRelTo(p4, s3)
    test.triggered([], [])
    s1.rmRelTo(p2)
    test.triggered([], [s1])

})

test.run('C watchAmpersand', () => {
    // res > & > ( any relation )
    // + res > x > y trigger
    // + y > x-1 > res trigger
    let s1 = miRdfRel.getAddRes('C:s1')
    let p3 = miRdfRel.getAddRes('C:p3')
    let p4 = miRdfRel.getAddRes('C:p4')
    let s5 = miRdfRel.getAddRes('C:s5')
    let s6 = miRdfRel.getAddRes('C:s6')
    p3.addRelTo(miRdfRel.getAddRes('owl:inverseOf'), p4)
    s1.watchAddRelTo('&', test.triggerAdd)
    s1.addRelTo(p3, s5)
    test.triggered([s5], [])
    s1.addRelTo(p3, s6)
    test.triggered([s6], [])
    s1.rmRelTo(p3)
    test.triggered([], [s5, s6])
    s6.addRelTo(p3, s1)
    s6.addRelTo(p4, s1)
    test.triggered([s6], [])
})
