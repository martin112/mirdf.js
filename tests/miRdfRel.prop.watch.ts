/// <reference path="test.ts" />
/// <reference path="../miRdfRel/miRdfRel.ts" />

test.run('A watching a property', () => {

    let s1 = miRdfRel.getAddRes('A:s1')
    let p2 = miRdfRel.getAddRes('A:p2')

    s1.addProp(p2, 'test 1')
    s1.addProp(p2, 'test 2')

    s1.watchProp(p2, test.triggerAdd)
    test.triggered(['test 1', 'test 2'], [])

    s1.rmProp(p2)
    test.triggered([], ['test 1', 'test 2'])

    s1.addProp(p2, 'test 3')
    s1.addProp(p2, 'test 3')
    test.triggered(['test 3'], [])
    s1.rmProp(p2, 'test 3')
    test.triggered([], ['test 3'])
})

test.run('B watching resource with a property filter', () => {

    let s1 = miRdfRel.getAddRes('B:s1')

    s1.watchPropFilter('B:p2', '+=', '4', test.triggerAdd)

    let p2 = miRdfRel.getAddRes('B:p2')

    s1.addProp(p2, '4')
    test.triggered([s1], [])

    s1.addProp(p2, '5')
    test.triggered([], [])

    s1.addProp(p2, '3')
    test.triggered([], [s1])

    s1.rmProp(p2, '3')
    test.triggered([s1], [])

    s1.rmProp(p2, '5')
    test.triggered([], [])

    s1.rmProp(p2)
    test.triggered([], [s1])

})

test.run('C watch same prop multiple times', () => {

    let s1 = miRdfRel.getAddRes('C:s1')
    let p2 = miRdfRel.getAddRes('C:p2')

    let u1 = s1.watchProp(p2, test.triggerAdd)

    test.triggered([], [])
    s1.addProp(p2, 'test 1')
    s1.addProp(p2, 'test 1')
    s1.addProp(p2, 'test 1')
    test.triggered(['test 1'], [])

    let u2 = s1.watchProp(p2, test.triggerAdd2)
    test.triggered(['test 1'], [])

    s1.rmProp(p2)
    test.triggered([], ['test 1', 'test 1'])

    u1()
    s1.addProp(p2, 'test 2')
    test.triggered(['test 2'], [])

    s1.rmProp(p2)
    test.triggered([], ['test 2'])
    u2()
})
