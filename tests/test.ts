/* helpers to create test cases */

namespace test {
    let _testnr = 0
    let _tests: number
    let _failed: number
    let triggeredAdd: any[]
    let triggeredRm: any[]

    export function str(obj:any) {
        if (obj === undefined) {
            return 'undef'
        }
        if (obj === false) {
            return 'false'
        }
        if (obj === true) {
            return 'true'
        }
        if (obj instanceof Set) {
            let out = '{ '
            for (let v of obj) {
                out += str(v) + ','
            }
            return out + ' }'
        }
        if (obj instanceof Array) {
            let out = '[ '
            for (let v of obj) {
                out += str(v) + ','
            }
            return out + ']'
        }
        if (obj.name) {
            return '<' + obj.name + '>'
        }
        if (obj.constructor == Object) {
            let out = '{ '
            for (let v in obj) {
                out += `${v}:${str(obj[v])}; `
            }
            return out + '}'
        }
        return String(obj)
    }

    function _cmp<t>(a:t, b:t) {
        return str(a) == str(b)
    }

    export function run(testName:string, test: ()=>void): boolean {
        ++ _testnr
        _tests = 0
        _failed = 0
        triggeredAdd = []
        triggeredRm = []

        console.group(_testnr, testName)
        test()
        if (_failed > 0) {
            console.error(`__ ${_failed} of ${_tests} tests FAIL __`)
            console.groupEnd()
            return false
        } else {
            console.info(`__ all ${_tests} tests pass __`)
            console.groupEnd()
            return true
        }
    }

    export function eq<t> (val:t, shouldBe:t) {
        ++ _tests
        if (_cmp(val, shouldBe)) {
            // console.info('OK')
        } else {
            ++ _failed
            console.error(`${_tests})`, str(val), '!', str(shouldBe))
        }
    }
 
    export function triggered(add:any[], rm:any[]) {
        ++ _tests
        let addPass = _cmp(triggeredAdd, add)
        let rmPass = _cmp(triggeredRm, rm)
        if (addPass && rmPass) {
            // console.info('OK')
        } else {
            ++ _failed
            let err = [`${_tests})`]
            if (!addPass) {
                err.push('add', str(triggeredAdd), str(add))
            }
            if (!rmPass) {
                err.push('rm', str(triggeredRm), str(rm))
            }
            console.error.apply(false, err)
        }
        triggeredAdd = []
        triggeredRm = []
    }

    export function triggerAdd<matchT>(val:matchT) {
        // console.debug('triggerAdd', val)
        triggeredAdd.push(val)
        return function() { // triggerRm
            // console.debug('triggerRm', val)
            triggeredRm.push(val)
        }
    }
    export function triggerAdd2<matchT>(val:matchT) {
        // same as triggerAdd, but makes it possible to do two similar calls
        // console.debug('triggerAdd', val)
        triggeredAdd.push(val)
        return function() { // triggerRm
            // console.debug('triggerRm', val)
            triggeredRm.push(val)
        }
    }

}
