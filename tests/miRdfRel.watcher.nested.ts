/// <reference path="test.ts" />
/// <reference path="../miRdfRel/miRdfRel.ts" />

test.run('A Test for watchPath triggering wrong triggers', () => {
    /* there was an issue that adding a watcher to something
       could trigger previous watchers too */

    let s1 = miRdfRel.getAddRes('A:s1')
    
    miRdfRel.watchRes('A:s1', function(m) {
        miRdfRel.watchRes('A:s1', function(m) {
            return function() { console.debug('???') }
        })
        return function() { console.debug('???') }
    })

    let p2 = miRdfRel.getAddRes('A:p2')

    s1.addProp(p2, 'test')

    s1.watchProp(p2, function(m) {
        s1.watchProp(p2, function(m) {
            return function() { console.debug('???') }
        })
        return function() { console.debug('???') }
    })

})
