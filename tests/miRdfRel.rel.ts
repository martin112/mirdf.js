/// <reference path="test.ts" />
/// <reference path="../miRdfRel/miRdfRel.ts" />
// tsc watchers.testprop.ts -t es2017 --module amd --outfile watchers.testprop.js;node watchers.testprop.js

test.run('A simple relations', () => {

    let A = miRdfRel.getAddRes('a')
    let s1 = miRdfRel.getAddRes('A:s1')
    let p2 = miRdfRel.getAddRes('A:p2')
    let s3 = miRdfRel.getAddRes('A:s3')
    let s4 = miRdfRel.getAddRes('A:s4')
    let C5 = miRdfRel.getAddRes('A:C5')
    s1.addRelTo(A, C5)
    s1.addRelTo(p2, s3)
    s1.addRelTo(p2, s4)
    s3.addRelTo(p2, s4)

    test.eq(C5.relFrom(A), new Set([s1]))
    test.eq(s1.relTo(p2), new Set([s3, s4]))
    test.eq(s3.relTo(p2), new Set([s4]))
    test.eq(s1.relFrom(p2), new Set([]))
    test.eq(s3.relFrom(p2), new Set([s1]))
    test.eq(s4.relFrom(p2), new Set([s1, s3]))
})

let owl_inverseOf = miRdfRel.getAddRes('owl:inverseOf')

test.run('B inverse relations', () => {
    let s1 = miRdfRel.getAddRes('B:s1')
    let s2 = miRdfRel.getAddRes('B:s2')
    let p4 = miRdfRel.getAddRes('B:p4')
    let p5 = miRdfRel.getAddRes('B:p5')
    p5.addRelTo(owl_inverseOf, p4)
    s1.addRelTo(p4, s2)

    // s1 relTo p4 match
    test.eq(s1.relTo(p4), new Set([s2]))
    test.eq(s1.relTo(p5), new Set())

    // s2 has no rel to p1
    test.eq(s2.relTo(p4), new Set([]))
    test.eq(s2.relTo(p5), new Set([s1]))

    // s1 is not related from anywhere
    test.eq(s1.relFrom(p4), new Set())
    test.eq(s1.relFrom(p5), new Set([s2]))

    // s2 is related from s1 via s4
    test.eq(s2.relFrom(p4), new Set([s1]))
    test.eq(s2.relFrom(p5), new Set())
})
