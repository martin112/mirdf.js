/// <reference path="test.ts" />
/// <reference path="../miRdfRel/watcher.ts" />

test.run('watcher value', function(){

    type matcher = { m:string, v:string }
    let value = '---'

    let watchValue = watcher.newWatcherGroup<string,matcher,matcher>(
        function(_change, watcherValue) {
            if (value == watcherValue.m) {
                // matching, return true, key for the match, and what to send
                // console.debug('    ', 'true', watcherValue.m, watcherValue.v)
                return [true, watcherValue.m, watcherValue]
            } else {
                // else return false, key for the match
                // console.debug('    ', 'false', watcherValue.m, watcherValue.v)
                return [false, watcherValue.m]
            }
        },
    )
    function changeValue(newValue:string) {
        value = newValue
        watchValue.trigger(newValue)
    }
    let w1 = {m:'AAA', v:'1'}
    let unwatchCb_1 = watchValue.watchAdd(w1, test.triggerAdd)
    watchValue.trigger(value)

    changeValue('xxx')
    changeValue('BBB')
    test.triggered([], [])
    changeValue('AAA')
    test.triggered([w1], [])

    // Should trigger AAA 2 add')
    let w3 = {m:'BBB', v:'3'}
    let w2 = {m:'AAA', v:'2'}
    let unwatchCb_3 = watchValue.watchAdd(w3, test.triggerAdd)
    let unwatchCb_2 = watchValue.watchAdd(w2, test.triggerAdd)
    changeValue('AAA')
    test.triggered([w2], [])

    // Shouldnt trigger anything')
    changeValue('AAA')
    test.triggered([], [])

    // Should trigger AAA 1 rm, AAA 2 rm, BBB 3 add')
    changeValue('BBB')
    test.triggered([w3], [w1, w2])
    unwatchCb_1()

    // Should trigger AAA 2 add, BBB 3 rm')
    changeValue('AAA')
    test.triggered([w2], [w3])

    // dont trigger anything
    changeValue('AAA')
    unwatchCb_2()
    unwatchCb_3()
    changeValue('BBB')
    changeValue('AAA')
    test.triggered([], [])
})


test.run('watcher one to many', function() {

    type matcher = string
    let values: Set<string> = new Set()
    let watchValue = watcher.newWatcherGroup<string,matcher,string>(
        function(change, matcher) {
            // hm... here we need last added, so a key/change would be good
            if (values.has(change))
                return [true, change, change]
            else
                return [false, change]
        },
    )
    function add(value:string) {
        values.add(value)
        watchValue.trigger(value)
    }
    function remove(value:string) {
        values.delete(value)
        watchValue.trigger(value)
    }

    add('G')
    add('H')
    let unwatch = watchValue.watchAdd('unused', test.triggerAdd)
    values.forEach(v => watchValue.trigger(v))
    test.triggered(['G', 'H'], [])

    add('A')
    add('A')
    test.triggered(['A'], [])

    add('B')
    test.triggered(['B'], [])

    // trigger add C')
    add('C')
    test.triggered(['C'], [])

    // trigger rm A')
    remove('A')
    test.triggered([], ['A'])

    // trigger rm C')
    remove('C')
    test.triggered([], ['C'])

    unwatch()
    // trigger nothing')
    remove('B')
    test.triggered([], [])
    console.debug('   ', watchValue._set)
})



test.run('watcher something like prop filters', function() {

    type propVal = {
        prop: string
        val: string
    }
    type propMatcher = {
        prop:string
        val: string
        op: "=="|"!="|"++"
        matching: boolean
    }
    let props: Record<string, Set<string>> = {}

    function propFilter(propName:string, match:propMatcher) {
        switch (match.op) {
            case "==":
                return propName in props && props[propName].has(match.val)
            case "!=":
                return !(propName in props) || !props[propName].has(match.val)
            case "++":
                if (!(propName in props) || !props[propName].size) return false
                for (let value of props[propName]) {
                    if (value <= match.val) return false
                }
                return true
            default:
                console.warn('Unknown propMatch operator', match.op)
                break
        }
    }
    function propFilterMatch(change:propVal, match:propMatcher): watcher.matcherReturn<string> {
        if (change.prop != match.prop) return
        if (propFilter(change.prop, match)) return [true, change.prop, change.prop]
        return [false, change.prop]
    }

    let propWatch = watcher.newWatcherGroup<propVal,propMatcher,string>(propFilterMatch)


    function add(p:propVal) {
        if (!(p.prop in props)) props[p.prop] = new Set()
        props[p.prop].add(p.val)  // if p.val not in props[p.prop] add and trigger
        propWatch.trigger(p)
    }
    function remove(p:propVal) {
        if (p.prop in props) {
            props[p.prop].delete(p.val)
            propWatch.trigger(p)
        }
    }

    console.info('Test p1==x')

    propWatch.watchAdd({prop:'p1', val:'x',op:'==',matching:false}, test.triggerAdd)
    test.triggered([], [])

    // add p1 x : trigger add')
    let p1x = {prop:'p1', val:'x'}
    add(p1x)
    test.triggered(['p1'], [])

    // rm p1 x : trigger rm')
    remove(p1x)
    test.triggered([], ['p1'])

    
    console.info('Test p2++b')

    propWatch.watchAdd({prop:'p2', val:'b',op:'++',matching:false}, test.triggerAdd)

    // add p2 a
    let p2a = {prop:'p2', val:'a'}
    add(p2a)

    // add p2 c')
    let p2c = {prop:'p2', val:'c'}
    add(p2c)
    add({prop:'p2', val:'c'})
    test.triggered([], [])

    // rm p2 a : trigger add p2')
    remove(p2a)
    test.triggered(['p2'], [])

    // rm p2 c : trigger rm p2')
    remove(p2c)
    test.triggered([], ['p2'])

    console.debug(props)

})
