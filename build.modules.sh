#!/bin/sh
#
# build all libraries
# build to this build/module

mkdir -p "build/es2017"

if [ -n "$1" ]
then
    files="$1"
else
    files="
miRdfRel/miRdfRel.ts
miRdfa/miRdfa.ts
miRdFormat/miRdFormat3.ts
"
fi

for f in $files
do
    target="build/es2018/$(basename "${f%.*}").js"
    tsc "$f" -t es2018 --module amd --outfile "$target" --strictNullChecks --strictFunctionTypes --sourcemap
    # TODO remove all console.debug from target
    sed -E 's/console.(log|debug|info|...|count)\((.*)\);?//g' -i.debug.js "$target"
    echo ">>> $target"
done

