#!/usr/bin/python3
"""
Generate documentation for miRdf.js

Using Doc3n as framework. Most tags are in restructured text.

Output is doc.html/manual.mirdf
"""
import sys
import os
from subprocess import run
import logging
from pathlib import Path
from doc3n import TemporaryFolder
from doc3n.export import Exporter, stitch, to_pages
from doc3n.export.formats import markdown, html5
from doc3n.gather import Gather

logging.basicConfig(level=logging.INFO)

# change folder to where this script lies
os.chdir(Path(sys.argv[0]).parent or ".")

res = run(
    ["git", "log", "-1", "--format=%h %ci!%cI"],
    capture_output=True,
    check=True,
    encoding="utf-8",
)
version, date = res.stdout.strip().split("!", 1)

# --- maybe not correct
# use arg NOCACHE to skip cache
# use arg CLEARCACHE to clear cache

gather = Gather("all")

PROJECT_ROOT = "../"

gather.add_path(PROJECT_ROOT, "readme.rst")
gather.add_path(PROJECT_ROOT, "miRdfRel")
gather.add_path(PROJECT_ROOT, "miRdfa")
gather.add_path(PROJECT_ROOT, "mi.mirdf")
gather.add_path(PROJECT_ROOT, "ideas")

pages = to_pages(stitch(gather))

md_export = Exporter(pages, "md")
md_export.file_prefix = "../../"
md_export.formatter.output_css = "doc3n/example/resources/style1.css"
with TemporaryFolder(Path("md/")) as tmppath:
    md_export.export(tmppath)

html_export = Exporter(pages, "html5")
html_export.file_prefix = "../../"
html_export.include_source = True
html_export.formatter.output_css = "doc3n/example/resources/style1.css"
with TemporaryFolder(Path('html/')) as tmppath:
    html_export.export(tmppath)

# mirdf = export.Mirdf(
#    DOCUMENTS,
#    'mi.RDF.js',
#    prefix='miRdf.js:',
#    prefix_url='https://martin.insulander.info/mirdf/mirdf.js/',
#    source_url='https://martin.insulander.info/mirdf/mirdf.js/',
#    out_text_format='html5',
#    )
# mirdf.add_stmt(mirdf.prefix, 'schema:dateModified', date, False)
# mirdf.add_stmt(mirdf.prefix, 'schema:isPartOf', "https://martin.insulander.info/mirdf", False)
# mirdf.add_stmt(mirdf.prefix, 'schema:creator', "https://martin.insulander.info", False)
# mirdf.add_stmt(mirdf.prefix, 'schema:version', version, False)
# mirdf.write('mirdf/miRdf.mirdf')
