---
pagetitle: miRdfa technical
---

# miRdfa technical

[miRdfa/miRdfa.ts 59](miRdfa/miRdfa.ts#59){.source file application javascript}

Details on how miRdfa is implemented. Good for developing and debugging
miRdfa itself.

## Internal structure

[miRdfa/realisator.ts](miRdfa/realisator.ts){.source file text plain}

Under miRdfa, the instanses, realisations of the templates are placed.

miRdfa-instance.children\[\]

:   

    realisator

    :   

        match\[\]

        :   realisator/realisator group\...

    realisator group

    :   

        siblings\[\]

        :   

            realisator\[\]

            :   

                match\[\]

                :   realisator/realisator group\...

## Realisators

[miRdfa/realisator.ts 19](miRdfa/realisator.ts#19){.source file text plain}

The realisators are central to this library. They takes a template,
watch for any data matching it\'s query and on a match, tells where to
put the match. It also does selecting, sorting and limiting on groups.

There are different type of realisators, depending on their underlying
template, but they all have similar traits.

### Life cycle of realisators and their matches

Activate/deactivate and show/hide are both separated to limit nr of
redraws, check [when to draw - dirty flag](#when-to-draw---dirty-flag).

### Watching data

Realisators watch data in miRdfa by watching paths.

paths are parsed when the html is templified, showing any obvious error
directly.

Absolute paths are directly on miRdfa.watchParsedPath(), and for
relative paths, each resource is used, {resource}.watchParsedPath().

When watching anything, miRdfRel returns an unwatch callback which is
used to stop the monitoring when the realisastor is deactivated.

## Matches

## Nodes and html

[miRdfa/init.ts 807](miRdfa/init.ts#807){.source file text plain}

> From html nodes attributes are cut out and used to define the
> templates. The actual html node is then cut from the dom-tree and also
> put in it\'s template.
>
> # placeholder node
>
> To know where to add elements, html nodes are replaced with a dummy
> node, a meta-element. \*/
>
> /\*

### When to draw - dirty flag

[miRdfa/realisator.ts 45](miRdfa/realisator.ts#45){.source file text plain}

To limit nr of redraws, each realisator has a dirty flag to check if it
needs to be udpated. On a change, *show* is triggered on affected
realisator and trickles down to all it\'s children, and those with a
dirty flag set, will be reordered and redrawn.

## Template components
