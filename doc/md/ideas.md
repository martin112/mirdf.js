---
pagetitle: Ideas
---

# Ideas

[ideas/README](ideas/README){.source file text plain}

Thoughs of improvements, possible issues and how to fix them.

## Clone template

[ideas/clone templates](ideas/clone templates){.source file text plain}

Using a template on multiple places would reduce code duplicates and
also make it possible for tree views. The html template tag is hidden by
default which fits this purpose:

    <template id="{template id}">{content}</template>

    <article clone="{template id}"></article>

And for trees:

    <template id="{template id}">
        <div clone="{template id}" retracted>
    </template>

One issue is it has to be a separate template even if it is only used in
the same tree. If that is an issue, we could add a new attribute,
template_id={template id} working the same.

Internally reach it by:

    miRdfa.template[{template id}]

## Input handling

[ideas/input handling](ideas/input handling){.source file text plain}

This needs some thinking, but should be simplified.

template:

    <input property=smthng update>

resulting html:

    <input property=smthng.uniq/val onchange update uniq/val and depending on update-flag smthng/val

Should be stored in rel in a good way. Must be possible to get original
value, updated value and if it changed, from template queries and js.

In rel, maybe:

    resource
        property value
        property.input
            rdfs:value
            changed

    path('resource/property')
    path('resource/property.input/...changed')
    path('resource/property.input/rdfs:value')

## Manual init

[ideas/manualInit](ideas/manualInit){.source file text plain}

Remove the automatic miRdfa init and make it possible to add multiple
inits and do it dynamically:

    mirdfa = miRdfa(root)

Could this also make it possible to dynamically update templates

``` javascript
# rdfa-on-constructed and rdfa-on-deconstruct to be able to modify a template mostly for changing rdfa-rel-path for searches

function on_constructed(realisator) {
    view.realisator = realisator;
}

function on_search_from_date(e) {
    mi.rdfa.update_realisator_rel_path(':date>='+e.value);
}

function on_deconstruct(realisator) {
    view.realisator = undefined;
}

realisation.replaceChildTemplates(html)
```

## Prefill

[ideas/prefill](ideas/prefill){.source file text plain}

Could maybe have an argument allowing prefilled values and overwrite
them when loading template. Maybe **prefilled/skip/ignore/dummy**.

## Prefix in miRdfRel

[ideas/prefixInRel](ideas/prefixInRel){.source file text plain}

The rel-store should handle prefixes. It must be compatible with the
mirdf reader and writer.

Maybe in reader, use miRdfRel.prefixes, miRdfRel.replaces

And in miRdfRel add some tools:

    * _Resource.expandPrefix() => http://martin.insulander.info/ns/spca#Requirement
    * miRdfRel.addPrefix('http://martin.insulander.info/ns/spca#')
    * spca = miRdfRel.getPrefix('http://martin.insulander.info/ns/spca#')

Still templates will use prefixes, so we have to make sure to define
them early.

> miRdfRel.addPrefix(\'rdfs\', \'<http://>\.../rdfs\') =\> error if it
> is differently defined

## Backref in rel store

[ideas/rel backref](ideas/rel backref){.source file text plain}

In miRdfRel.path add some kind of backref. Would simplify filters. F.ex.
[..]{.title-ref} meaning parent match:

    /dbr:Song/-a/song:status_guitar/rdfs:value+=6/.. => a dbr:Song

## Rel store watchers aborted on return

[ideas/rel_stop_watcher_on_return_true](ideas/rel_stop_watcher_on_return_true){.source file text plain}

Let watchers be aborted based on return value:

    for watcher to act on:
       if (watcher(...))
           unwatch
           break

## Update html only once on changes

[ideas/separate update and drawing](ideas/separate update and drawing){.source file text plain}

Is it possible to update tree first and then html to update all at the
same time and make it much quicker.

Maybe need a pause method:

    rdfa.pause_ui()
    update store -> and rdfa
    rdfa.continue_ui()

    rdfa.add(much)
    - pause ui
    - update
    - continue ui

## Updated attributes

[ideas/updatedAttribs](ideas/updatedAttribs){.source file text plain}

Should simplify and remove \"data\" and \"mirdfa\" prefix from
attributes. For example:

-   about
-   resource
-   property
-   rel
-   attrib
-   retracted
-   onadded
-   onremove
-   arranger
-   arrange-on
-   clone
