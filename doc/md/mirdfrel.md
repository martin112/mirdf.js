---
pagetitle: miRdfRel
---

# miRdfRel

[miRdfRel/miRdfRel.ts](miRdfRel/miRdfRel.ts){.source file text x-c++}

miRdfRel - [RDF](mirdf_js)-data storage, for tripplets and relations
with methods to watch for specific changes to the data.

See [RDF](mirdf_js) for an introduction to data stored as relations in
triplets.

You can describe relations like:

    Martin knows Agnes
    My song is in genre Folk

And query or watch for changes:

    Show number of all Martins friends
    List all songs of a specific genre, and add new ones if they appear

The features are:

-   resources (subjects and predicates) are objects (a class)
-   manage relations between resources (a relation from one resource to
    another)
-   manage properties of resources (a relation to a string value)
-   query relations to and from (inverse relations) a resource
-   watch for changes to resources, relations and properties
-   query and watch paths (check [miRdfRel Query
    Path](#mirdfrel-query-path)).

miRdfRel does not require each resource to be prefixed. Usually they are
checked when using f.ex. miMirdf reader.

The reasoner is working with some standard RDF namespaces and semantic.
\'a\' is used instead of typeof, and the reasoner somewhat understands
**owl:inverseOf**, **rdfs:subPropertyOf**

## And how to use it

List names of friends:

    let martin = miRdfRel.getAddRes('mi:Martin')
    let names = new Set()
    martin.relTo('foaf:knows').forEach(r=>r.prop('rdfs:label').forEach(p=>names.add(p)))
    // or with a path
    names = martin.path('foaf:knows/rdfs:label')  // {'Agnes', 'Anders', 'Simon'}

List songs by genre, omitting Christmas songs:

    miRdfRel.path('/dbo:MusicGenre/-a/rdfs:label!=Christmas music/-dbo:genre')
    // From left: list all music genres,
    // skip those labeled "Christmas music",
    // find songs (or other things), with these genres

### miRdfRel main module

[miRdfRel/miRdfRel.ts 73](miRdfRel/miRdfRel.ts#73){.source file text x-c++}

Starting point to retrieve resources, query paths and some helper
functions.

The dataset is global and automatically initiated, so if one part of the
program stores anything, other parts will also be able to use that data.

#### Manage resources

#### Query absolute paths

For description of paths, see [miRdfRel Query
Path](#mirdfrel-query-path).

#### watch

\*/

> // connection between resrouce name and resource - also to keep
> resources unique let \_resources: Record\<string, Resource\> = {}
>
> let \_watchersAdd = watcher.newWatcherGroup\<string,string,Resource\>(
>
> :   
>
>     function(changedResName:string, watchResName:string) {
>
>     :   if (changedResName != watchResName) return if
>         (exists(watchResName)) return \[true, watchResName,
>         \_resources\[watchResName\]\] return \[false, watchResName\]
>
>     },
>
> ); /\* \$in:rst manage resources Get resource (or undefined if it does
> not exist):
>
> > `miRdfRel.`**getRes**`(resource name)`
>
> */ export function getAddRes (name:string) { let
> res:Resource\|undefined = getRes(name); if (!res) { res = new
> miRdfRel.Resource(name, \"only for internal use\") \_resources\[name\]
> = res \_watchersAdd.trigger(name) } return res; } /* \$in:rst manage
> resources Get resource (add it if it does not exist):
>
> > `miRdfRel.`**getAddRes**`(resource name)`
>
> \*/ export function getRes(name:stringundefined { if (isRes(name))
> return name as Resource; return \_resources\[name as string\] }
>
> /\* \$in:rst manage resources Remove resource:
>
> > `miRdfRel.`**rmRes**`(Resource | resource name)`
>
> \*/ export function rmRes(<res:Resource%7Cstring>) { let resChecked =
> getRes(res) if (resChecked) resChecked.rm() } export function
> \_rmRes(<res:Resource>) { delete \_resources\[res.name\]
> \_watchersAdd.trigger(res.name) }
>
> /\* \$in:rst manage resources List all resources:
>
> > `miRdfRel.`**listRes**`() => [Resource]`
>
> \*/ export function listRes (): Resource\[\] { return
> Object.values(\_resources) }
>
> /\* \$in:rst manage resources Check if an object is a resource:
>
> > `miRdfRel.`**isRes**`(Object) => boolean`
>
> \*/ export function isRes (something:any): boolean { return something
> instanceof miRdfRel.Resource }
>
> /\* \$in:rst manage resources Check if a resource is defined:
>
> > `miRdfRel.`**exists**`(resource name) => boolean`
>
> */ export function exists (name:string): boolean { return (name in
> \_resources) } /* \$in:rst watch When the resource is added,
> [onAddedCb](){.broken} is called. Call unwatch to stop watching:
>
> > `miRdfRel.`**watchRes**`(resource name, onAddedCb(Resource)) => unwatch()`
>
> \*/ export function watchRes ( name:string,
> cbOnAdded:watcher.onAddedCb\<Resource\>): watcher.unwatchCb { let
> unwatch = \_watchersAdd.watchAdd(name, cbOnAdded, \[name\]) return
> unwatch }
>
> /\* \$in:rst query absolute paths Query resources and values matching
> a path:
>
> > | `miRdfRel.`**path**`(string) => [Resource | string]`
> > | `miRdfRel.`**parsedPath**`([path]) => [Resource | string]`
>
> */ export function path(pathStr:string) : Set\<string\|Resource\> { let path = miRdfRel.pathFromString(pathStr); if (!(\"absolute\" in path) \|\| !path.absolute) { let str = miRdfRel.pathToString(path) console.error(\`miRdfRel.*Path First element must be a resource; \${str}\`)
>
> :   return new Set() } return parsedPath(path);
>
> } export function parsedPath(path:miRdfRel.pathAbs) { let matches:
> Set\<Resourcestring): () =\> void { matches.add(match); return
> function() {} }); unwatch && unwatch(); return new Set(matches); } /\*
> \$in:rst watch When an entry matches the path, onAdded is called. Use
> unwatch to stop watching.
>
> > | `miRdfRel.`**watchPath**`(string, onAdded(Resource)) => unwatch()`
> > | `miRdfRel.`**watchParsedPath**`([path], onAdded(Resource)) => unwatch()`
>
> */ export function watchPath ( pathStr:string, cbOnAdded:watcher.onAddedCb\<Resource\|string\>): watcher.unwatchCb\|undefined { let path = miRdfRel.pathFromString(pathStr); if (!(\"absolute\" in path) \|\| !path.absolute) { let str = miRdfRel.pathToString(path) console.error(\`miRdfRel.*Path First element must be a resource; \${str}\`)
>
> :   return } return watchParsedPath(path, cbOnAdded);
>
> } export function watchParsedPath ( path:miRdfRel.pathAbs, cbOnAdded:
> watcher.onAddedCb\<Resourceundefined; let cbPathOnResourceAdded:
> (<res:Resource>) =\> watcher.unwatchCb; if (path.rest) {
> cbPathOnResourceAdded = function(matchingRes) { nextUnwatch =
> matchingRes.watchParsedPath(path.rest, cbOnAdded); return function()
> {} } } else { cbPathOnResourceAdded = function(matchingRes) {
> cbOnAdded(matchingRes); nextUnwatch = undefined; return function() {}
> } } let thisUnwatch = \_watchersAdd.watchAdd(path.res,
> cbPathOnResourceAdded, \[path.res\]) return function() { nextUnwatch
> && nextUnwatch(); thisUnwatch(); } }
>
> /\* \$in:rst manage resources list all statements, for debugging and
> export:
>
> > `miRdfRel.`**stmts**`() => [ Resource, Resource, Resource | string ]`
>
> \*/ export function stmts(): stmt\[\] { let stmts: stmt\[\] = \[\];
> for (let resource of listRes()) { stmts.push(\...resource.stmts()); }
> return stmts; }

}

### miRdfRel Resource

### miRdfRel Query path

[miRdfRel/path.ts](miRdfRel/path.ts){.source file text x-c++}

> For a bit more complex queries, like multiple relations, or filtering
> out certain resources, a query path is recommended. Instead of
> checking each step maunaully, it follows multiple steps directly.
>
> ‼ A query path is a bit heavier than simpler requests, but much easier
> to handle.
>
> # path steps
>
> Steps are separated with \'/\'.
>
> Each step can be either a filter or a relation.
>
> The first step can also be a resurce, an absolute path, if the path is
> used directly from miRdfRel and not on a resource.
>
> A relation is f.ex. `someone/foaf:knows`. It can also be a check on
> relations back to resource, `music_genre/-dbp:genre`
>
> A filter is build up by property, operator and value, f.ex.
> `rdfs:label==Tjoho`. See [operators](){.broken}
>
> End of path; If the path ends with a \'/\' it means only return
> relations. If it ends with \'=\' it means only properties (string
> values).
>
> # Operators
>
> \*/
>
> export type path = pathAbsMorepathFilterMorepathRelLastpathFilter }
> type \_pathLast = { <res:string> rest:undefined only:\"rel\"false }
> type \_pathAbs = { absolute: true } type \_pathRel = { absolute: false
> inv: boolean } type \_pathFilt = { op:operator val:string
> dotval:boolean } export type pathAbs = pathAbsMorepathRelLast type
> pathRelMore = \_pathMore & \_pathRel type pathRelLast = \_pathLast &
> \_pathRel export type pathFilter = pathFilterMore\|pathFilterLast type
> pathFilterMore = \_pathMore & \_pathFilt type pathFilterLast =
> \_pathLast & \_pathFilt
>
> // Make sure to match operators in resource.ts let
> \_reasoner_operator_search = /(.+)(\--==+=!==.)(.\*)/;
>
> function \_pathFromString (parts:string\[\], first?:true): path {
>
> :   // console.debug(\'miRdfRel.\_pathFromString\', parts); if
>     (parts.length == 0) throw [miRdfRel \_pathFromString called with
>     empty path]{.title-ref} let part = parts.shift() as string if
>     (first && !part) { // absolute - starts with / if (parts.length
>     == 0) throw [Path with only a slash]{.title-ref} part =
>     parts.shift() as string if (\_reasoner_operator_search.exec(part))
>     { throw [Absolute path cannot start with a filter; \${part}
>     \${parts}]{.title-ref} } return \_pathFromStringRest(part, parts,
>     { absolute: true, }) as pathAbsMore\|pathAbsLast } if
>     (!part.length) { // empty step: // or missed last step: / should
>     never occur throw [Path cannot contain double //; \${part}
>     \${parts}]{.title-ref} } let match =
>     \_reasoner_operator_search.exec(part) if (match) { if (part\[0\]
>     == \'-\') throw [Path inverse pred cannot be a filter \${part}
>     \${parts}]{.title-ref} // filter if (match\[2\] == \'=.\') { //
>     dotval return \_pathFromStringRest(match\[1\], parts, { op:
>     \'==\', dotval: true, val: \'.\' + match\[3\], }) } else { //
>     filter value return \_pathFromStringRest(match\[1\], parts, { op:
>     match\[2\] as operator, dotval: false, val: match\[3\], }) } }
>     else { // relation let inv: boolean if (part\[0\] == \'-\') { part
>     = part.slice(1) inv = true } else { inv = false } return
>     \_pathFromStringRest(part, parts, { absolute: false, inv: inv, })
>     }
>
> } function \_pathFromStringRest( pred:string, rest:string\[\],
> pathBase:\_pathAbs_pathFilt ):path { //
> console.debug(\'miRdfRel.\_pathFromStringRest\', rest, rest.length,
> rest\[0\]) if (rest.length == 0) { // Last path entry if
> (pred.slice(-1) == \'=\') { // last path entry, only predicates
> (\....=) return { \...pathBase, res: pred.slice(0, -1), only: \"val\",
> } as pathRelLastpathFilterLast } } else if (rest.length == 1 &&
> rest\[0\]==\'\') { // last path entry, only relations (\..../) return
> { \...pathBase, res: pred, only: \"rel\", } as pathRelLastpathRelMore
> } }
>
> /\* \$in:rst Query absolute paths
>
> Convert between a string and a query path. Internally query paths (or
> parsed paths) are used:
>
> > | `miRdfRel.`**pathFromString**`(string) => path`
> > | `miRdfRel.`**pathToString**`(path) => string`
>
> \*/
>
> export function pathFromString (str: string): path {
>
> :   
>
>     if (!str)
>
>     :   throw [pathFromString called with empty path]{.title-ref}
>
>     const parts: string\[\] = str.split(\"/\"); return
>     \_pathFromString(parts, true);
>
> }
>
> export function pathToString (path: path): string {
>
> :   let out = \'\'; if (path === undefined) return \'\' else if
>     (\"op\" in path) { // filter out += \_pathToStringFilter(path); }
>     else { out += \_pathToStringRelation(path); } if (path.rest) out
>     += \'/\' + pathToString(path.rest); return out
>
> } function \_pathToStringFilter
> (path:pathFilterMorepathRelMorepathRelLast): string { let out = \'\';
> if (path.absolute == true) out += \'/\'; else if (path.inv) out +=
> \"-\"; out += path.res; if (\"only\" in path) { if (path.only ==
> \"rel\") out += \'/\'; if (path.only == \"val\") out += \'=\'; }
> return out; }

}

### Watcher

[miRdfRel/miRdfRel.ts](miRdfRel/miRdfRel.ts){.source file text x-c++}

\*/ /// \<reference path=\"definitions.ts\" /\> /// \<reference
path=\"resource.ts\" /\> /// \<reference path=\"path.ts\" /\> ///
\<reference path=\"watcher.ts\" /\>

namespace miRdfRel { /\* namespace miRdfRel include by either: include
in code: /// \<reference path=\"mi.rdf.rel\"/\> or compile as module and
use as: import { miRdfRel } from \"./miRdfRel.ts\"

[miRdfRel/watcher.ts](miRdfRel/watcher.ts){.source file text x-c++}

A small library to hande watching and triggereing of data. It is written
and adapted for miRdfRel, but could be used outside of it.

#### Usage

Start with creating a matcher function. Input to it is a value
describing a change of data, and a value to watch for. The value can be
an ordinary string or a more complex structure. The matcher function
returns three different values:

-   undefined if not applicable
-   \[true, key, what to send to [onAddedCb](){.broken}\] for matches
-   \[false, key\]

The `key` is a string used for differentiating matches. Usually a
representation of the change.

Secondly, create a watcher group with the matcher function:

> `let watchergroup = watcher.`**newWatcherGroup**`(matcher)`

Then, to watch for a change:

> `let unwatch = watcherGroup.`**addWatcher**`(onAddedCb)`

When any data is updated and matching, [onAddedCb](){.broken} is called.
It is called one tome for each match, and also includes already existing
values when the watcher is created:

> `onAddedCb(result value) => onRemoveCb`

If a matching value is removed, the `onRemoveCb` from the onAddedCb is
called, without any argument.

> `onRemoveCb()`

To stop watching, call the `unwatch()` function from when the watcher
was created.

Note. A match can be inverse, like resource is missing relation, but
usually they are when someting is added.

\*/

namespace watcher {

> /\* in:rst Watcher unwatchCb \-\-\-\-\-\-\-\--To stop watching for
> something. This callback is returned from each new watcher you make.
> \*/ export type unwatchCb = { \_collapse?: {cb?:() =\> void} (): void
> } export type onRemovedCb = () =\> void
>
> /\* \$in:rst Watcher onAddedCb \-\-\-\-\-\-\-\--When a match is made,
> onAddedCb is called with a wathergroup specific value. The onAddedCb
> can(should) return a **onRemovedCb**. This method is called when it
> doesn\'t match anymore, f.ex. a relation is removed. \*/ export type
> onAddedCb\<addT\> = (val:addT) =\> onRemovedCb\|undefined
>
> export type watcher\<matchT,addT\> = {
>
> :   onAddedCb: onAddedCb\<addT\> onRemovedCb:
>     Record\<string,onRemovedCb\> matchVal: matchT
>
> } export type triggerCbs = (() =\> void)\[\] export type
> matcherReturn\<addT\> = \[true, string, addT\]undefined export type
> watcherGroup\<changeT,matchT,addT\> = { watchAdd: (matchVal:matchT,
> onAddedCb:onAddedCb\<addT\>, matchChanges?:changeT\[\]) =\> unwatchCb
> getTriggered: (change:changeT) =\> triggerCbs actOnTriggered:
> (triggered:triggerCbs) =\> void trigger: (change:changeT) =\> void,
> \_set: Set\<watcher\<matchT,addT\>\> clear: () =\> void, }
>
> export let \_watchersCount: number = 0
>
> /\* call each callback in a list of them
>
> :   separate trigger checks and acting on them to reduce risk of
>     checking manipulated lists \*/
>
> export function actOnTriggered(triggered:triggerCbs) {
>
> :   triggered.forEach(cb =\> cb())
>
> }
>
> /\* Create a watcher group that is used for storing
>
> :   callbacks and call them when a matching change occurs
>
> \*/ export function newWatcherGroup\<changeT,matchT,addT\>(
> matcher:(change:changeT,matchVal:matchT) =\> matcherReturn\<addT\>, ):
> watcherGroup\<changeT,matchT,addT\> {
>
> > type thisWatcher = watcher\<matchT,addT\>
> >
> > let set:Set\<thisWatcher\> = new Set();
> >
> > /\* check a watcher against a change, return trigger callback if it
> > matches (add or remove) \*/ let getTriggerCbForWatcher =
> > function(change:changeT, watcher: thisWatcher): {():void}\|undefined
> > { let match = matcher(change, watcher.matchVal) if (match) { let
> > \[matching, key, value\] = match if (matching == true) { if (!(key
> > in watcher.onRemovedCb)) { return function() {
> > watcher.onRemovedCb\[key\] = watcher.onAddedCb(value as addT) as
> > onRemovedCb } } } else { if (key in watcher.onRemovedCb) { let cb =
> > watcher.onRemovedCb\[key\] delete watcher.onRemovedCb\[key\] return
> > cb } } } }
> >
> > /\* check all watchers against a change, return trigger callbacks
> > for each matching (add or remove) \*/ let getTriggered =
> > function(change:changeT): triggerCbs { // must be followed by
> > actOnTriggered! let triggered: {():void}\[\] = \[\] for (let watcher
> > of set) { let triggerCb = getTriggerCbForWatcher(change, watcher)
> > triggerCb && triggered.push(triggerCb) } return triggered }
> >
> > /\* get list of triggers matching a change and act on them \*/ let
> > trigger = function(change:changeT) {
> > actOnTriggered(getTriggered(change)) }
> >
> > /\* remove all watchers from group \*/ let clear = function() {
> > set.clear() }
> >
> > /\* add a new watcher that will be triggered on a change matching it,
> >
> > :   optionally send with a change to trigger it imideatly \*/
> >
> > let watchAdd = function(matchVal:matchT, onAddedCb:onAddedCb\<addT\>, matchChanges?:changeT\[\]): unwatchCb {
> >
> > :   ++\_watchersCount let w = { matchVal: matchVal, onAddedCb:
> >     onAddedCb, onRemovedCb: {}, } as watcher\<matchT,addT\>
> >     set.add(w) for (let change of matchChanges \|\| \[\]) { let cb =
> >     getTriggerCbForWatcher(change, w) cb && cb() } return function()
> >     { \--\_watchersCount set.delete(w); }
> >
> > } return { watchAdd: watchAdd, getTriggered: getTriggered,
> > actOnTriggered: actOnTriggered, trigger: trigger, \_set: set, // for
> > debugging clear: clear, }
>
> }

}
