---
pagetitle: miRdfa
---

# miRdfa

[miRdfa/miRdfa.ts](miRdfa/miRdfa.ts){.source file application javascript}

A template engine for making pages based on non-hierarchical data.

Inspired by rdfa lite, and heavily based on [RDF](mirdf_js). In stead of
using hierarchical data (like json), it handles data of directed
relations (can be seen as documents or graphs), specifically RDF_data.
The idea is to use the same data format from server storage all the way
to the end view.

-   Templates are written directly in html, making it easy to preview
    the result
-   Changes to the underlying data is reflected directly in the view
-   Queries of data are intuitive paths through the relational data
-   The engine is written in typescript

## Hello World

``` html
<!DOCTYPE html>
<head>
    <title>Hello miRdfa</title>
    <source href=miRdfa.js></source>
    <script>
        miRdfa.setupGlobal();
        let my_hello = miRdfRel.getAddRes('my:hello')
        let rdfs_label = miRdfRel.getAddRes('rdfs:label')
        my_hello.addProp(rdfs_label, "miRdfa world!")
    </script>
</head>
<article about=":hello">
    <h2 property="rdfs:label"></h2>
</article>
```

## MiRdfa Usage

Check [miRdfa templates](mirdfa-templates) to see how to write the
templates in html.

For a more indepth view, mostly for debugging miRdfa, check [miRdfa
technical](mirdfa-technical).

### miRdfa data base

miRdfa is built around a data storage, and the ui updates with changes
to this data. The data is based on [RDF](mirdf_js). See
[miRdfRel](mirdfrel) for how to manipulate the data.

### Initialize miRdfa

The miRdfa.js engine needs to be initialised. It can either be setup
globally (one instance for the whole page) or one or more instances for
specific parts of the page. But note that the same data storage will be
used for all instances.

#### Global

[miRdfa/miRdfa.ts 87](miRdfa/miRdfa.ts#87){.source file application javascript}

When setting up globally, the inital miRdfa-object is replaced:

``` javascript
miRdfa.setupGlobal(optional function(){ run when setup is done })
miRdfa.update(function(){ wait with redraw until function is done *})
```

#### Instance

[miRdfa/miRdfa.ts 107](miRdfa/miRdfa.ts#107){.source file application javascript}

For dymaically loaded pages or very big pages.

Each instance behaves the same as the global \"miRdfa\" object after
setupGlobal, and in most places in the documentation, the main
\"miRdfa\" object can be replaced with your instance.

``` javascript
let instance = miRdfa.setup(templateRootHtmlElement)
instance.printTemplate()
```

#### Print template

[miRdfa/miRdfa.ts 178](miRdfa/miRdfa.ts#178){.source file application javascript}

When an miRdfa instanse is created, it will print out the template
structure to the console. It is done with this function:

``` javascript
miRdfa.printTemplate()
```

### For bigger updates

[miRdfa/miRdfa.ts 158](miRdfa/miRdfa.ts#158){.source file application javascript}

If you change a lot, it goes quicker if you use the update-function. It
will pause redrawing until the function is done:

``` javascript
miRdfa.update(function(){ miRdfRel.addRes(.... })
```

### arranger

[miRdfa/miRdfa.ts 205](miRdfa/miRdfa.ts#205){.source file application javascript}

See [Sort and limit](){.broken} for more general information.

miRdfa has an arranger function included, that can sort and limit. It
takes some optional arguments.

Possible arguments are:

limit

:   Number of entries to show

desc

:   Set to true to sort smallest first

on

:   List of properties to sort on

``` javascript
miRdfa.arranger({'limit': 5, 'descend':false})
```
