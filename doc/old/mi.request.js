'use strict';
/*************
 * Make one request at a time, stops previous request
   version 2016.11.24 - added ready function
   version 2015.04.03 - fixed _extend default props don't overwrite given props
   version 2017.11.04 - fixed adding optionals and added delay on callbacks

   to make a request
   var request = new Request()
   request.send
      address     // www.example.com/request.php
      options {}
         arg      // arguments as stringto add to address request.php?a=1,b=2
         method   // f.ex. 'POST' defaults to 'GET' (case sensitive!)
         body     // body to send, mostly with a 'POST' request
         header {}// add these headers, f.ex. 'Accept':'application/json'
         timeout  // milliseconds to wait before time out, defaults to 10000
         responseType // defaults to string, use for example 'json'
      callback {} // functions to call:
         // the return xmlhttprequest is reused on next call
         200..299 or 'ok'    // callback response, status, xmlhttprequest
            // on good status. Call specific status callback
            // if it doesn't exists, try 'ok'
         error or // an error occured ( don't use together with 400.599 )
         400..599 or 'error' // callback ready state, status, xmlhttprequest
            // on error status. Call specific status callback
            // if it doesn't exists, try 'error'

   to abort current request
      request.abort()
   to see if the request is not waiting
      request.ready()

   on success passes returned text to cb_done
 * ********************** */


var mi = typeof(mi)=='undefined' ? {} : mi;

mi.Request = function( default_options, default_callbacks ) {
   var ready = false;

   try {
      this.xhr = new XMLHttpRequest(); // IE7+
   } catch(e) {
      window.console && console.error( 'mi.Request init xhr', e );
      return undefined;
   }

   this.default_options = default_options || {};
   this.default_callbacks = default_callbacks || {};

   // last request because
   // IE , Firefox can't attach custom data to xhr
   // _last.responseType because some browsers doesn't support it
   var _last = {
      cb: {},
      responseType: undefined,
   }

   var call_back = function(_this, callback, arglist) {
      setTimeout(function() {
         callback.apply(_this, arglist);
      }, 10);
   }

   /* to implement something like this
      if responseType not supported and options.responseType == json
      and not defined response JSON.parse(responseText)
   */
   this.onreadystatechange = function() {
      // callback, this==xhr
      if ( this.readyState==4 ) {
         ready = true;
         if ( this.status>=200 && this.status<300 ) {
            var response = this.response;
            // fix json
            if ( _last.responseType=='json'
               && this.responseType!='json' ) {
               try {
                  response = JSON.parse( this.responseText );
               } catch(e) {
                  window.console && console.error( e, this.responseText );
                  response = null;
               }
            }

            if ( _last.cb[ this.status ] ) {
                call_back(this, _last.cb[ this.status], [response, this.status]);
            } else if ( _last.cb.ok ) {
                call_back(this, _last.cb.ok, [response, this.status]);
            }

         } else {
            if ( _last.cb[ this.status ] ) {
               call_back(this,
                         _last.cb[this.status], [this.status, this.response, this.readyState]);
            } else if ( _last.cb.error ) {
               call_back(this, _last.cb.error, [this.status, this.response, this.readyState]);
            }
         }

     } else if (_last.cb.wait) {
         call_back(this, _last.cb.wait, [this.readyState]);
      }
         // response
         // This is null if the request is not complete or was not successful.

         // status
         //case 0: ??
         //case 401:   // Unauthorized
         //case 403:   // Forbidden
         //case 404:   // Not Found

         // readyState https://developer.mozilla.org/en/docs/Migrate_apps_from_Internet_Explorer_to_Mozilla
         //case 4:  // request doneif ( typeof new XMLHttpRequest().responseType !== 'string' ) {
         //case 3:  // request is in process but only part of the data is ready
         //case 2:  // request is sent but not in process
         //case 1:  // data requested but not sent
         //case 0:  // called but not initialized
   }

   this.abort = function() {
      if ( this.xhr.readyState >=2  ) { // send has been called
         this.xhr.onreadystatechange = function(){}; // for IE
         this.xhr.abort();
      }
   }

   var _extend = function( obj, with_obj ) {
      var props = Object.keys( with_obj );
      for (var i=0,prop; prop=props[i]; i++) {
         if ( obj[prop]!==undefined && typeof( with_obj[prop] )=='object' ) {
            _extend( obj[prop], with_obj[prop] );
         } else {
            obj[prop] = with_obj[prop];
         }
      }
   }

   this.ready = function() {
      return ready;
   }

   this.send = function ( address, options, add_callbacks ) {

      this.abort(); // for Mozilla
      ready = false;

      _last.cb = {};
      _extend( add_callbacks, this.default_callbacks );
      _extend( _last.cb, add_callbacks );

      _extend( options, this.default_options );

      this.xhr.open(
         options.method ? options.method : 'GET',
         address + (options.arg ? '?'+options.arg : '' ),
         true
      );

      // place xhr assignments after open, http://keelypavan.blogspot.se/2006/03/reusing-xmlhttprequest-object-in-ie.html

      this.xhr.address = address;

      _last.responseType = options.responseType;
      if ( options.responseType ) {
         try {
            this.xhr.responseType = options.responseType;
         } catch( e ) {}
      }

      this.xhr.onreadystatechange = this.onreadystatechange;

      for ( var header in options.header ) {
         this.xhr.setRequestHeader( header, options.header[header] );
      }

      this.xhr.timeout = options.timeout || 10000;
      // xhr.ontimeout ?

      this.xhr.send( options.body );
   }

   ready = true;
}

