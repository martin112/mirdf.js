/*
 Small library handling location and history as hash. It is specially made for mi.rdfa and updates the mi.rdfa.store with info on current selected view.

For usage, include library and setup the library in a javasript that is run early (before window.load).

go.settings = {
   default: ':main',  // Main view resource
   title_prefix: 'Chords - ',  // Prefix for window title
   title_property: 'dbp:title',  // property to use to extract title for a view
   nav_resource: ':nav',  // Base resource for navigation
   nav_property: ':view',  // View property for navigation
}
 
*/
function go(where, skippushstate) {
   where = go.main(where)
        || go.event(where)
        || go.element(where)
        || go.hash(where)
        || where;
   for (var spor_t of mi.rdfa.store.reason('/' + go.settings.nav_resource
                                         + '/' + go.settings.nav_property)) {
      mi.rdfa.store.remove(spor_t);
   }
   console.info('go', where);
   if (where) {
      var label = mi.rdfa.store.reason('/' + where + '/' 
                                      + go.settings.title_property)[0];
      label = label ? label[label.target] : where;
      document.title = go.settings.title_prefix + label;
      mi.rdfa.store.add([go.settings.nav_resource,
                         go.settings.nav_property,
                         where, true]);
      if (!skippushstate) {
         var state_where = (where != go.settings.default ? '#' + where : '#');
         window.history.pushState(where, label, state_where);
      }
      document.body.classList.remove('loading');
   }
}
go.main = function(where) {
   if (where == undefined) {
      return go.settings.default;
   }
}
go.event = function(e) {
   if (e.preventDefault) {
      e.preventDefault();
      e.stopImmediatePropagation();
      return go.element(e.currentTarget || e.target);
   }
}
go.element = function(el) {
   if (el.getAttribute) {
      return el.getAttribute('href')
          || el.getAttribute('resource')
          || el.getAttribute('about');
   }
}
go.hash = function(hash) {
   if (hash[0] == '#') {
      return hash.substr(1);
   }
}
go.state_go = function(e) {
   go(e.state, true);
}
window.addEventListener('popstate', go.state_go);
window.addEventListener('load', function() {
    go(location.hash);
});

