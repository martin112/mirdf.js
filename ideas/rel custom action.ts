/*
    Add cusom functions to miRdfRel.
    Either register a global property, or add to a specific resource.

*/

Register custom properties and action on how to calculate them:

miRdfRel.addCalculatedRel(property<Resource>, (Resource, property<Resource>)=>[Resource] )
miRdfRel.addCalculatedVal(property<Resource>, (Resource, property<Resource>)=>[str] )
<Resource>.addCalculadedRel(property<Resource>, (Resource, property<Resource>)=>[Resource] )
<Resource>.addCalculadedVal(property<Resource>, (Resource, property<Resource>)=>[str] )

miRdfRel.onAddedRel(property<Resource>, (Resource, property<Resource>, Resource|str)=>void)
miRdfRel.onAddedProp(property<Resource>, (Resource, property<Resource>, Resource|str)=>void)
<Resource>.onAddedRel(property<Resource>, (Resource, property<Resource>, Resource|str)=>void)
<Resource>.onAddedProp(property<Resource>, (Resource, property<Resource>, Resource|str)=>void)

/* Examle last updated formatted */

function last_updated_formatted(res: Resource) -> string {
    return somehow format find_last_updated(res)
}
function find_last_updated(res: Resource) -> Date {
    let last_updated = 0;
    for (let v in res.getProp('task:last_updated')) {
        if (v > last_updated) {
            last_updated = v
        }
    }
    for (let c in res.getRel('task:child_comment')) {
        child_last_updated = find_last_updated(c)
        if (child_last_updated > last_updated) {
            last_updated = child_last_updated
        }
    }
    return last_updated
}
miRdfRel.reg_calculated_val('ui:last_updated', last_updated_formatted)
...
    <span property="ui:last_updated"></span>


/* example value */
miRdfRel.addCalculatedVal('cla1:dayWorkTime', (resPersonDay: Resource, prop: Resource) => {
    // assert one first activitiy per day
    let act = resPersonDay.getRelOne(firstActivity)
    if (!act) return 0
    let dayWorkTime = 0
    while (act) => {
        dayWorkTime += act.getVal('cla1:actWorkTime').vales().items()[0].value
        act = act.nextActivity.values().items()[0].value
    }
    return dayWorkTime
})

/* example cached value
personDay.getRelOne(cla1_dayWorkTime).getValOne(fn_calculate)

:Martin
 :20230301
   a :PersonDay
   cla1:dayWorkTime
    a cla1DayWorkTime
    fn:func_:Martin_:20230301_cla1:dayWorkTime
        a fn:Function
        fn:value
        fn:_cached
        fn:invalidate
        fn:cleanup

act:20230301_2
 a dayWorkTime
 fn:affects fn:func_:Martin_:20230301_cla1:dayWorkTime


on update
    :20230301_2
        workTime = X
        fn:affects.forEach(res.getRelOne(fn:invalidate))


function fn_invalidate(res, fn_invalidate<Resource>)
    res.rmProp(fn__cached)
    res.getRel(fn_affects).forEach(res=>{res.getProp(fn_invalidate)})


res_fn_func_Martin_20230301_cla1_dayWorkTime.remove()
    kommer automagiskt att rensa upp affected!
    det svåra är att se till att lägga till dem!


onAddedRel(cla1_activityDay)
    wt = newRes
    day.addRel(cla1_dayWorkTime, wt)
    cla1_dayWorkTime  = day.getRelOne(cla1_workTime)
    wt.addRel(fn_affcets, cla1_workTime)

onAddedRel(activity_day, cla1_activity, activity)
    activity_day.addRel(fn_affects, cla1_workTime)


*/

