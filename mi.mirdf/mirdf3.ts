/*
Data reader for compact indented rdf format
*/
/// <reference path="../miRdfRel/definitions.ts" />


namespace miRdf {

   export var HEADER_MIRDF3 = '# mirdf:version "3'

   type stmtCb = (stmt: miRdfRel.stmt) => any

   export function read3(text:string, cbAddStmt:stmtCb, cbRmStmt:stmtCb, defaultPrefix:string, prefix:Prefix) {
      if (!prefix.knownPre(defaultPrefix)) {
         throw "Default prefix [" + default_prefix + "] isn't known";
      }

      var first_line_end = text.indexOf('\n');
      if (!text.startsWith(HEADER_MIRDF3)) {
         var first_line = text.substr(0, first_line_end < 0 ? undefined : first_line_end);
         throw "Not a mirdf3 stream header: " + first_line;
      }
      var from = first_line_end + 1;
     
      function pre_fix_res(ref, linenr) {
         /*
         ref is first replaced if needed, then checked for unknown
         prefixes.
         @ref a namespaced reference
         @return - corrected ref
         */
         if (ref[0] == '#') {
            ref = defaultPrefix + ref.substr(1);
         } else if (ref != 'a') {
            // allow 'a' as a resource
            for (var rep in replace) {
               if (ref.startsWith(rep)) {
                  ref = ref.replace(rep, replace[rep]);
               }
            }
            // warn if prefix is unknown
            var prefix_found = false;
            for (var pre in prefix) {
               if (ref.startsWith(pre)) {
                  prefix_found = true;
                  break;
               }
            }
            if (!prefix_found) {
               console.warn(
                  'Undefined prefix for resource "' + ref + '" on line ' + linenr + ': ' + line);
            }
         }
         return ref;
      }

      function pre_fix(sss, ppp, ooo, rel, linenr, line): miRdfRel.stmt {
         /*
         If it is a prefix setter, 'mirdf:pre',
         update namespace prefix and maybe prefix replace table

         Else, check and maybe replace prefix of subject, predicate
         and object if it is a relation.
         */
         if (ppp == 'mirdf:pre') {
            // new prefix definition
            if (!(sss in prefix)) {
               // new prefix
               var matched_pre = false;

               for(var pre in prefix) {
                  if (prefix[pre] == ooo) {
                     matched_pre = pre;
                     break;
                  }
               }
               if (matched_pre) {
                  // url stated before
                  replace[sss] = matched_pre;
               } else {
                  prefix[sss] = ooo;
               }
            } else {
               // old prefix
               if (prefix[sss] != ooo) {
                  // redefinition of prefix
                  var found = false;
                  for (var i=2; i<100; i++) {
                     var new_prefix = String(i) + sss
                     if (!(new_prefix in prefix)) {
                        found = true;
                        replace[sss] = new_prefix;
                        prefix[new_prefix] = ooo;
                        break;
                     }
                  }
                  if (!found) {
                     throw "To many prelaced prefixes, handles up to 99 same names" +
                           " on line " + linenr + ': ' + line;
                  }
               }
            }
         } else {
            // other statement
            ppp = pre_fix_res(ppp, linenr, line);
            sss = pre_fix_res(sss, linenr, line);
            if (rel && ooo) ooo = pre_fix_res(ooo, linenr, line);
         }
         return {s:sss, p:ppp, o:ooo, r:rel};
      }
      var adder = new function() {
         var sss = undefined;
         var ppp = undefined;
         var kind = undefined;
         var ooo = undefined;
         this.submit_last = function() {
            if (!ooo && kind != '-') {
               // no last statement to add
               return;
            }
            else if (kind == '-"') {  // remove value
               cbRmStmt(pre_fix(sss, ppp, ooo, false, linenr, line));
            }
            else if (kind == '-' && ooo) {  // remove relation
               cbRmStmt(pre_fix(sss, ppp, ooo, true, linenr, line));
            }
            else if (kind == '-') {  // !ooo // remove property
               cbRmStmt(pre_fix(sss, ppp, ooo, true, linenr, line));
               // prevent removing multiple times
               kind = undefined;
            }
            else if (kind == '"') {  // add value
               cbAddStmt(pre_fix(sss, ppp, ooo, false, linenr, line));
            }
            else if (kind == '') {  // add relation
               cbAddStmt(pre_fix(sss, ppp, ooo, true, linenr, line));
            } else {
               throw "unknwon kind '" + kind + "'";
            }
            // only act once on each ooo
            kind = undefined;
            ooo = undefined;
         }
         this.set_s = function(match_s, linenr) {
            this.submit_last();
            sss = match_s;
         }
         this.set_p = function(match_p, linenr) {
            if (!sss) throw "Property assignment before first subject @ line " + linenr;
            this.submit_last();
            ppp = match_p;
         }
         this.set_kind = function(match_t, linenr) {
            kind = match_t;
         }
         this.set_o_part = function(o_part, linenr, force_kind_to_property) {
            if (!ppp) throw "Object defined without propert @ line " + linenr;
            if (ooo) {
               ooo += '\n' + o_part;
            } else {
               if (force_kind_to_property) {
                  kind = '"';
               }
               ooo = o_part;
            }
         }
      }

      let line_re = /^([^ ]+)?( +([^ ]+)( +(-"|"|-|)(.*))?)?/;
      // match 0:complete, 1:s, 3:p, 5:type, 6:o
      let line:string;
      let linenr = 0;
      while(from >= 0) {
         var next_nl = text.indexOf('\n', from);
         if (next_nl > 0) {
            line = text.slice(from, next_nl);
            from = next_nl + 1;
         } else {
            line = text.slice(from);
            from = -1;
         }
         linenr += 1;
         // if (linenr > 10) throw 'DEBUGGING';
         if (!line || line[0] == '%') {
            // empty line or comment
         } else if (line[0] == '"') {
            adder.set_o_part(line.substr(1), linenr, true);
         } else {
            var match = line.match(line_re);
            if (match) {
               if (match[1]) {
                  adder.set_s(match[1], linenr);
               }
               if (match[3]) {
                  adder.set_p(match[3], linenr);
                  if (match[5] !== undefined) {
                     adder.set_kind(match[5]);
                     if (match[6]) {
                        adder.set_o_part(match[6]);
                     }
                  }
               }
            }
         }
      }
      adder.submit_last();
      adder.submit_last();
      adder.submit_last();
   }

   export function read3_test() {
      let str = `# mirdf:version "3
s:1 p:1 o:1
s:2
 p:2 o:o2
 p:3 \"oo3
s:3 p:4
"oo5:1
"oo5:2
 p:5  "oo6
s:4  p:6 -"oo7 rm
 p:7   -o:8rm
 p:8-
s:5 p:9 -"sdf
"sdf
 p:10 "df
s:6 p:11 o:9
  p:12 "o:10 str
  p:13   o:11
s:7 p:14 o:12`;
      console.log('input string:');
      console.log(str);
      console.log('parsed statements:');
      read3(
         str,
         function(stmt:miRdfRel.stmt){console.log('add', stmt)},
         function(stmt:miRdfRel.stmt){console.log('rm', stmt)},
         'test:',
         new Prefix({
            'test:': 'http://example.org',
            's:': 'http://example.org/s',
            'p:': 'http://example.org/p',
            'o:': 'http://example.org/o',
         })
      );
   }
}