/*
Data handlers for compact indented rdf format
*/
/// <reference path="../miRdfRel/definitions.ts" />
/// <reference path="mirdf3.ts" />

namespace miRdf {

    export class Prefix {
        private prefix: Record<string,string> = {}
        private replace: Record<string,string> = {}
        readonly defaultPrefix: string

        constructor(prefix:Record<string,string>) {
            this.extend(prefix)
        }

        add(pre:string, url:string): void {
            this.prefix[pre] = url
        }

        extend(prefixes:Record<string,string>): void {
            for (let pre in prefixes) {
                this.add(pre, prefixes[pre])
            }
        }

        knownPre(pre:string): boolean {
            return pre in this.prefix
        }

        expand(resource:string): string|undefined {
            for (let pre in this.prefix) {
                if (resource.startsWith(pre)) {
                    return this.prefix[pre] + resource.substr(pre.length)
                }
            }
            console.warn(`Prefix for "${resource}" is not defined`)
            return
        }
    }

    export class Mirdf {
        public prefix: Prefix
        private cbAddStmt: stmtCb
        private cbRmStmt: stmtCb

        constructor(cbAddStmt:stmtCb, cbRmStmt:stmtCb, prefix:Prefix) {
            this.prefix = prefix
            this.cbAddStmt = cbAddStmt
            this.cbRmStmt = cbRmStmt
        }

        read = function(text:string, defaultPrefix:string): void {
            miRdf.read3(this.cbAddStmt, this.cbRmStmt, this.prefix)
        }
    }
}