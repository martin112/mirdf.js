/*
Data reader for compact indented rdf format
20171202
*/
'use strict';
var mi = typeof(mi)!='undefined' ? mi : {};
mi.mirdf3 = mi.mirdf3 ? mi.mirdf3 : {};
var HEADER_MIRDF3 = '# mirdf:version "3'


/**
 * Handy object handling prefixes
 */
mi.mirdf3.Mirdf3 = function(cb_add_stmt, cb_rm_stmt, prefix, default_prefix) {
   var $ = this;
   $.default_prefix = default_prefix;
   $.prefix = (prefix === undefined) ? {} : prefix;
   $.replace = {};

   this.read = function(text, default_prefix) {
      mi.mirdf3.reader(cb_add_stmt, cb_rm_stmt, text,
                     default_prefix || $.default_prefix, $.prefix, $.replace);
   }

   this.expand_prefix = function(resource) {
     for (var prefix in $.prefix) {
       var len = prefix.length;
       if (resource.substr(0, len) == prefix) {
         return $.prefix[prefix] + resource.substr(len);
       }
     }
   }
}


/**
 * Read a text string formated in mirdf2 format
 * @param cb_add_stmt: called on each found statement, with it as parameter
 * @param default_prefix: What to replace default namespaces with (#)
 * @param default_url: Default namespace url
 * @param prefix: optional - dictionary with prefixes as keys and their matching url
 * @param replace: optional - dictionary with prefixes to replace. Key is replaced.
 */
mi.mirdf3.reader = function(cb_add_stmt, cb_rm_stmt, text,
                            default_prefix, prefix, replace) {

   if (prefix === undefined) {
      prefix = {};
   }
   if (replace === undefined) {
      replace = {};
   }
   if (default_prefix && prefix[default_prefix] === undefined) {
      throw "Default prefix [" + default_prefix + "] isn't a defined prefix";
   }
   var first_line_end = text.indexOf('\n');
   if (!text.startsWith(HEADER_MIRDF3)) {
      var first_line = text.substr(0, first_line_end < 0 ? undefined : first_line_end);
      throw "Not a mirdf3 stream header: " + first_line;
   }
   var from = first_line_end + 1;

   function pre_fix_res(ref, linenr) {
      /*
      ref is first replaced if needed, then checked for unknown
      prefixes.
      @ref a namespaced reference
      @return - corrected ref
      */
      if (ref[0] == '#') {
         ref = default_prefix + ref.substr(1);
      } else if (ref != 'a') {
         // allow 'a' as a resource
         for (var rep in replace) {
            if (ref.startsWith(rep)) {
               ref = ref.replace(rep, replace[rep]);
            }
         }
         // warn if prefix is unknown
         var prefix_found = false;
         for (var pre in prefix) {
            if (ref.startsWith(pre)) {
               prefix_found = true;
               break;
            }
         }
         if (!prefix_found) {
            console.warn(
               'Undefined prefix for resource "' + ref + '" on line ' + linenr + ': ' + line);
         }
      }
      return ref;
   }

   function pre_fix(sss, ppp, ooo, rel, linenr, line) {
      /*
      If it is a prefix setter, 'mirdf:pre',
      update namespace prefix and maybe prefix replace table

      Else, check and maybe replace prefix of subject, predicate
      and object if it is a relation.
      */
      if (ppp == 'mirdf:pre') {
         // new prefix definition
         if (!(sss in prefix)) {
            // new prefix
            var matched_pre = false;

            for(var pre in prefix) {
               if (prefix[pre] == ooo) {
                  matched_pre = pre;
                  break;
               }
            }
            if (matched_pre) {
               // url stated before
               replace[sss] = matched_pre;
            } else {
               prefix[sss] = ooo;
            }
         } else {
            // old prefix
            if (prefix[sss] != ooo) {
               // redefinition of prefix
               var found = false;
               for (var i=2; i<100; i++) {
                  var new_prefix = String(i) + sss
                  if (!(new_prefix in prefix)) {
                     found = true;
                     replace[sss] = new_prefix;
                     prefix[new_prefix] = ooo;
                     break;
                  }
               }
               if (!found) {
                  throw "To many prelaced prefixes, handles up to 99 same names" +
                        " on line " + linenr + ': ' + line;
               }
            }
         }
      } else {
         // other statement
         ppp = pre_fix_res(ppp, linenr, line);
         sss = pre_fix_res(sss, linenr, line);
         if (rel && ooo) ooo = pre_fix_res(ooo, linenr, line);
      }
      return [sss, ppp, ooo, rel];
   }
   var adder = new function() {
      var sss = undefined;
      var ppp = undefined;
      var kind = undefined;
      var ooo = undefined;
      this.submit_last = function() {
         if (!ooo && kind != '-') {
            // no last statement to add
            return;
         }
         else if (kind == '-"') {  // remove value
            cb_rm_stmt(pre_fix(sss, ppp, ooo, false, linenr, line));
         }
         else if (kind == '-' && ooo) {  // remove relation
            cb_rm_stmt(pre_fix(sss, ppp, ooo, true, linenr, line));
         }
         else if (kind == '-') {  // !ooo // remove property
            cb_rm_stmt(pre_fix(sss, ppp, ooo, true, linenr, line));
            // prevent removing multiple times
            kind = undefined;
         }
         else if (kind == '"') {  // add value
            cb_add_stmt(pre_fix(sss, ppp, ooo, false, linenr, line));
         }
         else if (kind == '') {  // add relation
            cb_add_stmt(pre_fix(sss, ppp, ooo, true, linenr, line));
         } else {
            throw "unknwon kind '" + kind + "'";
         }
         // only act once on each ooo
         kind = undefined;
         ooo = undefined;
      }
      this.set_s = function(match_s, linenr) {
         this.submit_last();
         sss = match_s;
      }
      this.set_p = function(match_p, linenr) {
         if (!sss) throw "Property assignment before first subject @ line " + linenr;
         this.submit_last();
         ppp = match_p;
      }
      this.set_kind = function(match_t, linenr) {
         kind = match_t;
      }
      this.set_o_part = function(o_part, linenr, force_kind_to_property) {
         if (!ppp) throw "Object defined without propert @ line " + linenr;
         if (ooo) {
            ooo += '\n' + o_part;
         } else {
            if (force_kind_to_property) {
                kind = '"';
            }
            ooo = o_part;
         }
      }
   }
   var line_re = /^([^ ]+)?( +([^ ]+)( +(-"|"|-|)(.*))?)?/;
   // match 0:complete, 1:s, 3:p, 5:type, 6:o
   var line;
   var linenr = 0;
   while(from >= 0) {
      var next_nl = text.indexOf('\n', from);
      if (next_nl > 0) {
         line = text.slice(from, next_nl);
         from = next_nl + 1;
      } else {
         line = text.slice(from);
         from = -1;
      }
      linenr += 1;
      // if (linenr > 10) throw 'DEBUGGING';
      if (!line || line[0] == '%') {
         // empty line or comment
      } else if (line[0] == '"') {
         adder.set_o_part(line.substr(1), linenr, true);
      } else {
         var match = line.match(line_re);
         if (match) {
            if (match[1]) {
               adder.set_s(match[1], linenr);
            }
            if (match[3]) {
               adder.set_p(match[3], linenr);
               if (match[5] !== undefined) {
                  adder.set_kind(match[5]);
                  if (match[6]) {
                     adder.set_o_part(match[6]);
                  }
               }
            }
         }
      }
   }
   adder.submit_last();
   adder.submit_last();
   adder.submit_last();
   return [prefix, replace];
}

mi.mirdf3._test = function() {
   var str = `# mirdf:version "3
s:1 p:1 o:1
s:2
 p:2 o:o2
 p:3 \"oo3
s:3 p:4
"oo5:1
"oo5:2
 p:5  "oo6
s:4  p:6 -"oo7 rm
 p:7   -o:8rm
 p:8-
s:5 p:9 -"sdf
"sdf
 p:10 "df
s:6 p:11 o:9
  p:12 "o:10 str
  p:13   o:11
s:7 p:14 o:12`;
   console.log('input string:');
   console.log(str);
   console.log('parsed statements:');
   mi.mirdf3.reader(
      function(stmt){console.log('add', stmt)},
      function(stmt){console.log('rm', stmt)},
      str,
      'test:',
      {
         'test:': 'http://example.org',
         's:': 'http://example.org/s',
         'p:': 'http://example.org/p',
         'o:': 'http://example.org/o',
      },
      {}
   );
}
