/*
Data reader for compact indented rdf format
20161106
*/
'use strict';
mi = typeof(mi)!='undefined' ? mi : {};
mi.mirdf2 = mi.mirdf2 ? mi.mirdf2 : {};

mi.mirdf2.HEADER = '# mirdf:version"2';

/**
 * Handy object handling prefixes
 */
mi.mirdf2.Mirdf2 = function(cb_add_stmt, prefix, default_prefix) {
   var $ = this;
   $.default_prefix = default_prefix;
   $.prefix = (prefix === undefined) ? {} : prefix;
   $.replace = {};

   this.read = function(text, default_prefix) {
      mi.mirdf2.reader(cb_add_stmt, text,
                     default_prefix || $.default_prefix, $.prefix, $.replace);
   }

   this.expand_prefix = function(resource) {
     for (var prefix in $.prefix) {
       var len = prefix.length;
       if (resource.substr(0, len) == prefix) {
         return $.prefix[prefix] + resource.substr(len);
       }
     }
   }
}

/**
 * Read a text string formated in mirdf2 format
 * @param cb_add_stmt: called on each found statement, with it as parameter
 * @param default_prefix: What to replace default namespaces with (#)
 * @param default_url: Default namespace url
 * @param prefix: optional - dictionary with prefixes as keys and their matching url
 * @param replace: optional - dictionary with prefixes to replace. Key is replaced.
 */
mi.mirdf2.reader = function(cb_add_stmt, text,
                            default_prefix, prefix, replace) {
   var S = 0;
   var P = 1;
   var O = 2;
   var R = 3;

   if (prefix === undefined) {
      prefix = {};
   }
   if (replace === undefined) {
      replace = {};
   }
   if (default_prefix && prefix[default_prefix] === undefined) {
      throw "Default prefix [" + default_prefix + "] isn't a defined prefix";
   }

   var header = text.substr(0, mi.mirdf2.HEADER.length);
   if (header != mi.mirdf2.HEADER) {
      throw "Not mirdf2, doesn't start with [" + mi.mirdf2.HEADER +
            ']; \"' + header + '"';
   }
   var from = header.length;

   function pre_fix_rel(ref, linenr) {
      /*
      ref is first replaced if needed, then checked for unknown
      prefixes.
      @ref a namespaced reference
      @return - corrected ref
      */
      if (ref[0] == '#') {
         ref = default_prefix + ref.substr(1);
      } else if (ref != 'a') {
         // allow 'a' as a resource
         for (var rep in replace) {
            if (ref.startsWith(rep)) {
               ref = ref.replace(rep, replace[rep]);
            }
         }
         // warn if prefix is unknown
         var prefix_found = false;
         for (var pre in prefix) {
            if (ref.startsWith(pre)) {
               prefix_found = true;
               break;
            }
         }
         if (!prefix_found) {
            console.warn(
               'Undefined prefix for "' + ref + '" on line ' + linenr + ': ' + line);
         }
      }
      return ref;
   }

   function pre_fix(sss, ppp, ooo, rel, linenr, line) {
      /*
      If it is a prefix setter, 'mirdf:pre',
      update namespace prefix and maybe prefix replace table

      Else, check and maybe replace prefix of subject, predicate
      and object if it is a relation.
      */
      if (ppp == 'mirdf:pre') {
         // new prefix definition
         if (!(sss in prefix)) {
            // new prefix
            var matched_pre = false;

            for(var pre in prefix) {
               if (prefix[pre] == ooo) {
                  matched_pre = pre;
                  break;
               }
            }
            if (matched_pre) {
               // url stated before
               replace[sss] = matched_pre;
            } else {
               prefix[sss] = ooo;
            }
         } else {
            // old prefix
            if (prefix[sss] != ooo) {
               // redefinition of prefix
               var found = false;
               for (var i=2; i<100; i++) {
                  var new_prefix = String(i) + sss
                  if (!(new_prefix in prefix)) {
                     found = true;
                     replace[sss] = new_prefix;
                     prefix[new_prefix] = ooo;
                     break;
                  }
               }
               if (!found) {
                  throw "To many prelaced prefixes, handles up to 99 same names" +
                        " on line " + linenr + ': ' + line;
               }
            }
         }
      } else {
         // other statement
         ppp = pre_fix_rel(ppp, linenr, line);
         sss = pre_fix_rel(sss, linenr, line);
         if (rel) ooo = pre_fix_rel(ooo, linenr, line);
      }
      return [sss, ppp, ooo, rel];
   }

   var sss;
   var ppp;
   var ooo;
   var line;
   var linenr = 0;
   var re_split_one_delim_space = /^ ?([^ ]+)( (.*))?$/;
   // exec[1] is the first part, exec[3] is the rest or undefined
   var re_split_one_delim_cit = /^ ?([^"]+)"(.*)$/;
   // null or exec[1] is first part and exec[2] the rest
   while(from >= 0) {
      var next_nl = text.indexOf('\n', from);
      if (next_nl > 0) {
         line = text.slice(from, next_nl);
         from = next_nl + 1;
      } else {
         line = text.slice(from);
         from = -1;
      }
      linenr += 1
      if (line && line[0] == '"') {
         // object, set or add to
         if (ooo != undefined) {
            ooo += '\n' + line.substr(1);
         }
         else if (ppp) {
            ooo = line.substr(1);
         }
         else {
            throw "Object defined without propert, "
                + ' @ line ' + linenr;
         }
      }
      else {
         if (ooo != undefined) {
            cb_add_stmt(pre_fix(sss, ppp, ooo, false, linenr, line));
            ppp = undefined;
            ooo = undefined;
         }
         else if (ppp) {
            throw "No object defined for property, "
                + ' @ line ' + linenr;
         }

         if (line && line[0] != '%') {
            // skip empty lines and comments
            if (line[0] != ' ') {
               // new subject
               var parts = re_split_one_delim_space.exec(line);
               sss = parts[1];
               line = parts[3];
            }
            else {
               line = line.substr(1);
            }

            if (line) {  // not empty
               // new property
               if (!sss) {
                  throw "Property assignment before first subject, "
                        + ' @ line ' + linenr;
               }
               // p".* or p or p [^" ]+
               var parts = re_split_one_delim_cit.exec(line)
               if (parts) {
                  // string object on same row (may continue on next
                  ppp = parts[1];
                  ooo = parts[2];
               }
               else {
                  // try look for rel split (delimiter wasn't ")
                  var parts = re_split_one_delim_space.exec(line);
                  if (!parts) {
                     throw 'Cannot interpret line <' + line +
                           '> @ line ' + linenr;
                  }
                  ppp = parts[1];
                  if (parts[3]) {
                     ooo = parts[3];
                     // object as relation

                     cb_add_stmt(pre_fix(sss, ppp, ooo, true, linenr, line));
                     ppp = undefined;
                     ooo = undefined;
                  }
                  // else no object, next line must be an object str
               }
            }
         }
      }
   }
   if (ooo) {
      // add last stmt if any
      cb_add_stmt(pre_fix(sss, ppp, ooo, false, linenr, line));
   }
   return [prefix, replace];
}
