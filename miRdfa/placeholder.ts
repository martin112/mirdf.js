/// <reference path="miRdfRel.ts"/>
/// <reference path="definitions.ts"/>
/// <reference path="template.ts"/>





/*
Fix rearrangement after fixing path and render blocks backwards...

On trigger:
    first improvement:
    create updated tree

    second improvement:
    draw backwards
        last back to first


Add data:
    holds rendering (not triggering)
    mirdfa.add_spors([s,p,o,r])
        hold rendering (prevent triggering),
        then start rendering again

levels:
    template *->
        realisator *->
            sibling *->+
                match *->
                    realisation *->


realisation
    placeholder // place to add new children
    placeholder // contains link to what templates to link to depending on wathcer
        // wait for multiple watchers!
        match // matches to realise
        match // after selecting, sorting
        match // and limiting
            realisation


placeholder
    a place in the current DOM-tree where new children
    can be added.
    It is connected to the template/s (realisators) that
    can be put there


watching something:
    realisator.remove_watcher_cb =
        // resource.watchPath()
        miRdfRel.watchParsedPath(
            query,
            function(resource) {
                return _private.add_match(resource, realisator) -> onRenoveMatch;
            });
    addMatch()

*/

class Match {
    constructor() {}
    realise() {}
    unrealise() {}
    show() {}
    hide() {}
}
type relQueryPath = string[];
type miRdfRelParent = {
    watchParsedPath:<argType> (
        path: relQueryPath,
        addCb: (
            match: resource,
            arg: argType,
            ) => // on add cb
            (arg: argType) => void, // on removed cb
        arg: argType,
        ) => () => void; // remove cb
}
type cbArgSiblingsPrioNr = {nr: number};
class Placeholder {
    /*
    Placeholder
    -----------
    Connects the location where in the DOM-tree a template can be put(realised),
    with the path queries for.
    A placeholder can be used for multiple templates (siblings).

    It handles when to realise children, (`expand/retract`_) and when to `show/hide`_ them.
 
    If and how children are placed (in the DOM) is baded on selecting, sorting and limiting
    of children.    
    */
    node: HTMLElement;
    template: Templ;
    matches: Array<Set<Match>>;

    constructor(template: Templ, parentNode: HTMLElement, parentResource: miRdfRelParent) {
        this.template = template;
        this.node = template.findPlaceholderNode(parentNode);
        for (let siblingsPrioNr = 0; siblingsPrioNr < templateSiblings.length; ++siblingsPrioNr) {
            const sibling = templateSiblings[siblingsPrioNr];
            const remove_watcher_cb = parentResource.watchParsedPath<cbArgSiblingsPrioNr>(
                sibling.query,
                this.addMatchCb,
                { nr: siblingsPrioNr }
            );
        }
    }
    remove() {
        for (let matchgroup of this.matches) {
            for (let match of matchgroup) {
                match.unrealise();
            }
        }
        this.matches = [];
    }
    private addMatchCb(resource: resource, arg: cbArgSiblingsPrioNr) {
        // new Realisation / new Match -- don't show it, just calculate it and follow its children
        let match = new Match();  // resource
        this.show(); // not for existing matches in miRdfRel!!! Needs change in miRdfRel
        this.matches[arg.nr].add(match)
        return function() {
            match.unrealise()
            this.matches[arg.nr].delete(match);
        }
    }
    show() {
        // show/update visible matches - triggered by either match cb or by miRdfa.waitUpdateFor() ???
        // child first
        // select resources to show
        // sort resources
        // limit resources
    }
}

/*
        match[siblingsPrioNr] = ...
    show()
        // in order:
        1. select
        2. sort
        3. limit

        show(hide(limit(sort(select(matches)))

        hideMatches
        for siblingGroup in matches:
            for match in siblingGroup
                if matchResource not in selected_resources
                    add to selectedMatches
                else
                    add to hideMatches
        sort selectedMatches -> sortedMatches
        limit selectedMatches -> +hideMatches
        for match in hideMatches
            match.hide()
        for match in selectedMatches
            match.show()


/*

siblings:  !!!
    a template can have siblings
    siblings are ordered (in appearance in html)
    and the first sibling matching a resource is
    selected to be sorted,limited and finally realised





    activate_realisator(realisator, trigger=False/True)

interface Realisator {
    type: string;
    realised: bool;
}



*/


