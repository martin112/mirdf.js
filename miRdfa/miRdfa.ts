/// <reference path="../miRdfRel/miRdfRel.ts"/>
/// <reference path="definitions.ts"/>
/// <reference path="realisator.ts"/>
/// <reference path="template.ts"/>
/// <reference path="init.ts"/>

/* $doc:rst miRdfa

A template engine for making pages based on non-hierarchical data.

Inspired by rdfa lite, and heavily based on RDF_. In stead of using
hierarchical data (like json), it handles data of directed relations
(can be seen as documents or graphs), specifically RDF_data.
The idea is to use the same data format from server storage all the
way to the end view.

* Templates are written directly in html, making it easy to preview the result
* Changes to the underlying data is reflected directly in the view
* Queries of data are intuitive paths through the relational data
* The engine is written in typescript

Hello World
===========

.. code:: html

    <!DOCTYPE html>
    <head>
        <title>Hello miRdfa</title>
        <source href=miRdfa.js></source>
        <script>
            miRdfa.setupGlobal();
            let my_hello = miRdfRel.getAddRes('my:hello')
            let rdfs_label = miRdfRel.getAddRes('rdfs:label')
            my_hello.addProp(rdfs_label, "miRdfa world!")
        </script>
    </head>
    <article about=":hello">
        <h2 property="rdfs:label"></h2>
    </article>

MiRdfa Usage
============

Check `miRdfa templates`_ to see how to write the templates in html.

For a more indepth view, mostly for debugging miRdfa, check `miRdfa technical`_.

miRdfa data base
----------------
miRdfa is built around a data storage, and the ui updates with changes to this data.
The data is based on RDF_. See miRdfRel_ for how to manipulate the data. 

Initialize miRdfa
-----------------
The miRdfa.js engine needs to be initialised. It can either be setup globally (one instance
for the whole page) or one or more instances for specific parts of the page. But note that
the same data storage will be used for all instances. 

$doc:rst miRdfa templates

$doc:rst miRdfa technical
Details on how miRdfa is implemented. Good for developing and debugging miRdfa itself.

Internal structure
------------------

Realisators
-----------

Matches
-------

Nodes and html
--------------

Template components
-------------------

*/
namespace miRdfa {

    interface withMiRdfa extends Window {
        miRdfa: Root
    }
    declare let window: withMiRdfa
    /* $in:rst Initialize miRdfa
    Global
    ------
    When setting up globally, the inital miRdfa-object is replaced:

    .. code:: javascript

        miRdfa.setupGlobal(optional function(){ run when setup is done })
        miRdfa.update(function(){ wait with redraw until function is done *})
    */
    export function setupGlobal(whenDone?:()=>void) {
        window.addEventListener("DOMContentLoaded", function(event) {
            window.miRdfa = new Root(event.target as Element)
            if (whenDone) {
                window.miRdfa.update(whenDone)
            } else {
                window.miRdfa.show()
            }
        })
    }
    /* $in:rst Initialize miRdfa
    Instance
    --------
    For dymaically loaded pages or very big pages.
    
    Each instance behaves the same as the global "miRdfa" object
    after setupGlobal, and in most places in the documentation,
    the main "miRdfa" object can be replaced with your instance.

    .. code:: javascript
    
        let instance = miRdfa.setup(templateRootHtmlElement)
        instance.printTemplate()
    */
    export function setup(rootEl:Element): Root {
        let root = new Root(rootEl)
        root.show()
        return root
    }
    export class Root {
        public root: Element
        public template: lib.templateRoot
        public children: (lib.realisatorGroup|lib.realisatorFixed)[]
        private isPaused: boolean
        debugId: string = 'R'
        constructor(rootEl:Element) {
            this.children = []
            this.template = init.templify(rootEl)
            this.root = this.template.rootNode
            for (let tpl of this.template.children) {
                let child = lib.newRealisator(this, tpl as any, [], undefined, `${this.debugId}.${this.children.length}`)
                this.children.push(child)
                child.activate()
                child.fixNode(this.root)
            }
            for (let realisator of this.children) {
                realisator.activate()
            }
        }
        status (): string {
            if (this.isPaused) return 'paused'
            return 'visible'
        }
        public show() {
            // implicit turn off pause if set
            this.isPaused = false
            for (let child of this.children) child.show()
        }
        public paused() {
            return this.isPaused
        }
        /* $in:rst MiRdfa Usage
        For bigger updates
        ------------------
        If you change a lot, it goes quicker if you use the update-function. It will
        pause redrawing until the function is done:

        .. code:: javascript

            miRdfa.update(function(){ miRdfRel.addRes(.... })
        */
        public update(func:()=>any) {
            this.isPaused = true  // pause any update until done
            try {
                func()                
            } catch (error) {
                console.error('miRdfa.update function failed')
                console.trace(error)
            }
            this.show()  // start showing things again
        }
        /* $in:rst Initialize MiRdfa
        Print template
        --------------
        When an miRdfa instanse is created, it will print out the template structure to the console.
        It is done with this function:

        .. code:: javascript

            miRdfa.printTemplate()
        */
        public printTemplate() {
            lib.printTemplate(this.template)
        }
        public arranger = arranger
        public stats = stats
    }

    export function stats() {
        // show statistics to check for memory leaks
        return {
            'watchers': watcher._watchersCount,
            // refcounters doesn't work well when you never decrease them
            //'realisatorGroups': lib.realisatorGroup.count,
            //'realisatorResources': lib.realisatorResource.count,
            //'matchResources': lib.matchResource.count,
        }
    }
    /* $in:rst mirdfa usage
    arranger
    --------
    See `Sort and limit`_ for more general information.
    
    miRdfa has an arranger function included, that can sort and limit. It takes some optional arguments.

    Possible arguments are:
    
    :limit: Number of entries to show
    :desc: Set to true to sort smallest first
    :on: List of properties to sort on

    .. code:: javascript

        miRdfa.arranger({'limit': 5, 'descend':false})
    */
    export function arranger(matches: (lib.matchResource|lib.matchProperty)[], arg:any)
            : (lib.matchResource|lib.matchProperty)[] {
        let sortDescend = arg && ("descend" in arg ? arg["descend"] : false)
        let arrangeOn: string[]

        if (!arg?.arrangeOn) {
            arrangeOn = []
        }
        else if (arg.arrangeOn instanceof Array) {
            arrangeOn = arg.arrangeOn
        }
        else {
            arrangeOn = [arg.arrangeOn]
        }
        let toSort = matches.map(m=>{return {v: m.arrangeGet(sortDescend, ...arrangeOn), m: m}})
        toSort.sort(function(a, b){return a.v <= b.v ? -1 : 1});
        let limFrom = arg && ("from" in arg ? arg["from"] : undefined)
        let limTo = arg && ("to" in arg ? arg["to"] : undefined)
        matches = toSort.slice(limFrom, limTo).map(ts=>ts.m)
        console.debug(`miRdfa.arranger ${sortDescend ? 'descending ' : ''}from ${limFrom ? limFrom : 0} ${limTo ? `to ${limTo} ` : ''}`, matches)
        return matches
    }
}
