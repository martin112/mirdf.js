/// <reference path="definitions.ts" />
/// <reference path="../miRdfRel/resource.ts" />
/// <reference path="../miRdfRel/watcher.ts" />
/// <reference path="template.ts" />
/// <reference path="miRdfa.ts" />

/*
$in:rst internal structure

Under miRdfa, the instanses, realisations of the templates are placed.

miRdfa-instance.children[]
        realisator
            match[]
                realisator/realisator group...
        realisator group
            siblings[]
                realisator[]
                    match[]
                        realisator/realisator group...

$in:rst Realisators

The realisators are central to this library.  They takes a template, watch for any
data matching it's query and on a match, tells where to put the match. It also does
selecting, sorting and limiting on groups.

There are different type of realisators, depending on their underlying template, but they
all have similar traits.

Life cycle of realisators and their matches
-------------------------------------------


Activate/deactivate and show/hide are both separated to limit nr of redraws,
check `when to draw - dirty flag`_.

Watching data
-------------
Realisators watch data in miRdfa by watching paths.

paths are parsed when the html is templified, showing any obvious error directly.

Absolute paths are directly on miRdfa.watchParsedPath(), and for relative paths,
each resource is used, {resource}.watchParsedPath().

When watching anything, miRdfRel returns an unwatch callback which is used to
stop the monitoring when the realisastor is deactivated.


$in:rst Nodes and html

When to draw - dirty flag
-------------------------
To limit nr of redraws, each realisator has a dirty flag to check if it needs to be udpated.
On a change, *show* is triggered on affected realisator and trickles down to all it's children,
and those with a dirty flag set, will be reordered and redrawn.

$in:rst Debugging code

miRdfa.children/mathches[x]...constructor|clean|
*/
namespace miRdfa.lib {

    type matchVal = miRdfRel.Resource | string

    type status = 'deleted'|'inactive'|'noNode'|'hidden'|'dirty'|'visible'

    export function newMatch(realisator: realisatorResource, matchVal: matchVal, debugId:string): matchResource
    export function newMatch(realisator: realisatorProperty, matchVal: matchVal, debugId:string): matchProperty
    export function newMatch(realisator: realisatorResourceGrouped, matchVal: matchVal, debugId:string): matchResource
    export function newMatch(realisator: realisatorPropertyGrouped, matchVal: matchVal, debugId:string): matchProperty
    export function newMatch(realisator: realisatorPropertyValue, matchVal: matchVal, debugId:string): matchPropertyValue
    export function newMatch(realisator: realisatorAttribute, matchVal: matchVal, debugId:string): matchAttribute
    export function newMatch(realisator: any, matchVal: matchVal): any {
        try {
            switch(realisator.template.a) {
                case 'templateProperty':
                case 'templatePropertyGrouped':
                    return new matchProperty(realisator, matchVal)
                case 'templateResource':
                case 'templateResourceGrouped':
                    return new matchResource(realisator, matchVal)
                case 'templatePropertyValue':
                    return new matchPropertyValue(realisator, matchVal)
                case 'templateAttribute':
                    return new matchAttribute(realisator, matchVal)
                default:
                    console.error('newMatch: unknown template', realisator.template.a)
            }
        } catch (e) {
            // exception is printed out, that is enough
        }
    }

    export function newRealisator(root:miRdfa.Root, templ:templateGroup, parentResources:miRdfRel.Resource[], parentArrangedMatch:match<any,any>|undefined, debugId:string): realisatorGroup;
    export function newRealisator(root:miRdfa.Root, templ:templatePropertyValue, parentResources:miRdfRel.Resource[], parentArrangedMatch:match<any,any>|undefined, debugId:string): realisatorPropertyValue;
    export function newRealisator(root:miRdfa.Root, templ:templateAttribute, parentResources:miRdfRel.Resource[], parentArrangedMatch:match<any,any>|undefined, debugId:string): realisatorAttribute;
    export function newRealisator(root:miRdfa.Root, templ:templateResource, parentResources:miRdfRel.Resource[], parentArrangedMatch:match<any,any>|undefined, debugId:string): realisatorResource;
    export function newRealisator(root:miRdfa.Root, templ:templateProperty, parentResources:miRdfRel.Resource[], parentArrangedMatch:match<any,any>|undefined, debugId:string): realisatorProperty;    
    export function newRealisator(root:miRdfa.Root, templ:templateFixed, parentResources:miRdfRel.Resource[], parentArrangedMatch:match<any,any>|undefined, debugId:string): realisatorFixed;
    export function newRealisator(root:miRdfa.Root, templ:any, parentResources:miRdfRel.Resource[], parentArrangedMatch:match<any,any>|undefined, debugId:string):any {
        let realisator: realisatorGroup|realisatorResource|realisatorProperty|realisatorPropertyValue|realisatorFixed|realisatorAttribute
        switch(templ.a) {
            case 'templateGroup':
                realisator = new realisatorGroup(root, templ, parentResources, parentArrangedMatch, debugId)
                break
            case 'templateFixed':
                realisator = new realisatorFixed(root, templ, parentResources, parentArrangedMatch, debugId)
                break
            case 'templateResource':
                realisator = new realisatorResource(root, templ, parentResources, parentArrangedMatch, debugId)
                break
            case 'templateProperty':
                realisator = new realisatorProperty(root, templ, parentResources, parentArrangedMatch, debugId)
                break
            case 'templatePropertyValue':
                realisator = new realisatorPropertyValue(root, templ, parentResources, parentArrangedMatch, debugId)
                break
            case 'templateAttribute':
                realisator = new realisatorAttribute(root, templ, parentResources, parentArrangedMatch, debugId)
                break
            default:
                console.error('realisator: unhandled template', debugId, templ)
                return
        }
        return realisator
    }

    function debugPrefix(obj: any, func: string): string {
        return `${obj.debugId || '     '}  ${obj.constructor.name}.${func}`
    }

    abstract class Realisator<templT extends template> {
        readonly root: miRdfa.Root
        readonly template: templT
        readonly parentResources: miRdfRel.Resource[]  // 1: parent resource, 2: it's parent, 3...
        isActive: boolean
        clean: boolean = false
        debugId: string
        parentArrangedMatch: match<any,any>|undefined

        constructor(root:miRdfa.Root, templ:templT, parentResources:miRdfRel.Resource[], parentArrangedMatch:match<any,any>|undefined, debugId: string) {
            this.root = root
            this.template = templ
            this.parentResources = parentResources
            this.parentArrangedMatch = parentArrangedMatch
            this.debugId = debugId
        }
        dirty() {
            this.clean = false
        }
        abstract status(): status
        abstract activate(): void
        abstract fixNode(entryNode:Element): void
        abstract show(): void
        abstract hide(parentRemoved:boolean): void
        abstract deactivate(): void
    }
    abstract class RealisatorWithMatches
            <templT extends templateResource | templateProperty | templatePropertyValue | templateAttribute | templateResourceGrouped | templatePropertyGrouped,
             matchT extends matchResource | matchProperty | matchPropertyValue | matchAttribute >
            extends Realisator<templT> {
        readonly template: templT
        unwatch: undefined|watcher.unwatchCb        
        matches: matchT[] = []
        arrangerCb: arrangerCb|undefined
        arrangerArg: any

        activate() {
            console.debug(
                debugPrefix(this, 'activate'),
                this.isActive ? 'isActive' : 'activate')
            if (this.isActive) return
            this.isActive = true
            if ("arrangerCb" in this.template) {
                this.arrangerCb = this.template.arrangerCb
                this.arrangerArg = this.template.arrangerArg
            }
            this.setupMatcher()
        }
        setupMatcher () {
            let thisRealisator = this as any  // realisatorResource|realisatorProperty
            let onAddedMatch = function(matchVal:miRdfRel.Resource|string) {
                let matchObj = newMatch(thisRealisator, matchVal, `${thisRealisator.debugId}.${thisRealisator.matches.length}`)
                // console.debug(debugPrefix(thisRealisator, 'onAddedMatch'), thisRealisator.matches.length, matchObj)
                if (!matchObj) return
                thisRealisator.matches.push(matchObj as matchT)
                thisRealisator.dirty()
                thisRealisator.show()
                matchObj.activate()
                return function() {
                    matchObj.delete(true)
                }
            }

            if (this.template.query) {
                let query = fixDots<miRdfRel.path>(
                        this.template.query,
                        this.parentResources[0] && this.parentResources[0].name,
                        this.parentResources[1] && this.parentResources[1].name,
                        this.parentResources[2] && this.parentResources[2].name,)
                if ("absolute" in query && query.absolute) {
                    // console.debug(debugPrefix(thisRealisator, 'setupMatcher Absolute'), miRdfRel.pathToString(query))
                    this.unwatch = miRdfRel.watchParsedPath(query, onAddedMatch)
                } else {
                    console.assert(this.parentResources.length > 0,
                        `Relative template with query ${miRdfRel.pathToString(query)} is missing parent resource`)
                    let parentResource = this.parentResources[0] as miRdfRel.Resource
                    // console.debug(debugPrefix(thisRealisator, 'setupMatcher Relative'), parentResource.name, miRdfRel.pathToString(query))
                    this.unwatch = parentResource.watchParsedPath(query, onAddedMatch)
                }
            } else {
                // empty path means this resource
                console.assert(this.parentResources.length > 0,
                    'RealisatorWithMatches with empty query is missing a parent resource')
                let parentResource = this.parentResources[0] as miRdfRel.Resource
                // console.debug(debugPrefix(thisRealisator, 'setupMatcher This'), parentResource)
                onAddedMatch(parentResource)
            }
        }
        setArranger(cb:arrangerCb, arg:any) {
            this.arrangerCb = cb
            this.arrangerArg = arg
            this.dirty()
            this.show()
        }
    }
    // clone a path and replace dots with resources
    function fixDots<pathT extends miRdfRel.path>(
                query:pathT, res1dot:string, res2dots:string|undefined, res3dots:string|undefined): pathT {
        let step = {...query}  // copy step
        // Todo, check if we need to replace dots in more places
        switch("res" in step ? step.res : "") {
            case '.':   if (res1dot)  step.res = res1dot   ; break
            case '..':  if (res2dots) step.res = res2dots  ; break
            case '...': if (res3dots) step.res = res3dots  ; break
            default: break
        }
        if ("dotval" in step && step.dotval) {
            switch(step.val) {
                case '.':   if (res1dot)  step.val = res1dot   ; break
                case '..':  if (res2dots) step.val = res2dots  ; break
                case '...': if (res3dots) step.val = res3dots  ; break
                default: break
            }
        }
        if ("rest" in step && step.rest) {
            step.rest = fixDots<miRdfRel.pathRel|miRdfRel.pathFilter>(step.rest, res1dot, res2dots, res3dots)
        }
        return step
    }

    export class realisatorGroup extends Realisator<templateGroup> {
        private placeholderNode: Element|undefined = undefined
        private siblings: (realisatorResourceGrouped | realisatorPropertyGrouped)[] = []
        private shownMatches: (matchResource|matchProperty)[] = []
        arrangerCb: arrangerCb|undefined
        arrangerArg: any

        constructor(root:miRdfa.Root, templ:templateGroup, parentResources:miRdfRel.Resource[], parentArrangedMatch:match<any,any>|undefined, debugId:string) {
            super(root, templ, parentResources, parentArrangedMatch, debugId)
            if (this.template.arrangerCb) {
                this.arrangerCb = this.template.arrangerCb
                this.arrangerArg = this.template.arrangerArg
            }
        }
        status (): status {
            if (!this.isActive) return 'inactive'
            if (!this.placeholderNode) return 'noNode'
            if (!this.clean) return 'dirty'
            return 'visible'
        }
        activate () {
            if (this.isActive) return
            this.isActive = true
            for (let childTpl of this.template.siblings) {
                switch(childTpl.a) {
                    case "templateResourceGrouped": this.siblings.push(new realisatorResourceGrouped(this, childTpl, this.parentArrangedMatch, `${this.debugId}.${this.siblings.length}`)); break
                    case "templatePropertyGrouped": this.siblings.push(new realisatorPropertyGrouped(this, childTpl, this.parentArrangedMatch, `${this.debugId}.${this.siblings.length}`)); break
                    default:                        console.error('Bug, unhandled child template', childTpl)
                }
            }
            for (let sibling of this.siblings) {
                sibling.activate()
            }
        }
        fixNode (entryNode: Element) {
            console.debug(debugPrefix(this, 'fixNode'))
            if (this.template.position) {
                // standalone group
                // TODO: why do we add something to the position???
                let pos = this.template.position.slice()
                if (this.template.groupChildren) {
                    pos.push(0)
                }
                /* just trying to remove this
                 * else {
                    ++pos[pos.length-2]
                }*/
                this.placeholderNode = positionToNode(entryNode, pos)
            } else {
                // group in another template (a resource)
                if (this.template.groupChildren) {
                    if (!entryNode.children.length) {
                        console.error('Group has no children')
                    } else {
                        this.placeholderNode = entryNode.children[0]
                    }
                } else {
                    if (!entryNode.nextElementSibling) {
                        console.error('Group has no following sibling', this.template)
                    } else {
                        this.placeholderNode = entryNode.nextElementSibling
                    }
                }
            }
        }
        show () {
            console.debug(
                debugPrefix(this, 'show?'),
                this.root.paused() ? 'rootPaused' : '-',
                !this.isActive ? 'notActive' : '-',
                !this.placeholderNode ? 'noPlaceholder' : '-',
                this.clean ? 'clean' : 'dirty',
                `shownMatches:${this.shownMatches.length}`)
            if (this.root.paused()) return
            if (!this.isActive) return
            if (!this.placeholderNode) return // not yet set up - show will/must be called later
            if (!this.clean) {
                this.clean = true
                // Update selected resources (those that will be arranged below)
                let selectedMatches = selectMatches(this.siblings)
                this.shownMatches = SortLimitMatches(selectedMatches, this, this.arrangerCb, this.arrangerArg)
            }
            // call show on all visible matches
            // place each node before the placeholder node so they are inserted downwards
            for (let match of this.shownMatches) { match.show(this.placeholderNode) }
        }
        hide (parentRemoved:boolean) {
            console.debug(debugPrefix(this, 'hide'))
            for (let sibling of this.siblings) {
                let newMatches: any[] = []
                for (let match of sibling.matches) {
                    match.hide(parentRemoved)
                    if (!match.deleted) newMatches.push(match)
                }
                sibling.matches = newMatches
            }
        }
        deactivate () {
            console.debug(debugPrefix(this, 'deactivate'))
            for (let sibling of this.siblings) {
                sibling.deactivate()
            }
            this.siblings = []
            this.isActive = false
        }
    }

    function selectMatches
            (groups: (realisatorResourceGrouped|realisatorPropertyGrouped)[]):
            (matchResource|matchProperty)[] {
        let selectedValues: string[] = []
        let selectedMatches: (matchResource|matchProperty)[] = []
        for (let group of groups) {
            let updatedMatches: any[] = []
            for (let match of group.matches) {
                if (match.deleted) {
                    match.hide(false)
                } else {
                    updatedMatches.push(match)
                    let matchString = String(match.value)
                    if (selectedValues.indexOf(matchString) == -1) {
                        selectedValues.push(matchString)
                        selectedMatches.push(match)
                    } else {
                        match.hide(false)
                    }
                }
            }
            group.matches = updatedMatches;
        }
        return selectedMatches
    }
    function removeDeletedMatches
            <matchT extends matchResource|matchProperty>
            (matches: matchT[]): matchT[] {
        let updatedMatches: matchT[] = []
        for (let match of matches) {
            if (match.deleted) {
                match.hide(false)
            } else {
                updatedMatches.push(match)
            }
        }
        return updatedMatches
    }
    function SortLimitMatches
            (matches: (matchResource|matchProperty)[],
             realisator: (Realisator<any>),
             arrangerCb: arrangerCb|undefined, arrangerArg: any): (matchResource|matchProperty)[] {
        let showMatches: (matchResource|matchProperty)[]
        if (arrangerCb) {
            showMatches = arrangerCb(matches.slice(), realisator, arrangerArg)
                // hide those not in shownMatches
            for (let match of matches) {
                if (showMatches.indexOf(match) < 0) match.hide(false)
            }
        } else {
            showMatches = matches
        }
        return showMatches
    }

    export class realisatorResourceGrouped extends RealisatorWithMatches<templateResourceGrouped,matchResource> {
        private group: realisatorGroup
        constructor(group: realisatorGroup, templ: templateResourceGrouped, parentArrangedMatch:match<any,any>|undefined, debugId:string) {
            super(group.root, templ, group.parentResources, parentArrangedMatch, debugId)
            this.group = group
        }
        status (): status {
            if (!this.isActive) return 'inactive'
            return this.group.status()
        }
        dirty () {
             this.group.dirty()
        }
        fixNode (_entryNode: Element) {
            throw `BUG realisatorResource.fixNode called ${this.debugId}`
        }
        show () {
            // handled by realisatorGroup instead
            this.group.show()
        }
        hide (parentRemoved:boolean) {
            // handled by realisatorGroup instead            
            this.group.hide(parentRemoved)
        }
        deactivate () {
            console.debug(debugPrefix(this, 'deactivate'))
            this.isActive = false
            for (let match of this.matches) match.delete(false)
            if (this.unwatch) {
                this.unwatch()
                this.unwatch = undefined
            }
            // assume hide is called before
        }
    }

    export class realisatorPropertyGrouped extends RealisatorWithMatches <templatePropertyGrouped,matchProperty> {
        private placeholderNode: Element|undefined
        private group: realisatorGroup
        constructor(group: realisatorGroup, templ: templatePropertyGrouped, parentArrangedMatch:match<any,any>|undefined, debugId:string) {
            super(group.root, templ, group.parentResources, parentArrangedMatch, debugId)
            this.group = group
        }
        status (): status {
            if (!this.isActive) return 'inactive'
            return this.group.status()
        }
        fixNode (_entryNode: Element) {
            throw `BUG realisatorResource.fixNode called ${this.debugId}`
        }
        show () {
            // handled by realisatorGroup instead
            this.group.show()
        }
        hide (parentRemoved:boolean) {
            console.debug(debugPrefix(this, 'hide'), this.placeholderNode && true)
            this.group.hide(parentRemoved)
        }
        deactivate () {
            console.debug(debugPrefix(this, 'deactivate'), this.unwatch)
            for (let match of this.matches) match.delete(false)
            if (this.unwatch) {
                this.unwatch()
                this.unwatch = undefined
            }
            // assume hide is called before
        }
    }

    export class realisatorProperty extends RealisatorWithMatches <templateProperty,matchProperty> {
        private placeholderNode: Element|undefined
        private shownMatches: (matchResource|matchProperty)[] = []
        status (): status {
            if (!this.isActive) return 'inactive'
            if (!this.placeholderNode) return 'hidden'
            if (!this.clean) return 'dirty'
            return 'visible'
        }
        fixNode (entryNode: Element) {
            this.placeholderNode = positionToNode(entryNode, this.template.position)
        }
        show () {
            console.debug(
                debugPrefix(this, 'show?'),
                this.root.paused() ? 'root paused' : '-',
                !this.isActive ? 'not active' : '-',
                !this.placeholderNode ? 'no placeholder' : '-',
                this.clean ? 'clean' : 'dirty')
            if (this.root.paused()) return
            if (!this.isActive) return
            if (!this.placeholderNode) return // not yet set up - show will/must be called later
            if (!this.clean) {
                this.clean = true
                this.matches = removeDeletedMatches(this.matches)
                this.shownMatches = SortLimitMatches(this.matches, this, this.arrangerCb, this.arrangerArg)
            }
            // call show on all visible matches
            // place each node before the placeholder node so they are inserted downwards
            for (let match of this.shownMatches) { match.show(this.placeholderNode) }
        }
        hide (parentRemoved:boolean) {
            console.debug(debugPrefix(this, 'hide'), this.placeholderNode && true)
            if (!this.placeholderNode) return
            let newMatches: matchProperty[] = []
            for (let match of this.matches) {
                match.hide(parentRemoved)
                if (!match.deleted) {
                    newMatches.push(match)
                }
            }
            this.matches = newMatches
            this.placeholderNode = undefined
        }
        deactivate () {
            console.debug(debugPrefix(this, 'deactivate'), this.unwatch)
            if (this.unwatch) {
                this.unwatch()
                this.unwatch = undefined
            }
            this.placeholderNode = undefined
            for (let match of this.matches) match.delete(false)
            // assume hide is called before
        }
    }

    export class realisatorResource extends RealisatorWithMatches<templateResource,matchResource> {
        private placeholderNode: Element|undefined
        private shownMatches: (matchResource|matchProperty)[] = []
        status (): status {
            if (!this.isActive) return 'inactive'
            if (!this.placeholderNode) return 'hidden'
            if (!this.clean) return 'dirty'
            return 'visible'
        }
        fixNode (entryNode: Element) {
            this.placeholderNode = positionToNode(entryNode, this.template.position)
        }
        show () {
            console.debug(
                debugPrefix(this, 'show?'),
                this.root.paused() ? 'rootPaused' : '-',
                !this.isActive ? 'notActive' : '-',
                !this.placeholderNode ? 'noPlaceholderNode' : '-',
                this.clean ? 'clean' : 'dirty',
                `shownNodes:${this.shownMatches.length}`)
            if (this.root.paused()) return
            if (!this.isActive) return
            if (!this.placeholderNode) return // not yet set up - show will/must be called later
            if (!this.clean) {
                this.clean = true
                this.matches = removeDeletedMatches(this.matches)
                this.shownMatches = SortLimitMatches(this.matches, this, this.arrangerCb, this.arrangerArg)
            }
            // call show on all visible matches
            // place each node before the placeholder node so they are inserted downwards
            for (let match of this.shownMatches) { match.show(this.placeholderNode) }
        }
        hide (parentRemoved:boolean) {
            console.debug(debugPrefix(this, 'hide'), this.placeholderNode && true)
            if (!this.placeholderNode) return
            let newMatches: (matchResource)[] = []
            for (let match of this.matches) {
                match.hide(parentRemoved)
                if (!match.deleted) {
                    newMatches.push(match)
                }
            }
            this.matches = newMatches
            this.placeholderNode = undefined
        }
        deactivate () {
            console.debug(debugPrefix(this, 'deactivate'))
            this.isActive = false
            for (let match of this.matches) match.delete(false)
            if (this.unwatch) {
                this.unwatch()
                this.unwatch = undefined
            }
            // assume hide is called somewhere else
        }
    }

    export class realisatorPropertyValue extends RealisatorWithMatches<templatePropertyValue, matchPropertyValue> {
        private parentNode: Element|undefined
        status (): status {
            if (!this.isActive) return 'inactive'
            if (!this.parentNode) return 'hidden'
            if (!this.clean) return 'dirty'
            return 'visible'
        }
        fixNode (entryNode: Element) {
            // console.debug('realisatorPropertyValue.fixNode', this.debugId)
            this.parentNode = entryNode
        }
        show () {
            console.debug(
                debugPrefix(this, 'show?'),
                this.root.paused() ? 'rootPaused' : '-',
                !this.isActive ? 'notActive' : '-',
                !this.parentNode ? 'noParentNode' : '-',
                this.clean ? 'clean' : 'dirty')
            if (this.root.paused()) return
            if (!this.isActive) return
            if (!this.parentNode) return // not yet set up - show will/must be called later
            if (!this.clean) {
                this.clean = true
                // handle sorting
                let text:string = ''
                for (let match of this.matches) {
                    text += match.html()
                }
                this.parentNode.innerHTML = text
                this.parentNode.setAttribute('property',
                    this.template.query ? miRdfRel.pathToString(this.template.query) : '')
            }
        }
        hide (parentRemoved:boolean) {
            if (!this.parentNode) return
            if (!parentRemoved) this.parentNode.innerHTML = ''
            this.parentNode = undefined
        }
        deactivate () {
            console.debug(debugPrefix(this, 'deactivate'), this.unwatch)
            if (this.unwatch) {
                this.unwatch()
                this.unwatch = undefined
            }
        }
    }

    export class realisatorFixed extends Realisator<templateFixed> {
        private children: (realisatorGroup|realisatorAttribute|realisatorFixed)[] = []
        private node: Element|undefined
        status (): status {
            if (!this.isActive) return 'inactive'
            if (!this.node) return 'hidden'
            if (!this.clean) return 'dirty'
            return 'visible'
        }
        activate() {
            if (this.isActive) return
            this.isActive = true
            for (let child of this.template.children) {
                let realisator = newRealisator(this.root, child as any, this.parentResources, this.parentArrangedMatch, `${this.debugId}.${this.children.length}`)
                this.children.push(realisator)
                if (this.node) {
                    realisator.fixNode(this.node)
                }
                realisator.activate()
            }
            // fixNode isn't called here, so children wont work
            this.clean = true
        }
        fixNode (entryNode: Element) {
            // TODO make more efficient, so this isn't called both before activate and in activate
            console.debug(this.debugId, 'realisatorFixed.fixNode')
            this.node = positionToNode(entryNode, this.template.position)
            for (let child of this.children) {
                child.fixNode(this.node)
            }
        }
        show () {
            console.debug(
                debugPrefix(this, 'show?'),
                this.root.paused() ? 'rootPaused' : '-',
                !this.isActive ? 'notActive' : '-',
                !this.node ? 'noNode' : '-',
                this.clean ? 'clean' : 'dirty')
            if (this.root.paused()) return
            if (!this.isActive) return
            if (!this.node) return  // not yet set up - show will/must be called later
            for (let child of this.children) child.show()
        }
        hide(parentRemoved:boolean) {
            if (this.node) {
                for (let child of this.children) {
                    child.hide(parentRemoved)
                }
            }
            this.node = undefined
        }
        deactivate () {
            console.debug(debugPrefix(this, 'deactivate'))
            for (let child of this.children) child.deactivate()
            this.isActive = false
        }
    }

    export class realisatorAttribute extends RealisatorWithMatches<templateAttribute, matchAttribute> {
        node: Element|undefined
        status (): status {
            if (!this.isActive) return 'inactive'
            if (!this.node) return 'noNode'
            if (!this.clean) return 'dirty'
            return 'visible'
        }
        fixNode (entryNode: Element) {
            // 'realisatorAttribute.fixNode', this.debugId)
            // console.debug(debugPrefix(this, 'fixNode'), this.template.attr, this.template.query && miRdfRel.pathToString(this.template.query), this.template.pattern)
            this.node = entryNode
        }
        show () {
            console.debug(
                debugPrefix(this, 'show?'),
                this.root.paused() ? 'rootPaused' : '-',
                !this.isActive ? 'notActive' : '-',
                this.clean ? 'clean' : 'dirty',
                !this.node ? 'noNode' : '-',
                this.template.attr,
                this.template.query && miRdfRel.pathToString(this.template.query),
                this.template.pattern,
                `matches:${this.matches.length}`)
            if (this.root.paused()) return
            if (!this.node) return  // not yet setup - show will/must be called later
            if (!this.isActive) return
            if (!this.clean) {
                this.clean=true
                let newMatches:matchAttribute[] = []
                for (let match of this.matches) {
                    if (match.deleted) {
                        match.hide(false)
                    } else {
                        match.show(this.node)
                        newMatches.push(match)
                    }
                }
                this.matches = newMatches
            }
        }
        hide (parentRemoved:boolean) {
            this.clean = false
            for (let match of this.matches) {
                match.hide(parentRemoved)
            }
        }
        deactivate () {
            this.isActive = false
            for (let match of this.matches) {
                match.delete(true)
            }
            this.show()
            this.clean = false
        }
    }

    export abstract class match
            <realisatorT extends realisatorPropertyGrouped|realisatorProperty|realisatorResourceGrouped|realisatorResource|realisatorAttribute|realisatorPropertyValue,
             matchT extends miRdfRel.Resource|string> {
        readonly realisator: realisatorT
        debugId: string
        private arrangeOn: Record<string,string[]> = {}
        value: matchT
        isActive = false

        constructor(realisator:realisatorT, value: matchT) {
            this.debugId = `${realisator.debugId}.${realisator.matches.length}`;
            this.realisator = realisator
            this.value = value
            this.arrangeOnAdd(this)
        }
        activate() {
            this.isActive = true
        }
        abstract status(): status
        abstract delete(triggerShow: boolean): void
        toString(): string {
            return this.value.toString()
        }
        arrangeOnMatch(): match<any,any>|undefined {
            if ("arrangerCb" in this.realisator.template && this.realisator.template.arrangerCb) {
                return this
            } else {
                return this.realisator.parentArrangedMatch
            }
        }
        arrangeOnAdd (match:match<any,any>) {
            if (match.realisator.template.arrangeOn != undefined) {
                let arrangedMatch = this.arrangeOnMatch()
                if (arrangedMatch === undefined) {
                    console.warn(`arrangeOn "${match.realisator.template.arrangeOn}"" without any parent arranger`);
                    return
                }
                let matches: string[];
                if (match.realisator.template.arrangeOn in arrangedMatch.arrangeOn) {
                    matches = arrangedMatch.arrangeOn[match.realisator.template.arrangeOn];
                } else {
                    matches = [];
                    arrangedMatch.arrangeOn[match.realisator.template.arrangeOn] = matches;
                }
                let val = String(match)
                if (!matches.includes(val)) {
                    matches.push(val)
                    matches.sort()
                }
            }
        }
        arrangeOnRm (match:match<any,any>) {
            if (match.realisator.template.arrangeOn != undefined) {
                let arrangedMatch = this.arrangeOnMatch()
                if (arrangedMatch) {
                    let matches = arrangedMatch.arrangeOn[match.realisator.template.arrangeOn]
                    let index = matches.indexOf(match.toString())
                    if (index !== undefined && index >= 0) {
                        matches.splice(index, 1)
                    }
                }
            }
        }
        arrangeGet(descend: boolean, ...arrangeOn: string[]): string {
            /*  Get a value to sort match on
                A match can be arranged on multiple attributes (arrangeOn)
                Each existing min or max value for an "arrangeOn"
                is added to string in order
            */
            let arrangeValue: string = ''
            for (let oneArrangeOn of arrangeOn) {
                let oneValues = this.arrangeOn[oneArrangeOn]
                if (oneValues && oneValues.length > 0) {
                    if (descend) {
                        arrangeValue += oneValues[oneValues.length - 1]
                    } else {
                        arrangeValue += oneValues[0]
                    }
                }
            }
            return arrangeValue
        }
    }

    abstract class matchWithMatches
            <realisatorT extends realisatorPropertyGrouped|realisatorProperty|realisatorResourceGrouped|realisatorResource,
            childT extends {deactivate:()=>void, activate:()=>void, template:template},
            matchT extends string|miRdfRel.Resource>  extends match<realisatorT, matchT> {
        children: childT[] = []
        node: Element|undefined
        public deleted: boolean = false
        isActive = false

        constructor (realisator: realisatorT, value: matchT) {
            super(realisator, value)
            this.arrangeOnAdd(this)
        }
        activate() {
            if (!this.isActive) {
                this.isActive = true
                for (let realisator of this.children) {
                    switch (realisator.template.a) {
                        case 'templateGroup':
                        case 'templatePropertyValue':
                        case 'templateProperty':
                        case 'templateResource':
                            if (!realisator.template.retracted) realisator.activate()
                            break;
                        default:
                            realisator.activate()
                    }
                }
            }
        }
        abstract show(entryNode:Element): void
        abstract hide(parentRemoved:boolean): void
        delete (triggerShow:boolean) {
            console.debug(debugPrefix(this, 'delete'),
                          this.value instanceof miRdfRel.Resource ? this.value.name : this.value)
            this.deleted = true
            this.arrangeOnRm(this)
            for (let child of this.children) {
                child.deactivate()
            }    
            this.realisator.dirty()
            if (triggerShow) this.realisator.show()
        }
    }

    export class matchResource
            extends matchWithMatches<
                realisatorResource|realisatorResourceGrouped,
                (realisatorGroup|realisatorFixed|realisatorPropertyValue|realisatorAttribute),
                miRdfRel.Resource> {
// TODO?       private isRetracted: boolean
        status (): status {
            if (this.deleted) return 'deleted'
            if (!this.isActive) return 'inactive'
            if (!this.node) return 'hidden'
            return 'visible'
        }
        constructor (realisator: realisatorResource|realisatorResourceGrouped, matchVal:miRdfRel.Resource|string) {
            if (!(matchVal instanceof miRdfRel.Resource)) {
                console.error('matchResource failed on query',
                    realisator.template.query ?
                    miRdfRel.pathToString(realisator.template.query) : '.',
                    `resulting in`, matchVal)
                throw `matchResource got a non-resource`
            }
            super(realisator, matchVal)
            console.debug(debugPrefix(this, 'constructor'), this.value.name)

            for (let child of this.realisator.template.children) {
                let childRealisator = newRealisator(
                    this.realisator.root,
                    child as any,  // problem with overloaded method
                    [this.value, ...this.realisator.parentResources],
                    this.arrangeOnMatch(),
                    `${this.debugId}.${this.children.length}`
                )
                this.children.push(childRealisator)
            }
            // realisator.dirty() - done in realisator.ts onAddedMatch - parent must know of this before it redraws
        }
        toggleRetractableChildren() {
            if (!this.node) {
                console.warn('Trying to toggle retractable children before node exists')
                return
            }
            for (let child of this.children) {
                if (child instanceof realisatorGroup) {
                    // && (!groupId || groupId == child.template.id) {
                    if (child.template.retractable) {
                        if (child.isActive) {
                            child.hide(false)
                            child.deactivate()
                        } else {
                            child.activate()
                            child.show()
                        }
                    }
                }
            }
        }
        show (placeBeforeNode:Element) {
            console.debug(
                debugPrefix(this, 'show?'),
                !this.node ? 'no node' : '-',
                `children:${this.children.length}`)
            console.assert(!this.deleted)
            let added = false
            if (!this.node) {
                added = true
                this.node = this.realisator.template.html.cloneNode(true) as Element
                if (this.realisator.template.query
                    && "absolute" in this.realisator.template.query
                    && this.realisator.template.query.absolute) {
                    this.node.setAttribute('about', this.value.name)
                } else {
                    this.node.setAttribute('resource', this.value.name)
                }
                for (let child of this.children) child.fixNode(this.node)
            }
            for (let child of this.children) child.show()
            let parentNode = placeBeforeNode.parentNode as Element  // must exist
            parentNode.insertBefore(this.node, placeBeforeNode);
            if (added && this.realisator.template.onAddedCb) {
                this.realisator.template.onAddedCb(
                    this.node, this, this.realisator.template.onAddedArg)
            }
        }
        hide (_parentRemoved:boolean) {
            console.debug(debugPrefix(this, 'hide'), this.node && true, this.children.length)
            if (!this.node) return
            let parentNode = this.node.parentNode as Element // must exist, else this.html is already detached
            for (let child of this.children) {
                child.hide(true)
            }
            parentNode.removeChild(this.node)
            this.node = undefined
        }
    }

    export class matchProperty
            extends matchWithMatches<
                realisatorProperty|realisatorPropertyGrouped,
                realisatorAttribute,
                string> {
        constructor (realisator:realisatorProperty|realisatorPropertyGrouped, matchVal:miRdfRel.Resource|string) {
            let value = (matchVal instanceof miRdfRel.Resource) ? matchVal.name : matchVal
            super(realisator, value)
            console.debug(debugPrefix(this, 'constructor'), value)

            for (let child of this.realisator.template.children) {
                let childRealisator = new realisatorAttribute(
                    this.realisator.root,
                    child,
                    this.realisator.parentResources,
                    this.arrangeOnMatch(),
                    `${this.debugId}.${this.children.length}`
                )
                this.children.push(childRealisator)
            }
            realisator.dirty()
        }
        status (): status {
            if (this.deleted) return 'deleted'
            if (!this.isActive) return 'inactive'
            if (!this.node) return 'hidden'
            return 'visible'
        }
        show (placeBeforeNode:Element) {
            console.debug(
                debugPrefix(this, 'show?'),
                !this.node ? 'no node': '-',
                `children:${this.children.length}`)
            console.assert(!this.deleted)
            let added = false
            if (!this.node) {
                added = true
                this.node = this.realisator.template.html.cloneNode(true) as Element
                this.node.innerHTML = this.value
                this.node.setAttribute('property',
                    this.realisator.template.query ? miRdfRel.pathToString(this.realisator.template.query) : '')
                for (let child of this.children) child.fixNode(this.node)
            }
            for (let child of this.children) child.show()
            let parentNode: Element = placeBeforeNode.parentNode as Element  // must exist
            parentNode.insertBefore(this.node, placeBeforeNode)
            if (added && this.realisator.template.onAddedCb) {
                this.realisator.template.onAddedCb(
                    this.node, this, this.realisator.template.onAddedArg)
            }
        }
        hide (parentRemoved:boolean) {
            console.debug(debugPrefix(this, 'hide'), this.node && true)
            if (!this.node) return
            let parentNode: Element = this.node.parentNode as Element  // must exist
            if (!parentRemoved) parentNode.removeChild(this.node)
            for (let child of this.children) {
                child.hide(true)
            }
            this.node = undefined
        }
    }

    export class matchPropertyValue extends match<realisatorPropertyValue, string> {
        constructor (realisator:realisatorPropertyValue, matchVal:miRdfRel.Resource|string) {
            let value = (matchVal instanceof miRdfRel.Resource) ? matchVal.name : matchVal
            super(realisator, value)
            console.debug(debugPrefix(this, 'constructor'), matchVal)
            realisator.dirty()
        }
        status (): status {
            if (!this.isActive) return 'inactive'
            return 'visible'
        }
        html () {
            return this.value
        }
        delete (_triggerShow: boolean) {
            console.warn(`matchPropertyValue.delete not implemented (tried to delete ${this.value})`)
        }
    }

    export class matchAttribute extends match<realisatorAttribute, string> {
        readonly realisator: realisatorAttribute
        private node: Element|undefined
        deleted: boolean

        constructor (realisator:realisatorAttribute, matchVal:miRdfRel.Resource|string) {
            let value = (matchVal instanceof miRdfRel.Resource) ? matchVal.name : matchVal
            super(realisator, value)
            this.deleted = false
            console.debug(debugPrefix(this, 'constructor'),
            this.realisator.template.query && miRdfRel.pathToString(this.realisator.template.query), this.realisator.template.attr, value)
            realisator.dirty()
        }
        status (): status {
            if (this.deleted) return 'deleted'
            if (!this.isActive) return 'inactive'
            if (!this.node) return 'hidden'
            return 'visible'
        }
        show (node:Element) {
            console.debug(
                debugPrefix(this, 'show'),
                this.realisator.template.attr,
                this.value)
            if (this.node) return
            this.node = node
            let pattern = this.realisator.template.pattern
            if (!pattern) {
                node.setAttribute(this.realisator.template.attr, '')
            } else if (pattern.indexOf('??') < 0) {
                node.setAttribute(this.realisator.template.attr, pattern)
            } else {
                node.setAttribute(this.realisator.template.attr, pattern.replace('??', this.value))
            }
        }
        hide (parentRemoved:boolean) {
            console.debug(debugPrefix(this, 'hide'))
            if (!this.node) return
            if (!parentRemoved) this.node.removeAttribute(this.realisator.template.attr)
            this.node = undefined
        }
        delete (_triggerShow: boolean) {
            console.debug(debugPrefix(this, 'delete'))
            this.deleted = true
            this.realisator.dirty()
            this.realisator.show()
        }
    }
}
