/// <reference path="definitions.ts"/>
/// <reference path="../miRdfRel/path.ts"/>
/// <reference path="realisator.ts"/>

/* $in:rst template components

Internaly the templates are stored as a tree (in a dictionary structure)::

    miRdfA
        template (root template)
            children: template/templateGroup
                templateGroup[]
                    children: sibling templates
                        template[]
                            children: ...
 
Instances keep a reference to their template and it's childrens.
refer to the templates
                    
Templates and instances of them are handled in two internal structures.

*/

namespace miRdfa.lib {

    export type position = number[]|undefined

    export type onAddedCb = (node:Element, match:matchResource|matchProperty, arg:any) => void
    export type onRemoveCb = (node:Element, arg:any) => void
    // export type onFixNodeCb = (node:Element, realisator:any) => void
    export type arrangerCb = (matches:(matchResource|matchProperty)[], realisator:any, arg:any) => (matchResource|matchProperty)[]

    type withQuery = {
        query: miRdfRel.path | undefined
    }
    export type withList = {
        retractable: boolean
        retracted: boolean
        arrangerCb: arrangerCb|undefined
        arrangerArg: any
    }
    type withPosition = {
        position: position
    }
    type withNode = {
        html: Element
        id: string
        onAddedCb: onAddedCb|undefined
        onAddedArg: any
        onRemoveCb: onRemoveCb|undefined
        onRemoveArg: any
    }
    type withContainerChildren = {
        children: (templateResource|templateProperty|templateGroup|templateFixed)[]
    }
    export function withContainerChildren(tpl:templateInit) {
        return["templateResource", "templateProperty", "templateGroup", "templateFixed"].includes(tpl.a)
    }
    type withValueChildren = {
        children: (templateAttribute|templatePropertyValue|templateGroup)[]
    }
    export function withValueChildren(tpl:templateInit) {
        return["templateAttribute", "templatePropertyValue"].includes(tpl.a)
    }
    type withAnyChildren = {
        children: (templateAttribute|templatePropertyValue|templateResource|templateProperty|templateGroup|templateFixed)[]
    }
    export function withAnyChildren(tpl:templateInit) {
        return ["templateRoot", "templateAttribute", "templatePropertyValue", "templateResource", "templateResourceGrouped", "templateProperty", "templateGroup", "templateFixed"].includes(tpl.a)
    }
    export function anyChild(childTpl:templateInit): boolean {
        return ["templateAttribute", "templatePropertyValue", "templateResource", "templateResourceGrouped", "templateProperty", "templateGroup", "templateFixed"].includes(childTpl.a)
    }

    type withArrangeOn = {
        arrangeOn: string|undefined
    }
    type withCloned = {
        cloned: string
    }

    export type templateRoot = 
        withContainerChildren & {
        a: "templateRoot"
        rootNode: Element
        knownTemplates: Record<string,templateResource|templateResourceGrouped|templateProperty|templatePropertyGrouped>
    }

    /* $in:rst Template components
    Template group
    --------------
    A group of multiple templates that are to be placed together. It could
    be f.ex. a representation of media of different types, where video, audio or images
    have separate templates, but are shown in the same list.

    The templates are prioritized in order, so if multiple templates match the same
    resource or value, the first one will be used.

    Arranging and sorting is done on all matches for all templates in the group.
    */
    export type templateGroup =
        withPosition &
        withList & {
        a: 'templateGroup'
        groupChildren: boolean
        siblings: (templateResourceGrouped|templatePropertyGrouped|templateCloneGrouped)[]  // templates in prio order
    }

    export type templateResourceGrouped =
        withQuery &
        withNode &
        withAnyChildren &
        withCloned & 
        withArrangeOn & {
        a: "templateResourceGrouped"
    }

    export type templateResource =
        withQuery &
        withPosition &
        withNode &
        withList &
        withAnyChildren &
        withArrangeOn &
        withCloned & {
        a: "templateResource"
    }

    export type templatePropertyGrouped =
        withQuery &
        withNode &
        withCloned &
        withArrangeOn & {
        a: "templatePropertyGrouped"
        children: templateAttribute[]
    }

    export type templateProperty =
        withQuery &
        withPosition &
        withNode &
        withList &
        withArrangeOn &
        withCloned & {
        a: "templateProperty"
        children: templateAttribute[]
    }

    export type templateFixed =
        withPosition &
        withAnyChildren & {
        a: "templateFixed"
    }

    export type templatePropertyValue =
        withQuery &
        withList &
        withArrangeOn & {
        a: "templatePropertyValue"
    }

    export type templateAttribute =
        withQuery & {
        a: "templateAttribute"
        retractable: false
        attr: string  // what node attribute to update
        pattern: string  // how to fill in the attribute (e.g. some<?>thing)
    }

    /* $in:rst Template components
    Clone template group
    --------------------
    This group is temporary during initialisation and is used to later create a resource
    or property template. Therefore it must encompass all possible required attributes
    for those templates.
    */
    export type templateCloneGrouped =
        withQuery &
        withNode &
        withArrangeOn & {
        a: "templateCloneGrouped"
        cloneId: string
    }
    export type templateClone =
        withQuery &
        withNode &
        withPosition &
        withList &
        withArrangeOn & {
        a: "templateClone"
        cloneId: string
    }

    export type template = templateRoot|templateGroup|templateResource|templateFixed|templateAttribute|templateProperty|templatePropertyValue|templatePropertyGrouped|templateResourceGrouped
    export type templateInit = template | templateClone | templateCloneGrouped

    function printCb(cb:(...args: any)=>any): string {
        let str = String(cb)
        return str.slice(0, str.indexOf('\n')-1)
    }

    function printOnCbs(tpl:withNode):string {
        return `${tpl.onAddedCb ? `onAdded:${printCb(tpl.onAddedCb)}` : ''}${tpl.onRemoveCb ? `onRemove:${printCb(tpl.onRemoveCb)}` : ''}`
    }

    export function printTemplate(tpl: template|templateClone|templateCloneGrouped) {
        let query = "query" in tpl && tpl.query ? miRdfRel.pathToString(tpl.query) : '.'
        switch(tpl.a) {
            case "templateRoot":
                console.group(`${tpl.a} ${tpl.rootNode.tagName} ${tpl.rootNode.id || ''}`)
                tpl.children.forEach(printTemplate)
                console.groupEnd()
                break

            // group

            case "templateGroup":
                console.group(`${tpl.position ? '[' + tpl.position + '] ': ''}${tpl.a} `
                    + `${tpl.retractable ? (tpl.retracted ? '+' : '-') : ''} `
                    + `${tpl.arrangerCb ? 'arranger ' : ''}`)
                tpl.siblings.forEach(printTemplate)
                console.groupEnd()
                break
            case "templateResourceGrouped":
                console.group(`${tpl.a} ${tpl.html.tagName} ${query} ${tpl.html.id || ''} ${printOnCbs(tpl)}`)
                if (tpl.cloned) {
                    console.info(`cloned from id ${tpl.cloned}`)
                } else {
                    tpl.children.forEach(printTemplate)
                }
                console.groupEnd()
                break
            case "templatePropertyGrouped":
                console.group(`${tpl.a} ${tpl.html.tagName} ${query} ${printOnCbs(tpl)}`)
                if (tpl.cloned) {
                    console.info(`cloned from id ${tpl.cloned}`)
                } else {
                    tpl.children.forEach(printTemplate)
                }
                console.groupEnd()
                break

            // containers

            case "templateResource":
                console.group(`[${tpl.position}] ${tpl.a} ${tpl.html.tagName} ${query} ${tpl.html.id || ''} ${printOnCbs(tpl)}`)
                if (tpl.cloned) {
                    console.info(`cloned from id ${tpl.cloned}`)
                } else {
                    tpl.children.forEach(printTemplate)
                }
                console.groupEnd()
                break
            case "templateProperty":
                console.group(`[${tpl.position}] ${tpl.a} ${query} ${tpl.arrangeOn ? 'arrangeOn ' : ''} + ${printOnCbs(tpl)}`)
                if (tpl.cloned) {
                    console.info(`cloned from id ${tpl.cloned}`)
                } else {
                    tpl.children.forEach(printTemplate)
                }
                console.groupEnd()
                break
            case "templateFixed":
                console.group(`[${tpl.position}] ${tpl.a}`)
                tpl.children.forEach(printTemplate)
                console.groupEnd()
                break

            // values

            case "templatePropertyValue":
                console.group(`${tpl.a} ${query} ${tpl.arrangeOn ? 'arrangeOn ' : ''}`)
                console.groupEnd()
                break
            case "templateAttribute":
                console.group(`${tpl.a} ${tpl.attr} ${query} ${tpl.pattern}`)
                console.groupEnd()
                break
            default:
                // clone templates should never occur here
                console.error('Unknown template', tpl)
        }
    }

    /* $in:rst placeholder node
    node position
    -------------
    When a temlate is copied and prepared for the dom, we need to find where child elements
    should be added. To find out this, each template has a node position,
    an array of numbers. Each entry specifying witch child of that node to follow.
    There are two helper functions to handle this::

        nodeToPosition(fromParent, toChild)
        nodeFromPosition(fromParent, nodePosition)
    */
    export function positionFromNodes(fromParent:Element, toChild:Element): position|false {
        let position: position = []
        let node: Element

        // check if it is the parent to child
        node = toChild
        while (node && node!=fromParent) node = node.parentNode as Element;
        if (node != fromParent) {
            return false
        }

        node = toChild
        while ( node && node!=fromParent && node.parentNode ) {
            for ( let i=0; i<node.parentNode.children.length; ++i ) {
                if ( node.parentNode.children[i]==node ) {
                    position.unshift( i )
                    break
                }
            }
            node = node.parentNode as Element
        }
        return position
    }

    export function positionToNode(fromParent:Element, position:position): Element {
        let node: Element = fromParent
        if (!position) return node
        for ( let pos of position) {
            if ( ! node.children[pos] ) {
                console.error('positionToNode found no node', position, fromParent);
            }
            node = node.children[ pos ];
        }
        return node
    }
}
