/* $in:rst miRdfa templates

Templates are written in html attributes partly based on the standard
*RDFa* attributes.

The template watches for data in the miRdfRel_ store and update the content from that.
This is done with query paths.

There are a couple of different template types:

:resource container: Added for each resource matching a query.
:propertyValue: Add content (innerHTML) to an element.
:attribute: Add attributes to an element.

Basic example
=============
A template typically points out a resource or a group of them, and then
adding properties and relations to it::

    <article about="some:Group/-a">
        <h1 property="rdfs:label"></h1>
        <p property="rdfs:comment">any content will be replaced for the property value</p>
        <ul>
            <li rel="some:rel" property="rdfs:label"/>
        </ul>
    </article>

Data for this could look like::

    my:resource1
        a some:Group
        rdfs:label "resource 1
        rdfs:comment "And here is a comment for it

    my:resource2
        a some:Group
        rdfs:label "resoure 2
        rdfs:comment "second resource
        some:rel my:resource1

Querying data
=============
The expressions that are used to specify data are query paths. See `miRdfRel Query path`_
for a more complete description.

Each entry in the path is a relation or a filter. The path parts are separated with '/'.
In most cases, it is only a single entry, a resource or a relation.

If it is in an *about*-attribute it must be an absolute path, starting with '/' and a resource
(can be a specific resource or a resource type for futher selections).

For other entries, it is a relative path, based on the parent (miRdfRel) resource.

Filters are in the form <property><operator><value>, where operator is f.ex. ``==``, ``!=``, ``=!``, ``=*``.

Examples::

    /:nav                   # absolute, the resource :nav
    /some:Type/-a           # absolute, all resources of type "some:Type"
    rdfs:label              # property, the label of a resource
    some:value==selected    # filter
    /:Entry/-a/rdfs:label=* # all entries with one or more ``rdfs:label``

Dots
----

It is possible to replace parts of the step with dots, expressing current or parent resources.
One dot, for current resource, two for the parent resource, etc.

Template types and attributes
=============================

Container template
------------------

Value or attribute template
---------------------------

Sort and limit
==============
To sort and limit entries in templates the attributes arranger and arrangeOn are used.

Arranger is a `callback attribute`_ and arrangeOn is an id to be used in the arranger callback.

Matches are first selected, see `Groups or siblings`_ and than passed on to the arranger method given
with the arrange attribute.

ArrangeOn is used to arrange on content (properties) of other elements.

Check the miRdfa included àrranger`_ function for how it works.

*/
/// <reference path="template.ts"/>

namespace miRdfa.init {

    type nodeData = {
        id: string
        clone: string|undefined
        isResourceTemplate: boolean
        rel: string|undefined
        about: string|undefined
        resource: string|undefined
        property: string|undefined
        attributes: string|undefined
        group: boolean
        retracted: boolean
        retractable: boolean
        arrangeOn: string|undefined
        arrangerCb: lib.arrangerCb|undefined
        arrangerArg: any|undefined
        onAddedCb: lib.onAddedCb|undefined
        onAddedArg: any|undefined
        onRemoveCb: lib.onRemoveCb|undefined
        onRemoveArg: any|undefined
    }

    type initTree = {
        // Tree of templates that can include clones and other tweaks
        // used to build a stricter template tree to return from init
        template: lib.templateInit,
        children: initTree[],
        node: Element,
    }

    // loop through all elements, find templates and convert them
    export function templify(root:Element|Document): lib.templateRoot {
        let rootNode: Element = "body" in root ? root.body : root
        let templateRoot: lib.templateRoot = {
            a: "templateRoot",
            rootNode: rootNode,
            children: [],
            knownTemplates: {}
        }

        let initRootTemplate: initTree = {
            template: templateRoot,
            children: [],
            node: rootNode,
        }

        let parents: initTree[] = [initRootTemplate]

        for (let node of rootNode.querySelectorAll(
                "[about],[resource],[rel],[property],[attribute],[retracted],[arranger],[group]")) {
            let data = extractNodeAttributes(node)

            let [position, parent] = findParentAndPosition(parents, node)

            if (parent.template.a == "templateGroup") {
                // try put template in group
                let templateFromNode = templifyNodeGrouped(node, data)
                if (templateFromNode) {
                    let grouped = {
                        template: templateFromNode,
                        children: [],
                        node: node,
                    }
                    parent.children.push(grouped)
                    switch(templateFromNode.a) {
                        case "templateResourceGrouped":
                        case "templatePropertyGrouped":
                            templateRoot.knownTemplates[templateFromNode.id] = templateFromNode
                            parents.push(grouped)
                    }
                    continue
                }
                console.error('Skipping putting template in group', node, parent.template)
            }
            // try templify node - result in zero or more templates, first template is parent to the rest
            let tpls = templifyNode(node, data, position)
            let firstChild: initTree|undefined = undefined
            for (let childTemplate of tpls) {
                let child = {
                    template: childTemplate,
                    node: node,
                    children: [],
                }
                if (!firstChild) {
                    firstChild = child
                    parent.children.push(firstChild)
                } else {
                    firstChild.children.push(child)
                }
                switch(childTemplate.a) {
                    case "templateResource":
                    case "templateProperty":
                        templateRoot.knownTemplates[childTemplate.id] = childTemplate
                        // fall through
                    case "templateFixed":
                    case "templateGroup":
                        parents.push(child)
                }
            }
        }
        sanitizeInitTemplate(initRootTemplate, templateRoot)
        lib.printTemplate(templateRoot)
        return templateRoot
    }

    // create and populate templates

    /*
    Take a list of potential parents (node parents or previous sibling in case of groups)
    Calculate the position to it.

    The returend parent node must be either a parent, or a prevoius sibling in a group.

    Return position and parent template

    Siblings have all previous elements including parent node as siblings, start with a templateGroup
    */
    function findParentAndPosition(parents: initTree[], node:Element): [lib.position, initTree] {
        // let rootTemplate = parents[0][1]  // root
        while (parents.length) {
            let potential = parents[parents.length-1];
            if (potential.template.a == "templateGroup") {
                if (checkPotentialGroup(potential, node)) {
                    return [undefined, potential]
                }
            } else {
                let position = lib.positionFromNodes(potential.node, node)
                if (position) {
                    return [position, potential]
                }
            }
            parents.pop()
        }
        console.error('node found outside rootNode', node)
        throw 'Check template, node found outside rootNode. Possibly a missing end tag'
    }
    /* $in:rst  Template types and attributes
    Template Group
    ==============
    A couple of sibling nodes that are placed in the same list.
    See also `Template components`_.

    To create a template group:

    1. add a node with the group-attribute set to a node
    2. place one or more template nodes directly after or directly under this one
    3. they have to be resource or property templates.

    TODO: Currently there is an unspecified case that can cause weird issues
    If a group node has children, still nodes after that node will be pushed
    into the same group, causing errors or weird behaviour.

    TODO: groups in groups is untested, and probably unsupported. Is it needed?
    */
    function checkPotentialGroup(potential:initTree, node:Element): boolean {
        if (potential.template.a == "templateGroup") {
            // check if group is a previous sibling - and all previous siblings are in the group
            let sibling: Element|null = node
            while (sibling = sibling.previousElementSibling) {
                if (!potential.template.groupChildren && sibling == potential.node) return true
                if (sibling.tagName != "LINK") return false
            }
            // group is not a sibling, check if it is the parent
            if (potential.template.groupChildren && node.parentElement == potential.node) return true
        }
        return false
    }

    function templifyNode(node:Element, data:nodeData, position:lib.position): (lib.templateInit)[] {
        let res: (lib.templateInit)[] = []
        if (data.clone !== undefined) {
            return [getCloneTemplate(node, data, data.clone, position)]  // replaces node
        }
        let groupTemplate = getGroupTemplate(node, data, position)
        let propValTemplate = getPropertyValueTemplate(node, data)
        let attrTemplates = getAttributeTemplates(node, data)
        let propTemplate = getPropertyTemplate(node, data, position)  // replaces node
        let resTemplate = getResourceTemplate(node, data, position)  // replaces node

        if (resTemplate) {
            res.push(resTemplate)
            if (groupTemplate) {
                groupTemplate.position = undefined
                res.push(groupTemplate)
            }
            if (propValTemplate) {
                res.push(propValTemplate)
            }
            res.push(...attrTemplates)
            return res

        } else if (propTemplate) {
            res.push(propTemplate)
            res.push(...attrTemplates)
            return res

        } else if (attrTemplates.length) {
            let fixedTemplate: lib.templateFixed = {
                a: 'templateFixed',
                position: position,
                children: [],
            }
            res.push(fixedTemplate)
            if (groupTemplate) {
                groupTemplate.position = undefined
                res.push(groupTemplate)
            }
            if (attrTemplates.length) {
                res.push(...attrTemplates)
            }
            return res

        } else if (groupTemplate) {
            return [groupTemplate]
        }
        return []
    }

    function templifyNodeGrouped(node:Element, data:nodeData):
            lib.templateResourceGrouped|lib.templatePropertyGrouped|lib.templateCloneGrouped|undefined {
        if (data.clone !== undefined) {
            return getCloneGroupedTemplate(node, data, data.clone)  // replaces node
        }
        let propValTemplate = getPropertyValueTemplate(node, data)
        let attrTemplates = getAttributeTemplates(node, data)
        let propTemplate = getPropertyGroupedTemplate(node, data)  // replaces node
        let resTemplate = getResourceGroupedTemplate(node, data)  // replaces node

        if (!propTemplate && !resTemplate) return

        if (resTemplate) {
            if (propValTemplate) {
                resTemplate.children.push(propValTemplate)
            }
            resTemplate.children.push(...attrTemplates)
            return resTemplate

        } else if (propTemplate) {
            propTemplate.children.push(...attrTemplates)
            return propTemplate
        }
    }

    function getQueryFromData(data:nodeData): miRdfRel.path|undefined {
        let query: miRdfRel.path|undefined
        if (data.resource !== undefined) {
            if (data.resource) {
                if (data.resource[0] == '/') {
                    console.error('resource template path must be relative (not start with /)', data.resource)
                    return
                }
                query = miRdfRel.pathFromString(data.resource)
            }
        } else if (data.about !== undefined) {
            if (data.about) {
                if (data.about[0] != '/') {
                    console.error('about template path must be absolute (start with /)', data.about)
                    return
                }
                query = miRdfRel.pathFromString(data.about)
            }
        } else if (data.rel !== undefined) {
            query = miRdfRel.pathFromString(data.rel)
        }
        return query
    }

    function getCloneGroupedTemplate(node:Element, data:nodeData, cloneId:string): lib.templateCloneGrouped {
        var placeholderNode = document.createElement('link')
        placeholderNode.id = node.id
        let parentNode = node.parentNode as Element
        parentNode.replaceChild(placeholderNode, node);
        node.innerHTML = ''
        return {
            a: "templateCloneGrouped",
            cloneId: cloneId,
            query: getQueryFromData(data),
            html: node,
            id: data.id,
            arrangeOn: data.arrangeOn,
            onAddedCb: data.onAddedCb as lib.onAddedCb|undefined,
            onAddedArg: data.onAddedArg,
            onRemoveCb: data.onRemoveCb,
            onRemoveArg: data.onRemoveArg,
        }
    }
    function getCloneTemplate(node:Element, data:nodeData, cloneId:string, position:lib.position): lib.templateClone {
        return {
            ...getCloneGroupedTemplate(node, data, cloneId),
            a: "templateClone",
            position: position,
            retractable: data.retractable,
            retracted: data.retracted,
            arrangerCb: data.arrangerCb,
            arrangerArg: data.arrangerArg,
        }
    }
    function sanitizeInitTemplate(init: initTree, root: lib.templateRoot): lib.template|undefined {
        // console.debug('sanitizeInittemplate', init.template, init.children.length, init.node?.tagName)

        // fix clone
        let template : lib.template
        if (init.template.a == "templateClone" || init.template.a == "templateCloneGrouped") {
            let templateCloned = fixCloneTemplate(root, init.template)
            if (!templateCloned) return
            template = templateCloned
        } else {
            template = init.template
        }

        // check template specific needs
        switch(template.a) {
            // templates that must have children
            case "templateGroup":
            case "templateFixed":
                // todo: check child type
                if (!init.children.length) {
                    console.warn('Template must have children', init.node, template)
                }
                // pass through
            case "templateResource":
            case "templateProperty":
            case "templatePropertyGrouped":
            case "templateRoot":
            case "templateFixed":
            case "templateResource":
            case "templateResourceGrouped":
            case "templateGroup":
                for (let child of init.children) {
                    let childTemplate = sanitizeInitTemplate(child, root)
                    if (childTemplate) sanitizeInitAddChild(template, childTemplate)
                }
                break
            case "templateAttribute":
            case "templatePropertyValue":
                if (init.children.length) {
                    console.warn('Template can not have children', init.node, template)
                }
                break
            default:
                console.error('BUG unhandled template', template)
        }
        return template
    }
    function sanitizeInitAddChild(parentTemplate: lib.template, childTemplate: lib.template) {
        // make sure child fits in the parent template
        switch(parentTemplate.a) {
            case "templateProperty":
            case "templatePropertyGrouped":
                if (childTemplate.a == "templateAttribute") {
                    parentTemplate.children.push(childTemplate)
                    return
                }
                break
            case "templateRoot":
                switch (childTemplate.a) {
                    case "templateResource":
                    case "templateProperty":
                    case "templateGroup":
                    case "templateFixed":
                        parentTemplate.children.push(childTemplate)
                        return
                }
                break
            case "templateFixed":
            case "templateResource":
            case "templateResourceGrouped":
                switch (childTemplate.a) {
                    case "templateAttribute":
                    case "templatePropertyValue":
                    case "templateResource":
                    case "templateProperty":
                    case "templateGroup":
                    case "templateFixed":
                        parentTemplate.children.push(childTemplate)
                        return
                }
                break
            case "templateGroup":
                if (childTemplate.a == "templatePropertyGrouped"
                 || childTemplate.a == "templateResourceGrouped") {
                    parentTemplate.siblings.push(childTemplate)
                    return
                }
                break
            default:
                console.error('BUG unhandled template', parentTemplate)
        }
        console.warn(parentTemplate.a, 'cannot have children of type', childTemplate.a)
    }
    function fixCloneTemplate(
            templateRoot:lib.templateRoot, tpl: lib.templateInit)
            : lib.template|undefined {
        if (tpl.a == "templateCloneGrouped") {
            let cloneFromTemplate = templateRoot.knownTemplates[tpl.cloneId]
            if (!cloneFromTemplate) {
                console.warn('Clone id is unknown', tpl.cloneId)
            } else {
                if (cloneFromTemplate.a == "templateResource" || cloneFromTemplate.a == "templateResourceGrouped") {
                    return cloneTemplateResourceGrouped(cloneFromTemplate, tpl)
                } else if (cloneFromTemplate.a == "templateProperty" || cloneFromTemplate.a == "templatePropertyGrouped") {
                    return cloneTemplatePropertyGrouped(cloneFromTemplate, tpl)
                } else {
                    console.warn('Cannot clone from', cloneFromTemplate)
                }
            }
        } else if (tpl.a == "templateClone") {
            let cloneFromTemplate = templateRoot.knownTemplates[tpl.cloneId]
            if (!cloneFromTemplate) {
                console.warn('Clone id is unknown', tpl.cloneId)
            } else {
                if (cloneFromTemplate.a == "templateResource" || cloneFromTemplate.a == "templateResourceGrouped") {
                    return cloneTemplateResource(cloneFromTemplate, tpl)
                } else if (cloneFromTemplate.a == "templateProperty" || cloneFromTemplate.a == "templatePropertyGrouped") {
                    return cloneTemplateProperty(cloneFromTemplate, tpl)
                } else {
                    console.warn('Cannot clone from', cloneFromTemplate)
                }
            }
        }
        console.warn('BUG unhandled clone template', tpl)
    }
    function cloneTemplateResourceGrouped(fromTemplate:lib.templateResource|lib.templateResourceGrouped, cloneTemplate:lib.templateCloneGrouped): lib.templateResourceGrouped {
        cloneTemplate.html.innerHTML = fromTemplate.html.innerHTML
        return {
            a: "templateResourceGrouped",
            children: fromTemplate.children,
            query: cloneTemplate.query ? cloneTemplate.query : fromTemplate.query,
            html: cloneTemplate.html,
            id: cloneTemplate.id,
            cloned: cloneTemplate.cloneId,
            arrangeOn: cloneTemplate.arrangeOn ? cloneTemplate.arrangeOn : fromTemplate.arrangeOn,
            onAddedCb: cloneTemplate.onAddedCb ? cloneTemplate.onAddedCb : fromTemplate.onAddedCb,
            onAddedArg: cloneTemplate.onAddedCb ? cloneTemplate.onAddedArg : fromTemplate.onAddedArg,
            onRemoveCb: cloneTemplate.onRemoveCb ? cloneTemplate.onRemoveCb : fromTemplate.onRemoveCb,
            onRemoveArg: cloneTemplate.onRemoveCb ? cloneTemplate.onRemoveArg : fromTemplate.onRemoveArg,
        }
    }
    function cloneTemplateResource(fromTemplate:lib.templateResource|lib.templateResourceGrouped, cloneTemplate:lib.templateClone): lib.templateResource {
        cloneTemplate.html.innerHTML = fromTemplate.html.innerHTML
        let retractable = cloneTemplate.retractable
        let retracted = cloneTemplate.retracted
        let arrangerCb = cloneTemplate.arrangerCb
        let arrangerArg = cloneTemplate.arrangerCb
        return {
            a: "templateResource",
            children: fromTemplate.children,
            query: cloneTemplate.query ? cloneTemplate.query : fromTemplate.query,
            html: cloneTemplate.html,
            id: cloneTemplate.id,
            position: cloneTemplate.position,
            cloned: cloneTemplate.cloneId,
            arrangeOn: cloneTemplate.arrangeOn ? cloneTemplate.arrangeOn : fromTemplate.arrangeOn,
            onAddedCb: cloneTemplate.onAddedCb ? cloneTemplate.onAddedCb : fromTemplate.onAddedCb,
            onAddedArg: cloneTemplate.onAddedCb ? cloneTemplate.onAddedArg : fromTemplate.onAddedArg,
            onRemoveCb: cloneTemplate.onRemoveCb ? cloneTemplate.onRemoveCb : fromTemplate.onRemoveCb,
            onRemoveArg: cloneTemplate.onRemoveCb ? cloneTemplate.onRemoveArg : fromTemplate.onRemoveArg,
            retractable: retractable,
            retracted: retracted,
            arrangerCb: arrangerCb,
            arrangerArg: arrangerArg,
        }
    }
    function cloneTemplatePropertyGrouped(fromTemplate:lib.templateProperty|lib.templatePropertyGrouped, cloneTemplate:lib.templateCloneGrouped): lib.templatePropertyGrouped {
        cloneTemplate.html.innerHTML = fromTemplate.html.innerHTML
        return {
            a: "templatePropertyGrouped",
            children: fromTemplate.children,
            query: cloneTemplate.query ? cloneTemplate.query : fromTemplate.query,
            html: cloneTemplate.html,
            id: cloneTemplate.id,
            cloned: cloneTemplate.cloneId,
            arrangeOn: cloneTemplate.arrangeOn ? cloneTemplate.arrangeOn : fromTemplate.arrangeOn,
            onAddedCb: cloneTemplate.onAddedCb ? cloneTemplate.onAddedCb : fromTemplate.onAddedCb,
            onAddedArg: cloneTemplate.onAddedCb ? cloneTemplate.onAddedArg : fromTemplate.onAddedArg,
            onRemoveCb: cloneTemplate.onRemoveCb ? cloneTemplate.onRemoveCb : fromTemplate.onRemoveCb,
            onRemoveArg: cloneTemplate.onRemoveCb ? cloneTemplate.onRemoveArg : fromTemplate.onRemoveArg,
        }
    }
    function cloneTemplateProperty(fromTemplate:lib.templateProperty|lib.templatePropertyGrouped, cloneTemplate:lib.templateClone): lib.templateProperty {
        let retracted = cloneTemplate.retracted
        let retractable = cloneTemplate.retractable
        let arrangerCb = cloneTemplate.arrangerCb
        let arrangerArg = cloneTemplate.arrangerCb
        if (fromTemplate.a == "templateProperty") {
            if (!retractable) retractable = fromTemplate.retractable
            if (!cloneTemplate.retracted) retracted = fromTemplate.retracted
            if (!cloneTemplate.arrangerCb) arrangerCb = fromTemplate.arrangerCb
            if (!cloneTemplate.arrangerCb) arrangerCb = fromTemplate.arrangerArg
        }
        return {
            a: "templateProperty",
            children: fromTemplate.children,
            query: cloneTemplate.query ? cloneTemplate.query : fromTemplate.query,
            html: cloneTemplate.html,
            id: cloneTemplate.id,
            position: cloneTemplate.position,
            cloned: cloneTemplate.cloneId,
            arrangeOn: cloneTemplate.arrangeOn ? cloneTemplate.arrangeOn : fromTemplate.arrangeOn,
            onAddedCb: cloneTemplate.onAddedCb ? cloneTemplate.onAddedCb : fromTemplate.onAddedCb,
            onAddedArg: cloneTemplate.onAddedCb ? cloneTemplate.onAddedArg : fromTemplate.onAddedArg,
            onRemoveCb: cloneTemplate.onRemoveCb ? cloneTemplate.onRemoveCb : fromTemplate.onRemoveCb,
            onRemoveArg: cloneTemplate.onRemoveCb ? cloneTemplate.onRemoveArg : fromTemplate.onRemoveArg,
            retractable: retractable,
            retracted: retracted,
            arrangerCb: arrangerCb,
            arrangerArg: arrangerArg,
        }
    }

    function getGroupTemplate(node:Element, data:nodeData, position:lib.position): lib.templateGroup|undefined {
        if (!data.group) return
        return {
            a: "templateGroup",
            siblings: [],
            groupChildren: node.children.length > 0,
            position: position,
            retractable: data.retractable,
            retracted: data.retracted,
            arrangerCb: data.arrangerCb,
            arrangerArg: data.arrangerArg,
        }
    }

    function getResourceGroupedTemplate(node:Element, data:nodeData): lib.templateResourceGrouped|undefined {
        if (!data.isResourceTemplate) return

        var positioningNode = document.createElement('link')
        positioningNode.id = node.id
        let parentNode = node.parentNode as Element
        parentNode.replaceChild(positioningNode, node);

        return {
            a: "templateResourceGrouped",
            id: node.id,
            cloned: "",
            html: node,
            query: getQueryFromData(data),
            children: [],
            onAddedCb: data.onAddedCb ? data.onAddedCb as lib.onAddedCb:undefined,
            onAddedArg: data.onAddedArg,
            onRemoveCb: data.onRemoveCb,
            onRemoveArg: data.onRemoveArg,
            arrangeOn: data.arrangeOn,
        }
    }
    function getResourceTemplate(node:Element, data:nodeData, position:lib.position): lib.templateResource|undefined {
        let partly = getResourceGroupedTemplate(node, data)
        if (!partly) return

        return {
            ...partly,
            a: "templateResource",
            position: position,
            retractable: data.retractable,
            retracted: data.retracted,
            arrangerCb: data.arrangerCb,
            arrangerArg: data.arrangerArg,
        }
    }
    function getPropertyGroupedTemplate(
            node:Element, data:nodeData): lib.templatePropertyGrouped|undefined {
        if (data.property === undefined) return
        if (data.isResourceTemplate) return  // then it is a propertyValue
        let query: miRdfRel.path|undefined
        if (data.property) {
            if (data.property[0] == '/') {
                console.error('property template path must be relative (not start with /)', data.property)
                return
            }
            query = miRdfRel.pathFromString(data.property)
        }

        var positioningNode = document.createElement('link')
        positioningNode.id = node.id
        let parentNode = node.parentNode as Element
        parentNode.replaceChild(positioningNode, node);

        return {
            a: 'templatePropertyGrouped',
            query: query,
            html: node,
            id: data.id,
            cloned: "",
            children: [],
            arrangeOn: data.arrangeOn,
            onAddedCb: data.onAddedCb,
            onAddedArg: data.onAddedArg,
            onRemoveCb: data.onRemoveCb,
            onRemoveArg: data.onRemoveArg,
        }
    }
    function getPropertyTemplate(
        node:Element, data:nodeData, position: lib.position): lib.templateProperty|undefined {
        let partly = getPropertyGroupedTemplate(node, data)
        if (!partly) return
        return {
            ...partly,
            a: 'templateProperty',
            position: position,
            arrangeOn: data.arrangeOn,
            retractable: data.retractable,
            retracted: data.retracted,
            arrangerCb: data.arrangerCb,
            arrangerArg: data.arrangerArg,
        }
    }

    function getPropertyValueTemplate(
            node:Element, data:nodeData): lib.templatePropertyValue|undefined {
        if (data.property === undefined) return
        if (!data.isResourceTemplate) return  // then it is a property template
        let query: miRdfRel.path|undefined
        if (data.property) {
            if (data.property[0] == '/') {
                console.error('property template path must be relative (not start with /)', data.property)
                return
            }
            query = miRdfRel.pathFromString(data.property)
        }
        return {
            a: 'templatePropertyValue',
            query: query,
            arrangeOn: data.arrangeOn,
            arrangerCb: data.arrangerCb,
            arrangerArg: data.arrangerArg,
            retractable: data.retractable,
            retracted: data.retracted,
        }
    }

    function getAttributeTemplates(node:Element, data:nodeData): lib.templateAttribute[] {
        let attributes: lib.templateAttribute[] = []
        if (!data.attributes) return attributes

        for (var one_attribute of data.attributes.split('\n')) {
            var attribute_parts = one_attribute.trim().split(' ')
            if (attribute_parts[1] === undefined) {
                console.warn(`"attribute=${one_attribute}" is missing a query path`)
                continue
            }
            let query: miRdfRel.path|undefined;
            if (attribute_parts[1] == '.') {
                query = undefined
            } else {
                query = miRdfRel.pathFromString(attribute_parts[1])
            }
            attributes.push({
                'a': 'templateAttribute',
                'attr': attribute_parts[0],
                'query': query,
                retractable: false,                
                'pattern': attribute_parts.slice(2).join(' '),
            })
        }
        return attributes
    }

    // check and cut interesting node attributes from a node

    function cutAttributeString(node: Element, attribute:string): string|undefined {
        var attr = node.getAttribute(attribute);
        node.removeAttribute(attribute);
        if (attr == null) return undefined
        return attr;
    }
    function cutAttributeBoolean(node: Element, attribute:string): boolean {
        var attr = node.getAttribute(attribute);
        node.removeAttribute(attribute);
        if (attr == null) return false
        return true
    }
    /* $in:rst Template types and attributes
    Callback attribute
    ------------------
    In some cases, f.ex. when a node is added, or matches needs to be sorted,
    a function should be called. It is specified with a callback attribute.
    
    The format is::

        function arg
    
    Function must be an existing, global function. It is possible to use a
    function in an object (e.g. miRdfa.arranger).

    Any eventual *arg* is parsed by json and included in any call to the callback.
    */
    function cutAttributeCallback<cbT>(node: Element, attribute:string):
                [cbT|undefined, any|undefined] {
        let expression:string|undefined = cutAttributeString(node, attribute)
        if (!expression) return [undefined, undefined]
        let func_name = expression.split(' ', 1)[0]
        let func = window
        let parts = func_name.split('.')
        for (let part of parts) {
            func = func[part]
            if (func == undefined) {
                console.warn(`callback [${func_name}] does not exist`)
                return [undefined, undefined]
            }
        }
        let dummyobj = {}
        if (dummyobj.toString.call(func) !== '[object Function]') {
            console.warn(`callback [${func_name}] isn't a function`);
            return [undefined, undefined]
        }
        let arg_str = expression.substring(func_name.length + 1);
        let arg = arg_str ? JSON.parse(arg_str) : undefined
        return [func as unknown as cbT, arg]
    }

    /*
    $in:rst Nodes and html

    From html nodes attributes are cut out and used to define the templates.
    The actual html node is then cut from the dom-tree and also put in it's template.

    placeholder node
    ----------------
    To know where to add elements, html nodes are replaced with a dummy node,
    a meta-element.
    */

    /*
    $in:rst Template types and attributes
    List of attributes
    ------------------
    :rel: Query path for a `rel template`_
    :about: Query path for an `about template`_
    :resource: Query path for a `resource template`_
    :rel: See `rel template`_
    :group: See `template group`_
    :property: Query path for a `property template`_ or a `property value template`_
    :attributes: Attributes for a `attribute template`_
    :id: string id, used to refer it when cloning it
    :clone: string id of template to clone
    :arranger: Function to call to sort and limit matches, see `sort and limit`_, `callback attribute`_
    :arrangeOn: string id to be used as reference when sorting matches, see `sort and limit`_
    :retracted: Tell if a `container template`_ should not be activated at start
    :retractable: Tell if a `container template`_ should be possible to retract
    :onAdded: Called after a node is added, `callback attribute`_
    :onRemove: Called before a node is removed, , `callback attribute`_
    */
    function extractNodeAttributes(node:Element): nodeData {
        let [arrangerCb, arrangerArg] = cutAttributeCallback<lib.arrangerCb>(node, 'arranger')
        let [onAddedCb, onAddedArg] =
            cutAttributeCallback<lib.onAddedCb>(node, 'onAdded')
        let [onRemoveCb, onRemoveArg] = cutAttributeCallback<lib.onRemoveCb>(node, 'onRemove')
        let rel = cutAttributeString(node, 'rel')
        let about = cutAttributeString(node, 'about')
        let resource = cutAttributeString(node, 'resource')
        let retracted = cutAttributeBoolean(node, 'retracted') 
        return {
            id: node.id,
            clone: cutAttributeString(node, 'clone'),
            isResourceTemplate: rel !== undefined || about !== undefined || resource !== undefined,
            rel: rel,
            about: about,
            resource: resource,
            property: cutAttributeString(node, 'property'),
            attributes: cutAttributeString(node, 'attribute'),
            group: cutAttributeBoolean(node, 'group'),
            retracted: retracted,
            retractable: cutAttributeBoolean(node, 'retractable') || retracted,
            arrangeOn: cutAttributeString(node, 'arrangeOn'),
            arrangerCb: arrangerCb,
            arrangerArg: arrangerArg,
            onAddedCb: onAddedCb,
            onAddedArg: onAddedArg,
            onRemoveCb: onRemoveCb,
            onRemoveArg: onRemoveArg,
        }
    }
}
/* $in:rst Template types and attributes

Container template
==================
The query selects one or more resources.
Each matching resource creates an element (copied from the template).

about template
--------------
An absolute reference, does not care about any parent resource.

template::
    <article about="some:Type/-a">...</article>
    <article about=":nav">...</article>
result::
    <article about="resource1" type="some:Type">...</article>
    <article about=":nav">...</article>

resource template
-----------------
Query starts at parent resource.

template::
    <article about=":nav">
        <div resource=":entry">...</div>
    </article>
result::
    <article about=":nav">
        <div property=":entry" resource=":entry1">...</div>
        <div property=":entry" resource=":entry2">...</div>
    </article>

rel template
------------
*NOTE! not yet implemented*

Does not change context, meaning any relational lookup goes to parent resource.
But any dots_ are affected. It can be used for grouping.

Relations can be properties as well as resources???

template::
    <tag about="list">
        <tag rel="entry/type">
            <tag about="." property="rdfs:label"></tag>
            <tag resource="entry/type=.">
                ...
result::
    <tag about="list">
        <tag><!-- still resource1 context -->
            <tag about="someType" property="rdfs:label">Relation type</tag>
            <tag property="entry" resource="entry1">
            <tag property="entry" resource="entry2">

property template
-----------------
Get a specific property from a resource and put it as content of the element.
An element is created for each matching property.

template::

    <tag about="resource1">
        <tag property="mi:pref"/>

result::

    <tag about="resource1" type="some:Type">
        <tag property="mi:pref">resource1_mi:pref_value1</tag>
        <tag property="mi:pref">resource1_mi:pref_value2</tag>

Value or attribute template
===========================
These does not produce any new element, but act on existing ones.
Checks are related to parent resource. The parent resource can be stated
in the same element.

property value template
-----------------------
Update content (innerHTML) of an Element. In case of more matches, they are joined together.

An arranger can be used to limit to only one entry.

*NOTE! there should be a way of specifying how to join entries*

template::

    <h1 about=":nav" property="rdfs:label">

result::

    <h1 about=":nav" property="rdfs:label">First entrySecond entry</h1>

attribute template
------------------
Set attributes on an existing node. Multiple attributes are specified on different lines.
The matched value is inserted instead of any '??' if it exists.

The pattern is::

    <attribute name> <path> <content, optionally with ??>

    attribute="checked mi:selected
               title mi:rel/mi:pref2 Value where '??' is replaced with the value
               href this:url ??
               onclick . selected('??')"

Warning, only one attribute specification can be set to each attribute.
Specially it can complicate modifications of the class-attribute that will be overwritten.

*NOTE! If there are multiple matches, the behaviour isn't specified yet*

template::

    <A about="resource1" attribute="href some:url ??" property=rdfs:label/>

result::

    <A about="resource1" href="http://...." property="rdfs:label">Resource 1</A>

combining templates
--------------------
As seen in the above examples different template types can be combined.
The ones that cannot be combined are about, resource and rel.

When types are combined they are processed in the following order:

#. First the resource selector (about, resource or rel) to set context
#. Then property to make sure there is an element for each property value.
#. And lastly attributes are added to the elements.

*/
