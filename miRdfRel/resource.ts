/// <reference path="miRdfRel.ts" />
/// <reference path="watcher.ts" />
/// <reference path="definitions.ts" />

namespace miRdfRel {
    type _watchersAddRelToChange = {add:boolean, relName:string, objName:string}
    type _watchersAddRelFromChange = {add:boolean, relName:string, objName:string}

    export class Resource {

        /* $in:rst miRdfRel Resource

        Resources are objects matching a specific name. Each resource is a singleton
        for that name, so if you query for a specific resource name multiple times,
        you will get the same object.

        Resource objects are used to add and remove relations of properties to a resource.
        They can also be used as starting points for queries.

        ‼ Resources are created with ``miRdfRel.getAddRes`` (due to data integrity),
        or retrieved through a query.
        Trying to instansiate miRdfRel.Resource directly will trigger a warning.

        Manage resource
        ---------------

        Manage relations
        ----------------

        Manage properties
        -----------------

        Query relative paths
        --------------------
        */

        constructor (name:string, __internal_call_from_miRdfRel?:"only for internal use") {
            if (__internal_call_from_miRdfRel != "only for internal use") return miRdfRel.getAddRes(name)
            this.name = name;
        }

        /* $in:rst manage resource
        Get resource name (as string):

            ``Resource.``**toString**()
        */
        readonly name:string
        toString (): string {
            return this.name
        }
        private deleted:boolean = false
        isDeleted(): boolean {
            return this.deleted
        }
        // relations
        private _relTo: Record<string, Set<Resource>> = {}; // relation.name: set<Resource>
        private _relFrom: Record<string, Set<Resource>> = {};
        // for relation resources
        private _owl_inverseOf: Set<Resource> = new Set();
        private _rdfs_subproperties: Set<Resource> = new Set();
        // properties (strings)
        private _prop: Record<string, Set<string>> = {};

        /*
        For resources used as relation, find out related relations
        relation_res: <Resource> to use as base
        skip_inverse_check: <bool> - for recursive check
        return: {<str>} relations(include relation_res, {<str>} inv_relations
        
        inverse relations are created by adding owl:inverseOf (target relation) to
        a relation. It means that the relation is treated as an inverstion of the
        target relation, and if the target relation is looked for, this one can
        trigger in the oposit direction.
        */
        linkedRelations (skip_inverse_check:boolean): [Set<Resource>, Set<Resource>] {
            let relations:Set<Resource> = new Set();
            let inv_relations:Set<Resource> = new Set();

            relations.add(this);
            for (let sub_relation of this._rdfs_subproperties) {
                let subs = sub_relation.linkedRelations(skip_inverse_check);
                subs[0].forEach(r => relations.add(r));
                subs[1].forEach(r => inv_relations.add(r));
            }
            if (!skip_inverse_check) {
                for (let inv_relation of this._owl_inverseOf) {
                    let subs = inv_relation.linkedRelations(true);
                    subs[0].forEach(r => inv_relations.add(r));
                }
            }
            // console.debug('linkedRelations', relations, inv_relations);
            return [relations, inv_relations];
        }

        /* $in:rst manage resource
        To remove a resource:

           ``Resource.``**rm**``()``
        */
        public rm(): void {
            // decompose resource and triggering watchers
            this.deleted = true

            // trigger rmRelFrom watchers (through target res rmRelTo)
            for (let relName in this._relFrom) {
                let rel = miRdfRel.getRes(relName)
                if (!rel) throw 'BUG _relFrom must only contain defined relations'
                for (let res of this._relFrom[relName]) {
                    res.rmRelTo(rel, this);
                }
            }
            // this._watchersRmRelFrom = undefined;

            this._watchersAddRelTo.clear()
            for (let relation_name in this._relTo) {
                let relation_res = miRdfRel.getRes(relation_name) as miRdfRel.Resource;
                for (let relates_to of this._relTo[relation_name]) {
                    if (relates_to instanceof Resource) {
                        this.rmRelTo(relation_res, relates_to);
                    } else {
                        this.rmProp(relation_res, relates_to);
                    }
                }
            }
            // watchers prop - needs cleanup?

            this._watchersRm.trigger()
            this._watchersRm.clear()

            // cleanup watchers
            //this._watchersRmRelTo = undefined;
            // this._watchersProp = undefined;
            //this._watchersAddRelFrom = undefined;

            miRdfRel._rmRes(this)
        }
        /* $in:rst manage resource
        Watch if resource is removed:

            ``Resource.``**watchRm**``(onRemovedCb(resource name)) => unwatchCb()``
        */
        private _watchersRm = watcher.newWatcherGroup<void,void,string>(()=>[true, '', this.name])
        watchRm (onRemoved:watcher.onAddedCb<string>) {
            return this._watchersRm.watchAdd(undefined, onRemoved);
        }



        ///////////////////////////  RELATIONS  //////////////////////////////////////
        /* $in:rst manage relations
        Get specific relations

            ``Resource.``**relTo**``(relation) => [resource]``
        */
        relTo (rel:Resource): Set<Resource> {
            let out:Set<Resource> = new Set();
            if (!(rel instanceof Resource)) {
                throw `miRdfRel Resource.relTo relation ${rel} must be a resource`
            }

            let sub = rel.linkedRelations(false);
            let rel_superset = sub[0];
            let inv_superset = sub[1];

            for (let one_relation of rel_superset) {
                if (one_relation.name in this._relTo) {
                    this._relTo[one_relation.name].forEach(r=>out.add(r));
                }
            }
            if (inv_superset) {
                for (let one_inv_relation of inv_superset) {
                    if (one_inv_relation.name in this._relFrom) {
                        this._relFrom[one_inv_relation.name].forEach(r => out.add(r));
                    }
                }
            }
            return out;
        }
        relTo1 (rel:Resource): Resource|undefined {
            return this.relTo(rel).values().next().value
        }

        /* $in:rst manage relations
        Get specific relations from other resources

            ``Resource.``**relFrom**``(relation) => [resource]``
        */
        relFrom (relation:Resource): Set<Resource> {
            let superset = relation.linkedRelations(false);
            let rel_superset = superset[0];
            let inv_superset = superset[1];
            let resources_out:Set<Resource> = new Set();
            for (let one_relation of rel_superset) {
                if (one_relation.name in this._relFrom) {
                    this._relFrom[one_relation.name].forEach(r=>resources_out.add(r));
                }
            }
            for (let one_inv_relation of inv_superset) {
                if (one_inv_relation.name in this._relTo) {
                    this._relTo[one_inv_relation.name].forEach(r=>resources_out.add(r));
                }
            }
            return resources_out;
        }

        /* $in:rst manage relations
        Add a relation

            ``Resource.``**addRelTo**``(relation, Resource)``
        */
        addRelTo (rel:Resource, res:Resource): void {
            if (!(rel instanceof Resource)) {
                console.warn(`miRdfRel ${this.name} addRelTo relation must be a resource, not:`, rel)
                return
            }
            if (!(res instanceof Resource)) {
                console.warn(`miRdfRel ${this.name} addRelTo object must be a resource, not:`, res)
                return
            }
            let relation_name = rel.name;
            let resources = this._relTo[relation_name];
            if (!resources || !resources.has(res)) {
                if (!resources) {
                    resources = new Set();
                    this._relTo[relation_name] = resources;
                }
                resources.add(res);
                if (relation_name == 'rdfs:subPropertyOf') {
                    // rdfs:subPropertyOf -- check for loops
                    let sub = res.linkedRelations(false);
                    if (this in sub[0] || this in sub[1]) {
                        console.error(`Detected loop with rdfs:subPropertyOf for ${this}`);
                    } else {
                        res._rdfs_subproperties.add(this);
                    }
                } else if (relation_name == 'owl:inverseOf') {
                    this._owl_inverseOf.add(res);
                }

                let superset_relations = rel.linkedRelations(false);

                // inverse relation - can also trigger watchers
                res._addRelFrom(rel, superset_relations, this);

                let [sup_relations, inv_relations] = superset_relations;

                let actOn:watcher.triggerCbs = []
                // straight relationship watchers
                for (let one_relation of sup_relations) {
                    actOn.push(...this._watchersAddRelTo.getTriggered({add:true, relName:one_relation.name, objName:res.name}))
                }
                // inverse relations watchers
                for (let one_relation of inv_relations) {
                    actOn.push(...res._watchersAddRelFrom.getTriggered({add:true, relName:one_relation.name, objName:res.name}))
                }
                watcher.actOnTriggered(actOn)
            }
        }
        /* $in:rst manage relations
        Replace a relation

            ``Resource.``**replaceRel**``(relation, new Resource)``
        */
        replaceRelTo (rel:Resource, res:Resource): void {
            let already_set = false;
            for (let other_res of this.relTo(rel)) {
                if (other_res != res) {
                    this.rmRelTo(rel, other_res);
                } else {
                    already_set = true;
                }
            }
            if (!already_set) {
                this.addRelTo(rel, res);
            }
        }

        /* $in:rst manage relations
        Watch for new (and then removed) relations, trigger onAddedCb_ with the new target resource:

            ``Resource.``**watchAddRelTo**``(resource, onAddedCb(resource)) => unwatchcb()``
        */
        private _watchersAddRelTo = watcher.newWatcherGroup<_watchersAddRelToChange, string, Resource>(
            function(upd:_watchersAddRelToChange, matchRel:string) {
                if (upd.relName != matchRel && matchRel != '&') return undefined
                let obj = miRdfRel.getRes(upd.objName)
                console.assert(obj instanceof miRdfRel.Resource)
                if (upd.add) return [true, upd.objName, obj as miRdfRel.Resource] // added
                return [false, upd.objName] // removed
            },
        )
        public watchAddRelTo(rel:Resource|string, onAddedCb:watcher.onAddedCb<Resource>) {
            let relName = rel instanceof Resource ? rel.name : rel
            // find any existing relation to trigger for the new watcher
            let changes: _watchersAddRelToChange[] = []
            let relRes = miRdfRel.getRes(rel)
            if (relRes) { // relation exists, so check all relations
                for (let obj of this.relTo(relRes)) {
                    changes.push({
                        add: true,
                        relName: relName,
                        objName: obj.name,
                    })
                }
            }
            let unlinkCb = this._watchersAddRelTo.watchAdd(relName, onAddedCb, changes)
            return unlinkCb
        }

        /* $in:rst manage relations
        Watch for new relations, trigger onAddedCb_ with the new target resource:

            | ``Resource.``**watchAddRelTo**``(relation, onAddedCb(resource)) => unwatch()``
            | ``Resource.``**watchAddRelFrom**``(relation, onAddedCb(resource)) => unwatch()``
        */
        private _watchersAddRelFrom = watcher.newWatcherGroup<_watchersAddRelFromChange, string, Resource>(
            function(upd:_watchersAddRelFromChange, matchRel:string) {
                if (upd.relName != matchRel) return undefined
                let obj = miRdfRel.getRes(upd.objName)
                console.assert(obj instanceof miRdfRel.Resource)
                if (upd.add) return [true, upd.objName, obj as miRdfRel.Resource] // added
                return [false, upd.objName] // removed
            },
        )

        public watchAddRelFrom(rel:Resource|string, onAddedCb:watcher.onAddedCb<Resource>) {
            let relName = rel instanceof Resource ? rel.name : rel
            // find any existing relation to trigger for the new watcher
            let changes: _watchersAddRelToChange[] = []
            let relRes = miRdfRel.getRes(rel)
            if (relRes) { // relation exists, so check all relations
                for (let obj of this.relFrom(relRes)) {
                    changes.push({
                        add: true,
                        relName: relName,
                        objName: obj.name,
                    })
                }
            }
            let unlinkCb = this._watchersAddRelFrom.watchAdd(relName, onAddedCb, changes)
            return unlinkCb
        }

        /* $in:rst manage relations
        Remove a relation (if no resource is specificed, remove all resources)

            ``Resource.``**rmRelTo**``(relation, Resource?)``
        */
        rmRelTo (rel:Resource, optional_resource_res?:Resource): void {
            if (optional_resource_res) {
                this._rmRelTo(rel, optional_resource_res);
            } else {
                this.relTo(rel).forEach(res=>this._rmRelTo(rel, res));
            }
            // TODO this.__empty() && this.rm();
        }
        _rmRelTo (rel:Resource, res:Resource): void {
            // also this.addProp may trigger this
            let superset = rel.linkedRelations(false);
            let sup_relations = superset[0];
            let inv_relations = superset[1];

            let actOn:watcher.triggerCbs = []
            for (let one_relation of sup_relations) {
                let resources = this._relTo[one_relation.name]
                if (resources && resources.has(res)) {
                    res._rmRelFrom(one_relation, this)
                    resources.delete(res)
                    actOn.push(...this._watchersAddRelTo.getTriggered({add:false, relName:one_relation.name, objName:res.name}))
                }
            }
            for (let one_inv_relation of inv_relations) {
                let resources = this._relFrom[one_inv_relation.name]
                if (resources && resources.has(res)) {
                    // inverse relation - can also trigger watchers
                    actOn.push(...this._watchersAddRelFrom.getTriggered({add:false, relName:one_inv_relation.name, objName:res.name}))
                    res.rmRelTo(one_inv_relation, this);  // endless loop warning!!!
                }
            }
            watcher.actOnTriggered(actOn)
            // make sure also property watchers watching relations get triggered
            //this.__updatePropertyWatchers(rel.name);
        }

        _addRelFrom (rel:Resource, superset_relations:[Set<Resource>, Set<Resource>], fromRes:Resource) {
            // called from addRelTo on fromRes
            let resources = this._relFrom[rel.name];
            if (!resources || !resources.has(fromRes)) {
                if (!resources) {
                    resources = new Set();
                    this._relFrom[rel.name] = resources;
                }
                resources.add(fromRes);

                // trigger watchers
                let [sup_relations, inv_relations] = superset_relations

                let actOn:watcher.triggerCbs = []
                // straight relations watchers
                for (let one_relation of sup_relations) {
                    actOn.push(...this._watchersAddRelFrom.getTriggered({add:true, relName:one_relation.name, objName:fromRes.name}))
                }
                for (let one_relation of inv_relations) {
                    actOn.push(...this._watchersAddRelTo.getTriggered({add:true, relName:one_relation.name, objName:fromRes.name}))
                }
                watcher.actOnTriggered(actOn)
            }
        }
        _rmRelFrom (rel:Resource, res:Resource) {
            let resources = this._relFrom[rel.name];
            console.assert(resources && resources.delete(res),
                `_rmRelFrom not mirrored with rmRelTo for resource ${res}, ${rel}`);
            // inverse relation - _res one is always called from rmRelTo
            this._watchersAddRelFrom.trigger({add:false, relName:rel.name, objName:res.name})
        }



        /////////////////   PROPERTIES  ///////////////////////////

        /* $in:rst manage properties
        Get properties (a list of string values, not resources):

            ``Resource.``**prop**``(relation) => [string] ``
        */
        prop (prop:Resource): Set<string> {
            if (!(prop instanceof Resource)) {
                console.trace()
                throw `miRdfRel Resource.prop relation ${prop} must be a resource`;
            }
            let [rel_superset,_] = prop.linkedRelations(true);  // no inverse checks
            let out: Set<string> = new Set();
            for (let one_relation of rel_superset) {
                if (one_relation.name in this._prop) {
                    this._prop[one_relation.name].forEach(p=>out.add(p));
                }
            }
            return out;
        }
        prop1 (prop:Resource): string|undefined {
            return this.prop(prop).values().next().value
        }

        /* $in:rst manage properties
        Add a property:

            ``Resource.``**addProp**``(relation, string)``
        */
        addProp (prop:Resource, value:string): void {
            if (!(prop instanceof Resource)) {
                console.trace()
                throw `miRdfRel Resource.addProp relation ${prop} must be a resource`;
            }
            let values = this._prop[prop.name];
            if (!values || !values.has(value)) {
                if (!values) {
                    values = new Set();
                    this._prop[prop.name] = values;
                }
                values.add(value);
                let propVal = {prop:prop.name, val:value}
                this._watchersProp.trigger(propVal)
                this._watchersPropFilter.trigger(prop.name)
            }
        }
        /* $in:rst manage properties
        Replace all values of a property with a new one:

            ``Resource.``**replaceProp**``(relation, string)``
        */
        replaceProp (property:Resource, value:string): void {
            let already_set = false;
            for (let other_val of this.prop(property)) {
                if (other_val != value) {
                    this.rmProp(property, other_val);
                } else {
                    already_set = true;
                }
            }
            if (!already_set) {
                this.addProp(property, value);
            }
        }
        /* $in:rst manage properties
        Remove a property. Either a specific value or all for that property:

            ``Resource.``**rmProp**``(relation, string?)``
        */
        public rmProp (property:Resource, optionalValue?:string): void {
            if (optionalValue !== undefined) {
                this._rmProp(property, optionalValue);
            } else {
                let values = this.prop(property);
                for (let value of values) {
                    this._rmProp(property, value);
                }
            }
        }
        private _rmProp (prop:Resource, value:string): void {
            const values = this._prop[prop.name];
            if (values && values.delete(value)) {
                this._watchersProp.trigger({prop:prop.name, val:value})
                this._watchersPropFilter.trigger(prop.name)
            }
        }
        /* $in:rst Manage properties
        Trigger onAddedCb_ when a property value is added, and then if removed.

            ``Resource.``**watchProp**``(relation, onAddedCb({prop:string, val:string}) => cbUnwatch``
        */
        private _watchersProp: watcher.watcherGroup<propVal,string,string> = watcher.newWatcherGroup(
            function(change:propVal, matchVal:string):watcher.matcherReturn<string> {
                if (change.prop != matchVal) return undefined
                if (this._prop[change.prop].has(change.val))
                    return [true, `${change.prop} + ${change.val}`, change.val]
                return [false, `${change.prop} + ${change.val}`]
            }.bind(this))
        watchProp (prop:Resource|string, onAddedCb:watcher.onAddedCb<string>): watcher.unwatchCb {
            let propName = prop instanceof Resource ? prop.name : prop
            let changes: propVal[] = []
            if (propName in this._prop) {
                for (let value of this._prop[propName]) {
                    changes.push({prop:propName, val:value})
                }
            }
            let unwatchCb = this._watchersProp.watchAdd(propName, onAddedCb, changes)
            return unwatchCb
        }

        /* $in:rst manage properties
        Property filters - check if a resource has properties fulfilling a specific query.
        Check possible operators_. 

            ``Resource.``**propFilter**``(relation, operator, match string)``
        
        $in:rst Operators
        Criterias to filter a resource (resource/prop==val):

        :**`==`**: resource has a property/relation ``prop``, with value/resource ``val``
        :**`!=`**: resource don't have a property/relation ``prop`` with value/resource ``val``
        :**`=!`**: resource don't have any property/relation of type ``prop``
        :**`=*`**: resource has at least one property/relation of type ``prop``
        :**`--`**: ???
        :**`-=`**:
        :**`=+`**:
        :**`++`**:
        */
        public propFilter (prop:Resource|string, op:operator, matchVal:string): boolean {
            const propName = prop instanceof Resource ? prop.name : prop
            const values = this._prop[propName] || new Set()
            let propRes: Resource|undefined
            let matchRes: Resource|undefined
            switch(op) {
                case '--':
                    if (values.size == 0) return false
                    for (let value of values) if (value >= matchVal) return false
                    return true
                case '-=':
                    if (values.size == 0) return false
                    for (let value of values) if (value > matchVal) return false
                    return true
                case '+=':
                    if (values.size == 0) return false
                    for (let value of values) if (value < matchVal) return false
                    return true
                case '++':
                    if (values.size == 0) return false
                    for (let value of values) if (value <= matchVal) return false
                    return true
                case '=!':
                    if (values.size != 0) return false
                    propRes = miRdfRel.getRes(prop)
                    if (!propRes) return true
                    return this.relTo(propRes).size == 0
                case '=*':
                    if (values.size > 0) return true
                    propRes = miRdfRel.getRes(prop)
                    if (!propRes) return false
                    return this.relTo(propRes).size > 0
                case '!=':
                    if (values.has(matchVal)) return false
                    propRes = miRdfRel.getRes(prop)
                    if (!propRes) return true
                    matchRes = miRdfRel.getRes(matchVal)
                    if (!matchRes) return true
                    return !this.relTo(propRes).has(matchRes)
                case '==':
                    if (values.has(matchVal)) return true
                    propRes = miRdfRel.getRes(prop)
                    if (!propRes) return false
                    matchRes = miRdfRel.getRes(matchVal)
                    if (!matchRes) return false
                    return this.relTo(propRes).has(matchRes)
            }
        }
        /* $in:rst Manage properties
        Trigger onAddedCb_ when a property value is added.

            ``Resource.``**watchProp**``(relation, onAddedCb({prop:string, val:string}) => cbUnwatch``
        */

        /**
        Watch property filter. onAddedCb_ is called with the current resource
        (not property or match value) on a match. See operators_ for possible filters.

            ``Resource.``**watchProp**``(relation, operator, match string, onAddedCb(resource) => unwatch()``
        */
        private _watchersPropFilter = watcher.newWatcherGroup<string,propFilter,Resource>(
            function (change:string, matchValue:propFilter) {
                if (change != matchValue.prop) return undefined
                if (this.propFilter(matchValue.prop, matchValue.op, matchValue.val)) {
                    return [true, `${matchValue.prop}${matchValue.op}${matchValue.val}`, this]
                } else {
                    return [false, `${matchValue.prop}${matchValue.op}${matchValue.val}`]
                }
            }.bind(this))
        public watchPropFilter(prop:Resource|string, op:operator, value:string, onAddedCb:watcher.onAddedCb<Resource>): watcher.unwatchCb {
            let propName = prop instanceof Resource ? prop.name : prop
            let unwatchCb = this._watchersPropFilter.watchAdd({prop:propName, op:op, val:value}, onAddedCb, [propName])
            return unwatchCb
        }



        // ------------- PATH

        /* $in:rst query relative paths
        Follow a path from resource. Check what `miRdfRel Query Path`_:

            | ``Resource.``**path**``(path as string) => { resource|string }``
            | ``Resource.``**parsedPath**``(path) => { resource|string }``
        */
        path (pathStr:string): Set<Resource|string> {
            let path: miRdfRel.path = miRdfRel.pathFromString(pathStr);
            if ("absolute" in path && path.absolute == true) {
                let str = miRdfRel.pathToString(path)
                console.error(`miRdfRel.Resource.watchParsedPath Does not take absolute paths ${str}`)
                return new Set()
            }
            return this.parsedPath(path);
        }
        parsedPath (path:miRdfRel.pathRel|miRdfRel.pathFilter): Set<Resource|string> {
            let matches: Set<Resource|string> = new Set()
            let unwatch = this.watchParsedPath(path, function(match) {
                matches.add(match)
                return ()=>undefined
            })
            unwatch();
            return matches;
        }
        /* $in:rst query relative paths
        Watch relative path from resource. On a match onMatched is called (see onAddedCb_), Check `miRdfRel Query Path`_:

            | ``Resource.``**watchPath**``(path as string, onMatched(resource|string)) => unwatch()``
            | ``Resource.``**watchParsedPath**``(path, onMatched(resource|string)) => unwatch()``
        */
        watchPath (str:string, cbOnAdded:watcher.onAddedCb<Resource|string>): watcher.unwatchCb|undefined {
            let path = miRdfRel.pathFromString(str);
            if ("absolute" in path && path.absolute == true) {
                let str = miRdfRel.pathToString(path)
                console.error(`miRdfRel.Resource.watchParsedPath Does not take absolute paths ${str}`)
                return
            }
            return this.watchParsedPath(path, cbOnAdded);
        }

        watchParsedPath (
                    path:miRdfRel.pathRel|miRdfRel.pathFilter,
                    cbOnAdded:watcher.onAddedCb<Resource|string>)
                        : watcher.unwatchCb {

            // TODO: Watchers seem to fail in some circumstances
            // maybe f.ex. with paths like /..../rdfs:value++3 - create or update miRdfRel test case!
            let unwatchNextStep: watcher.unwatchCb|undefined = undefined
            // 2 unwatch for this step, can watch for rel and prop at the same time
            let unwatchThisStep1: watcher.unwatchCb
            let unwatchThisStep2: watcher.unwatchCb
            let collapse: {cb: watcher.onRemovedCb|undefined} = {
                // put remove callback in an object that is permantent within scope
                // note: set this exactly once below
                cb: function() { console.warn('BUG miRdfRel called onRemovedCb before it is set', miRdfRel.pathToString(path)) }
            }
            if ("only" in path) {
                console.assert(path.rest == undefined, "BUG path.only on non-last path part!")
                // watch last step in path
                let onLastMatch = function(match:Resource|string) {
                    collapse.cb = cbOnAdded(match)
                    return collapse.cb
                }
                // last step in path
                if ("op" in path) {  // Filter - match or not - only return resources
                    unwatchThisStep1 = this.watchPropFilter(path.res, path.op, path.val, onLastMatch)
                } else {
                    if (path.only != "val") { // rel || false
                        if ("inv" in path && path.inv) {
                            unwatchThisStep1 = this.watchAddRelFrom(path.res, onLastMatch)
                        } else {
                            unwatchThisStep1 = this.watchAddRelTo(path.res, onLastMatch)
                        }
                    }
                    if (path.only != "rel") { // val || false
                        unwatchThisStep2 = this.watchProp(path.res, onLastMatch)
                    }
                }
            } else {
                // watch next step in path
                let onStepMatch = function(res:Resource) {
                    unwatchNextStep = res.watchParsedPath(path.rest, cbOnAdded)
                    collapse.cb = function() { // on removed cb
                        if (unwatchNextStep) {
                            unwatchNextStep()
                            unwatchNextStep._collapse && unwatchNextStep._collapse.cb && unwatchNextStep._collapse.cb()
                        }
                    }
                    return collapse.cb
                }
                // next step in path
                if ("op" in path) {  // Filter - match or not
                    unwatchThisStep1 = this.watchPropFilter(path.res, path.op, path.val, onStepMatch)
                } else if (path.inv) {
                    unwatchThisStep1 = this.watchAddRelFrom(path.res, onStepMatch)
                } else {
                    unwatchThisStep1 = this.watchAddRelTo(path.res, onStepMatch)
                }
            }
            let unwatchCb = function() {
                // first unwatch next step
                unwatchNextStep && unwatchNextStep()
                // then this step filter/rel, val
                unwatchThisStep1 && unwatchThisStep1()
                unwatchThisStep2 && unwatchThisStep2()
            } as watcher.unwatchCb
            // use to remove any eventual match if a part in the path unmatch
            unwatchCb._collapse = collapse
            return unwatchCb
        }

        /* $in:rst manage resource
        Get a list of statements for this resource:

            | ``Resource.``**toStmts**``(includeRelatedFrom?)=>[``
            |    ``s:this resource``
            |    ``p:relation``
            |    ``o:resource|string``
            |    ``r:true if o is resource, false if it is a property value(a string) ]``
        */
        stmts (includeRelatedFrom?:boolean): stmt[] {
            // list relates to resources and properties
            let out: stmt[] = [];
            const thisName = this.toString()
            for (let propName in this._prop) {
                for (let val of this._prop[propName]) {
                    out.push({s:thisName, p:propName, o:val, r:false});
                }
            }
            for (let relName in this._relTo) {
                for (let res of this._relTo[relName]) {
                    out.push({s:thisName, p:relName, o:String(res), r:true});
                }
            }
            if (includeRelatedFrom) {
                for (let relName in this._relFrom) {
                    for (let res of this._relFrom[relName]) {
                        out.push({s:res.name, p:relName, o:thisName, r:true});
                    }
                }
            }
            return out;
        }
    }
}
