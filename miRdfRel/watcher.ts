/* general watchers handling for miRdfRel

$in:rst Watcher

A small library to hande watching and triggereing of data.
It is written and adapted for miRdfRel, but could be used outside of it.

Usage
-----

Start with creating a matcher function. Input to it is a value describing
a change of data, and a value to watch for. The value can be an ordinary
string or a more complex structure. The matcher function returns three different values:

* undefined if not applicable
* [true, key, what to send to onAddedCb_] for matches
* [false, key]

The ``key`` is a string used for differentiating matches. Usually a representation
of the change.

Secondly, create a watcher group with the matcher function:

  ``let watchergroup = watcher.``**newWatcherGroup**``(matcher)``

Then, to watch for a change:

  ``let unwatch = watcherGroup.``**addWatcher**``(onAddedCb)``

When any data is updated and matching, onAddedCb_ is called. It is called one tome
for each match, and also includes already existing values when the watcher is created:

  ``onAddedCb(result value) => onRemoveCb``

If a matching value is removed, the ``onRemoveCb`` from the onAddedCb is called,
without any argument.

  ``onRemoveCb()``

To stop watching, call the ``unwatch()`` function from when the watcher was created.

Note. A match can be inverse, like resource is missing relation, but usually they are
when someting is added.

*/

namespace watcher {

    /* in:rst Watcher
    unwatchCb
    ---------
    To stop watching for something. This callback is returned from each new watcher you make.
    */
    export type unwatchCb = {
        _collapse?: {cb?:() => void}
        (): void
    }
    export type onRemovedCb = () => void

    /* $in:rst Watcher
    onAddedCb
    ---------
    When a match is made, onAddedCb is called with a wathergroup specific value.
    The onAddedCb can(should) return a **onRemovedCb**. This method is called
    when it doesn't match anymore, f.ex. a relation is removed.
    */
    export type onAddedCb<addT> = (val:addT) => onRemovedCb|undefined

    export type watcher<matchT,addT> = {
        onAddedCb: onAddedCb<addT>
        onRemovedCb: Record<string,onRemovedCb>
        matchVal: matchT
    }
    export type triggerCbs = (() => void)[]
    export type matcherReturn<addT> = [true, string, addT]|[false, string]|undefined
    export type watcherGroup<changeT,matchT,addT> = {
        watchAdd: (matchVal:matchT, onAddedCb:onAddedCb<addT>, matchChanges?:changeT[]) => unwatchCb
        getTriggered: (change:changeT) => triggerCbs
        actOnTriggered: (triggered:triggerCbs) => void
        trigger: (change:changeT) => void,
        _set: Set<watcher<matchT,addT>>
        clear: () => void,
    }

    export let _watchersCount: number = 0

    /*  call each callback in a list of them
        separate trigger checks and acting on them
        to reduce risk of checking manipulated lists */
    export function actOnTriggered(triggered:triggerCbs) {
        triggered.forEach(cb => cb())
    }

    /* Create a watcher group that is used for storing
       callbacks and call them when a matching change occurs
    */
    export function newWatcherGroup<changeT,matchT,addT>(
                matcher:(change:changeT,matchVal:matchT) => matcherReturn<addT>,
                ): watcherGroup<changeT,matchT,addT> {

        type thisWatcher = watcher<matchT,addT>

        let set:Set<thisWatcher> = new Set();

        /* check a watcher against a change, return trigger callback if it matches (add or remove) */
        let getTriggerCbForWatcher = function(change:changeT, watcher: thisWatcher): {():void}|undefined {
            let match = matcher(change, watcher.matchVal)
            if (match) {
                let [matching, key, value] = match
                if (matching == true) {
                    if (!(key in watcher.onRemovedCb)) {
                        return function() {
                            watcher.onRemovedCb[key] = watcher.onAddedCb(value as addT) as onRemovedCb
                        }
                    }
                } else {
                    if (key in watcher.onRemovedCb) {
                        let cb = watcher.onRemovedCb[key]
                        delete watcher.onRemovedCb[key]
                        return cb
                    }
                }
            }
        }
        
        /* check all watchers against a change, return trigger callbacks for each matching (add or remove) */
        let getTriggered = function(change:changeT): triggerCbs {
            // must be followed by actOnTriggered!
            let triggered: {():void}[] = []
            for (let watcher of set) {
                let triggerCb = getTriggerCbForWatcher(change, watcher)
                triggerCb && triggered.push(triggerCb)
            }
            return triggered
        }

        /* get list of triggers matching a change and act on them */
        let trigger = function(change:changeT) {
            actOnTriggered(getTriggered(change))
        }

        /* remove all watchers from group */
        let clear = function() {
            set.clear()
        }

        /* add a new watcher that will be triggered on a change matching it,
           optionally send with a change to trigger it imideatly */
        let watchAdd = function(matchVal:matchT, onAddedCb:onAddedCb<addT>, matchChanges?:changeT[]): unwatchCb {
            ++_watchersCount
            let w = {
                matchVal: matchVal,
                onAddedCb: onAddedCb,
                onRemovedCb: {},
            } as watcher<matchT,addT>
            set.add(w)
            for (let change of matchChanges || []) {
                let cb = getTriggerCbForWatcher(change, w)
                cb && cb()
            }
            return function() {
                --_watchersCount
                set.delete(w);
            }
        }
        return {
            watchAdd: watchAdd,
            getTriggered: getTriggered,
            actOnTriggered: actOnTriggered,
            trigger: trigger,
            _set: set,  // for debugging
            clear: clear,
        }
    }
}
