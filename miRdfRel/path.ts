/// <reference path="definitions.ts" />

namespace miRdfRel {
    /*
    $in:rst miRdfRel Query path

    For a bit more complex queries, like multiple relations, or filtering out certain resources,
    a query path is recommended. Instead of checking each step maunaully, it follows multiple
    steps directly.

    ‼ A query path is a bit heavier than simpler requests, but much easier to handle.

    path steps
    ----------
    Steps are separated with '/'.

    Each step can be either a filter or a relation.

    The first step can also be a resurce, an absolute path, if the path is used directly from miRdfRel
    and not on a resource.

    A relation is f.ex. ``someone/foaf:knows``. It can also be a check on relations back to resource,
    ``music_genre/-dbp:genre``

    A filter is build up by property, operator and value, f.ex. ``rdfs:label==Tjoho``. See operators_

    End of path; If the path ends with a '/' it means only return relations. If it ends with '=' it
    means only properties (string values).

    Operators
    ---------
    */

    export type path = pathAbsMore|pathRelMore|pathFilterMore|pathAbsLast|pathRelLast|pathFilterLast
    type _pathMore = {
        res:string
        rest:pathRel|pathFilter
    }
    type _pathLast = {
        res:string
        rest:undefined
        only:"rel"|"val"|false
    }
    type _pathAbs = {
        absolute: true
    }
    type _pathRel = {
        absolute: false
        inv: boolean
    }
    type _pathFilt = {
        op:operator
        val:string
        dotval:boolean
    }
    export type pathAbs = pathAbsMore|pathAbsLast
    type pathAbsMore = _pathMore & _pathAbs
    type pathAbsLast = _pathLast & _pathAbs
    export type pathRel = pathRelMore|pathRelLast
    type pathRelMore = _pathMore & _pathRel
    type pathRelLast = _pathLast & _pathRel
    export type pathFilter = pathFilterMore|pathFilterLast
    type pathFilterMore = _pathMore & _pathFilt
    type pathFilterLast = _pathLast & _pathFilt

    // Make sure to match operators in resource.ts
    let _reasoner_operator_search = /(.+)(--|-=|==|=\*|\+=|\+\+|\!=|=\!|=\.)(.*)/;

    function _pathFromString (parts:string[], first?:true): path {
        // console.debug('miRdfRel._pathFromString', parts);
        if (parts.length == 0) throw `miRdfRel _pathFromString called with empty path`
        let part = parts.shift() as string
        if (first && !part) {
            // absolute - starts with /
            if (parts.length == 0) throw `Path with only a slash`
            part = parts.shift() as string
            if (_reasoner_operator_search.exec(part)) {
                throw `Absolute path cannot start with a filter; ${part} ${parts}`
            }
            return _pathFromStringRest(part, parts, {
                absolute: true,
            }) as pathAbsMore|pathAbsLast
        }
        if (!part.length) {
            // empty step: // or missed last step: / should never occur
            throw `Path cannot contain double //; ${part} ${parts}`
        }
        let match = _reasoner_operator_search.exec(part)
        if (match) {
            if (part[0] == '-')
                throw `Path inverse pred cannot be a filter ${part} ${parts}`
            // filter
            if (match[2] == '=.') {
                // dotval
                return _pathFromStringRest(match[1], parts, {
                    op: '==',
                    dotval: true,
                    val: '.' + match[3],
                })
            } else {
                // filter value
                return _pathFromStringRest(match[1], parts, {
                    op: match[2] as operator,
                    dotval: false,
                    val: match[3],
                })
            }
        } else {
            // relation
            let inv: boolean
            if (part[0] == '-') {
                part = part.slice(1)
                inv = true
            } else {
                inv = false
            }
            return _pathFromStringRest(part, parts, {
                absolute: false,
                inv: inv,
            })
        }
    }
    function _pathFromStringRest(
            pred:string, rest:string[],
            pathBase:_pathAbs|_pathRel|_pathFilt
            ):path {
        // console.debug('miRdfRel._pathFromStringRest', rest, rest.length, rest[0])
        if (rest.length == 0) {
            // Last path entry
            if (pred.slice(-1) == '=') {
                // last path entry, only predicates (....=)
                return {
                    ...pathBase,
                    res: pred.slice(0, -1),
                    only: "val",
                } as pathRelLast|pathFilterLast
            } else {
                //
                return {
                    ...pathBase,
                    res: pred,
                    only: false,
                } as pathRelLast|pathFilterLast
            }
        } else if (rest.length == 1 && rest[0]=='') {
            // last path entry, only relations (..../)
            return {
                ...pathBase,
                res: pred,
                only: "rel",
            } as pathRelLast|pathFilterLast
        } else {
            return {
                ...pathBase,
                res: pred,
                rest: _pathFromString(rest),
            } as pathFilterMore|pathRelMore
        }
    }

    /* $in:rst Query absolute paths

    Convert between a string and a query path. Internally
    query paths (or parsed paths) are used:

        | ``miRdfRel.``**pathFromString**``(string) => path``
        | ``miRdfRel.``**pathToString**``(path) => string``
    */

    export function pathFromString (str: string): path {
        if (!str)
            throw `pathFromString called with empty path`
        const parts: string[] = str.split("/");
        return _pathFromString(parts, true);
    }

    export function pathToString (path: path): string {
        let out = '';
        if (path === undefined)
            return ''
        else if ("op" in path) {
            // filter
            out += _pathToStringFilter(path);
        } else {
            out += _pathToStringRelation(path);
        }
        if (path.rest)
            out += '/' + pathToString(path.rest);
        return out
    }
    function _pathToStringFilter (path:pathFilterMore|pathFilterLast): string {
        let out = path.res;
        if (path.dotval)
            out += '=' + path.val;
        else
            out += path.op + path.val;
        return out;
    }
    function _pathToStringRelation (path:pathAbsMore|pathRelMore|pathAbsLast|pathRelLast): string {
        let out = '';
        if (path.absolute == true)
            out += '/';
        else if (path.inv)
            out += "-";
        out += path.res;
        if ("only" in path) {
            if (path.only == "rel") out += '/';
            if (path.only == "val") out += '=';
        }
        return out;
    }

}
