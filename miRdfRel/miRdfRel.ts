/*
$doc:rst miRdfRel

miRdfRel - RDF_-data storage, for tripplets and relations with 
methods to watch for specific changes to the data.

See RDF_ for an introduction to data stored as relations in triplets.

You can describe relations like::

   Martin knows Agnes
   My song is in genre Folk

And query or watch for changes::

    Show number of all Martins friends
    List all songs of a specific genre, and add new ones if they appear

The features are:

* resources (subjects and predicates) are objects (a class)
* manage relations between resources (a relation from one resource to another)
* manage properties of resources (a relation to a string value)
* query relations to  and from (inverse relations) a resource
* watch for changes to resources, relations and properties
* query and watch paths (check `miRdfRel Query Path`_).

miRdfRel does not require each resource to be prefixed. Usually they
are checked when using f.ex. miMirdf reader.

The reasoner is working with some standard RDF namespaces and semantic.
'a' is used instead of typeof, and the reasoner somewhat understands
**owl:inverseOf**, **rdfs:subPropertyOf**

And how to use it
'''''''''''''''''

List names of friends::

    let martin = miRdfRel.getAddRes('mi:Martin')
    let names = new Set()
    martin.relTo('foaf:knows').forEach(r=>r.prop('rdfs:label').forEach(p=>names.add(p)))
    // or with a path
    names = martin.path('foaf:knows/rdfs:label')  // {'Agnes', 'Anders', 'Simon'}

List songs by genre, omitting Christmas songs::

    miRdfRel.path('/dbo:MusicGenre/-a/rdfs:label!=Christmas music/-dbo:genre')
    // From left: list all music genres,
    // skip those labeled "Christmas music",
    // find songs (or other things), with these genres

miRdfRel main module
====================

miRdfRel Resource
=================

miRdfRel Query path
===================

Watcher
=======
*/
/// <reference path="definitions.ts" />
/// <reference path="resource.ts" />
/// <reference path="path.ts" />
/// <reference path="watcher.ts" />

namespace miRdfRel {
/*
namespace miRdfRel include by either:
include in code: /// <reference path="mi.rdf.rel"/>
or compile as module and use as: import { miRdfRel } from "./miRdfRel.ts"

$in:rst miRdfRel main module

Starting point to retrieve resources, query paths and some helper functions.

The dataset is global and automatically initiated, so if one part of the
program stores anything, other parts will also be able to use that data.

Manage resources
''''''''''''''''

Query absolute paths
''''''''''''''''''''
For description of paths, see `miRdfRel Query Path`_.

watch
'''''
*/

    // connection between resrouce name and resource - also to keep resources unique
    let _resources: Record<string, Resource> = {}

    let _watchersAdd = watcher.newWatcherGroup<string,string,Resource>(
        function(changedResName:string, watchResName:string) {
            if (changedResName != watchResName) return
            if (exists(watchResName)) return [true, watchResName, _resources[watchResName]]
            return [false, watchResName]
        },
    );
    /* $in:rst manage resources
    Get resource (or undefined if it does not exist):

        ``miRdfRel.``**getRes**``(resource name)``
    */
    export function getAddRes (name:string) {
        let res:Resource|undefined = getRes(name);
        if (!res) {
            res = new miRdfRel.Resource(name, "only for internal use")
            _resources[name] = res
            _watchersAdd.trigger(name)
        }
        return res;
    }
    /* $in:rst manage resources
    Get resource (add it if it does not exist):

        ``miRdfRel.``**getAddRes**``(resource name)``
    */
    export function getRes(name:string|Resource): Resource|undefined {
        if (isRes(name))
            return name as Resource;
        return _resources[name as string]
    }

    /* $in:rst manage resources
    Remove resource:

        ``miRdfRel.``**rmRes**``(Resource | resource name)``
    */
    export function rmRes(res:Resource|string) {
        let resChecked = getRes(res)
        if (resChecked) resChecked.rm()
    }
    export function _rmRes(res:Resource) {
        delete _resources[res.name]
        _watchersAdd.trigger(res.name)
    }

    /* $in:rst manage resources
    List all resources:

        ``miRdfRel.``**listRes**``() => [Resource]``
    */
    export function listRes (): Resource[] {
        return Object.values(_resources)
    }

    /* $in:rst manage resources
    Check if an object is a resource:

        ``miRdfRel.``**isRes**``(Object) => boolean``
    */
    export function isRes (something:any): boolean {
        return something instanceof miRdfRel.Resource
    }

    /* $in:rst manage resources
    Check if a resource is defined:

        ``miRdfRel.``**exists**``(resource name) => boolean``
    */
    export function exists (name:string): boolean {
        return (name in _resources)
    }    
    /* $in:rst watch
    When the resource is added, onAddedCb_ is called. Call unwatch
    to stop watching:

        ``miRdfRel.``**watchRes**``(resource name, onAddedCb(Resource)) => unwatch()``
    */
    export function watchRes (
            name:string,
            cbOnAdded:watcher.onAddedCb<Resource>): watcher.unwatchCb {
        let unwatch = _watchersAdd.watchAdd(name, cbOnAdded, [name])
        return unwatch
    }


    /* $in:rst query absolute paths
    Query resources and values matching a path:

        | ``miRdfRel.``**path**``(string) => [Resource | string]``
        | ``miRdfRel.``**parsedPath**``([path]) => [Resource | string]``
    */
    export function path(pathStr:string) : Set<string|Resource> {
        let path = miRdfRel.pathFromString(pathStr);
        if (!("absolute" in path) || !path.absolute) {
            let str = miRdfRel.pathToString(path)
            console.error(`miRdfRel.*Path First element must be a resource; ${str}`)
            return new Set()
        }
        return parsedPath(path);
    }
    export function parsedPath(path:miRdfRel.pathAbs) {
        let matches: Set<Resource|string> = new Set();
        let unwatch = watchParsedPath(
            path,
            function (match:Resource|string): () => void { matches.add(match); return function() {} });
        unwatch && unwatch();
        return new Set(matches);
    }
    /* $in:rst watch
    When an entry matches the path, onAdded is called. Use unwatch to stop watching.

        | ``miRdfRel.``**watchPath**``(string, onAdded(Resource)) => unwatch()``
        | ``miRdfRel.``**watchParsedPath**``([path], onAdded(Resource)) => unwatch()``
    */
    export function watchPath (
            pathStr:string,
            cbOnAdded:watcher.onAddedCb<Resource|string>): watcher.unwatchCb|undefined {
        let path = miRdfRel.pathFromString(pathStr);
        if (!("absolute" in path) || !path.absolute) {
            let str = miRdfRel.pathToString(path)
            console.error(`miRdfRel.*Path First element must be a resource; ${str}`)
            return
        }
        return watchParsedPath(path, cbOnAdded);
    }
    export function watchParsedPath (
            path:miRdfRel.pathAbs,
            cbOnAdded: watcher.onAddedCb<Resource|string>): watcher.unwatchCb {
        let nextUnwatch: watcher.unwatchCb|undefined;
        let cbPathOnResourceAdded: (res:Resource) => watcher.unwatchCb;
        if (path.rest) {
            cbPathOnResourceAdded = function(matchingRes) {
                nextUnwatch = matchingRes.watchParsedPath(path.rest, cbOnAdded);
                return function() {}
            }
        } else {
            cbPathOnResourceAdded = function(matchingRes) {
                cbOnAdded(matchingRes);
                nextUnwatch = undefined;
                return function() {}
            }
        }
        let thisUnwatch = _watchersAdd.watchAdd(path.res, cbPathOnResourceAdded, [path.res])
        return function() {
            nextUnwatch && nextUnwatch();
            thisUnwatch();
        }
    }

    /* $in:rst manage resources
    list all statements, for debugging and export:

        ``miRdfRel.``**stmts**``() => [ Resource, Resource, Resource | string ]``
    */
    export function stmts(): stmt[] {
        let stmts: stmt[] = [];
        for (let resource of listRes()) {
            stmts.push(...resource.stmts());
        }
        return stmts;
    }

}
