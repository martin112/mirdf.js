namespace miRdfRel {

    export type operator = '--'|'-='|'=='|'+='|'++'|'=!'|'=*'|'!='

    export type stmt = {
        s: string
        p: string
        o: string
        r: boolean
    }    

    export type propVal = {prop:string, val:string}
    export type propFilter = {prop:string, val:string, op:operator}

}
