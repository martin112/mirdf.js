$doc:rst miRdf.js

A collection of libraries to create dynamic web pages based on RDF_.

The libraries are made in typescript. Javascript builds can be found
under build/.

miRdfa template engine
''''''''''''''''''''''
miRdfa_ is a template engine to create dynamic html pages based on
templates with tag attributes similar to rdfa. The library converts
RDF_ to an hierarchical html page. The intention is to be able to use
the same data format the whole way from server data storage to client
template engine.

  * ``build/miRdfa.js``

miRdfRel data storage
'''''''''''''''''''''
miRdfRel_ is a library to store data in RDF_ on a client. It is
heavily used by miRdfa_.

You can use it to store and retrieve resources and their relations and
properties. It has a query language to follow multiple relations and
filter on properties. And it is possible to add watchers for changes
that trigger when matching data is added or removed.

  * ``build/miRdfRel.js``

miRdFormat data format
''''''''''''''''''''''
miRdFormat_ is a file/data format to store or transfer data in rdf.

It can be used to store a graph, or express changes or queries.

  * ``build/miRdFormat3.js``

RDF
'''
RDF is a format based on tripplets and their relations, subject,
predicate, object. Check out https://www.w3.org/RDF/.

Subject and predicate is each a resource, a unique identifier.
Object, may be a resource or a value of some kind (these libraries
use only strings).

The resources are placed in namespaces ad looks like
http://www.w3.org/2000/01/rdf-schema#label. Common practice
is to replace the namespace with a prefix, in this case ''rdfs:label''.

The resources in these libraries are always defined as prefixed entries.

There are standard namespaces (rdf, rdfs, dcp, etc) trying to
standardise definitions so it is possible to join different data sets.
Check f.ex. https://schema.org/.

.. code:: mirdf
  mi:Martin
    a rdfs:Class
    rdfs:label "Martin
    foaf:knows mi:Agnes

  mi:Agnes
    a rdfs:Class
    rdfs:label "Agnes


Development
'''''''''''
The libraries are written in typescript.

Documentation written in Doc3n.

Check Ideas_ for thoughts on improvements.
