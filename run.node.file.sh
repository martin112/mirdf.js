#!/bin/sh
#
# compile and run file with node
# save builds in build/-folder
target="build/$1.js"
mkdir -p "$(dirname "$target")"
tsc "$1" -t es2017 --module amd --sourceMap true --outfile "$target" && node "$target"
