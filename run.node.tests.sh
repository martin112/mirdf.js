#!/bin/sh
# Run all test suites

if [ -n "$1" ]
then
    ./run.node.file.sh "$1"
else
    for f in tests/*.ts
    do
        echo "** $f **"
        ./run.node.file.sh "$f"
    done
fi

