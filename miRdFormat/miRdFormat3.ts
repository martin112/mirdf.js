/// <reference path="../miRdfRel/miRdfRel.ts"/>
/// <reference path="miPrefixes.ts"/>
/*
$doc:rst MiRdFormat

Tools to import and export mirdf data, including prefix management.

miRdFormat
----------
The mirdf3-format can be used to exchange triples, including diffs.
All resources (subjects and objects) are written in their short form,
with {prefix}:{name} (see Prefixes_).

Basic forms are::

    subject predicate object
    subject predicate "literal
    subject
      predicate object
      prediacte "literal
      subject predicate
    "multiline
    "literal

It is also possible to add an action. Acctions are placed before the object or literal::

    subject predicate - "literal
    subject
      predicate =object

miRdFormat supports:

* Add: "+": Append the statment. This is the default action.
* Remove "-": Remove the statement. Object or literal can be omitted.
* Set "=": Replace any statement.

The header of a stream is:

    # mirdf3:version "3

And prefixes (needs to be defined before used) are written as::

    {prefix} mirdf:pre "{iri}
    rdf mirdf:pre "http://www.w3.org/1999/02/22-rdf-syntax-ns#'

miRdf3parse
-----------

miRdf3stringify
---------------

Prefixes
--------
*/

namespace miRdFormat {
    /*
    $in:rst miRdf3parse

    Reading lines in miRdf3-format, and call two callbacks on each complete statement.

    The callbacks are called with a statement (subject, predicate, object). For remove
    statements, the object may be undefined.

    .. code:: javascript

        miRdf3parse(lines, callbackAdd, callbackRemove)

    */
    export const HEADER_MIRDF3 = '# mirdf:version "3'
    export const DEFAULT_ACTION = "+"

    class Res {
        readonly name: string
        constructor(name: string) {
            this.name = name
        }
        toString(): string {
            return this.name
        }
    }
    type Distinct<T, DistinctName> = T & { __TYPE__: DistinctName };
    type Subj = Distinct<Res, 'subject'>
    type Pred = Distinct<Res, 'predicate'>
    type Obj = Distinct<Res|Readonly<string>, 'object'>
    type Act = "+"|"-"|"="
    type Prefix = Distinct<string, 'Prefix'>

    type CallbackAddStmt = (subj: Subj, pred: Pred, obj: Obj) => void
    type CallbackRmStmt = (subj: Subj, pred: Pred, obj?: Obj) => void

    export function miRdf3parse(lines: string[], cbAdd: CallbackAddStmt, cbRm?: CallbackRmStmt, defaultPre?: string) {
        let lineNr = 0;
        let outS: string|undefined
        let outP: string|undefined
        let outO: string|undefined
        let outV: string|undefined
        let outA: Act|undefined

        let replacePre: Record<string, string> = {}

        let statusDefaultPre: 'not tested'|'failed'|'ok' = 'not tested'

        const fixRes = (rawRes: string): Res => {
            if (rawRes.startsWith('#')) {
                if (statusDefaultPre === 'not tested') {
                    if (defaultPre === undefined) {
                        console.warn(`Using "#" without defining default prefix`)
                        statusDefaultPre = 'failed'
                    } else {
                        // log warning if the default prefix does not exist
                        let iri = miPrefixes.getIri(defaultPre)
                        if (iri !== undefined) {
                            statusDefaultPre = 'ok'
                        } else {
                            statusDefaultPre = 'failed'
                        }
                    }
                }
                let prefix: string
                let name = rawRes.slice(1)
                if (statusDefaultPre === 'ok') {
                    prefix = defaultPre + ':'
                } else {
                    prefix = ':'
                }
                return new Res(`${prefix}${name}`)
            } else {
                let split = miPrefixes.expand(rawRes)
                let pre = replacePre[split.prefix] || split.prefix
                return new Res(`${pre}${split.name}`)
            }
        }

        const commitStmt: () => void = () => {
            if (outP) {
                if (!outS || !outP || (!outO && outV === undefined && outA !== '-')) {
                    console.error(`Script error stmt not complete line ${lineNr}: ${outS} ${outP} ${outA} ${outO}`)
                } else if (outP == 'mirdf:pre') {
                    if (!outV) {
                        console.warn(`Missing value for prefix on line ${lineNr}: ${outS} ${outO} ${outV}`)
                    } else {
                        miPrefixes.define(outS, outV)
                    }
                } else {
                    let s = fixRes(outS) as Subj
                    let p = outP == 'a' ? fixRes('rdf:type') as Pred : fixRes(outP) as Pred
                    let o = (outO ? fixRes(outO): outV) as Obj
                    switch(outA) {
                        case '-':
                            cbRm && cbRm(s, p, o)
                            break
                        case '=':
                            cbRm && cbRm(s, p, o)
                            cbAdd(s, p, o)
                            break
                        case '+':
                            cbAdd(s, p, o)
                            break
                        default:
                            cbAdd(s, p, o)
                    }
                }
            }
            outP = undefined
            outO = undefined
            outV = undefined
            outA = undefined
        }

        /*  Regexp matchinging all lines in mirdf
            line:
                <subj> <pao>
                    <pao>
                "<val>
            pao:
                <pred> <act> <ov>
                <pred> <act><ov>
            ov:
            <obj>
            "<val>
        */
        const lineRe = /^"(?<valline>.*)|(?<subj>[^ ]+)?( +(?<pred>[^ ]+)( +(?<act>[\-+=])?( *(("(?<valobj>.*))|(?<obj>[^\s]+))?))?)?/
        const trimEnd = /\s+$/
        for (const lineIn of lines) {
            lineNr += 1
            const line = lineIn.replace(trimEnd, "")

            if (lineNr == 1) {
                if (line !== HEADER_MIRDF3) {
                    console.error(`Wrong header, must be ${HEADER_MIRDF3} was ${lines[0]}`)
                }
                continue
            }

            if (!line) {
                commitStmt()
                continue
            }
            const match = lineRe.exec(lineIn)
            const groups = match?.groups
            if (!groups) {
                console.error(`Cannot parse line ${lineNr}: ${lineIn}`)
            } else if (groups['valline']) {
                if (outV !== undefined) {
                    outV = `${outV}\n${groups['valline']}`
                } else {
                    outV = groups['valline']
                }
            } else {
                commitStmt()
                if (groups['subj']) {
                    outS = groups['subj']
                }
                if (groups['pred']) {
                    outP = groups['pred']
                    outA = (groups['act']) as Act
                    if (groups['obj']) {
                        outO = groups['obj']
                    } else {
                        outV = groups['valobj']
                    }
                }
            }
        }
        commitStmt()
    }

    export function miRdf3parseRdfRel(lines: string[], defaultPre?: string) {
        miRdf3parse(lines,
            (inSubj: Subj, inPred: Pred, inObj: Obj)=>{
                const subj = miRdfRel.getAddRes(inSubj.name)
                const pred = miRdfRel.getAddRes(inPred.name)
                if (inObj instanceof Res ) {
                    const obj = miRdfRel.getAddRes(inObj.name)
                    subj.addRelTo(pred, obj)
                } else {
                    subj.addProp(pred, inObj)
                }
            },
            (inSubj: Subj, inPred: Pred, inObj: Obj|undefined)=>{
                const subj = miRdfRel.getAddRes(inSubj.name)
                const pred = miRdfRel.getAddRes(inPred.name)
                if (inObj instanceof Res) {
                    const obj = miRdfRel.getAddRes(inObj.name)
                    subj.rmRelTo(pred, obj)
                } else if (inObj) {
                    subj.rmProp(pred)
                } else {
                    subj.rmRelTo(pred)
                    subj.rmProp(pred)
                }
            },
            defaultPre)
    }

    /*
    $in:rst miRdf3stringify

    Convert a list of statements to mirdf format.

    It takes a list of statements,
    Each statement containing ``(subject, predicate, object|literal|undefined, action|undefined]``.

    .. code:: javascript

        mirdf3stringify([
            [subj, pred, obj],
            [subj, pred, obj, '+'],
            [subj, pred, undefined, '-'],
        ])
    */
    export function mirdf3stringify(
            stmts: [Subj|miRdfRel.Resource, Pred|miRdfRel.Resource, string|Obj|miRdfRel.Resource, Act|undefined][]): string {
        let outHeader: string = HEADER_MIRDF3 + '\n'
        let outStmts: string = ''
        let lastSubj: Res|miRdfRel.Resource|undefined = undefined
        let outedPrefixes: Set<Prefix> = new Set()
        const fixPre = (inRes: Res|miRdfRel.Resource): Res => {
            let res: Res
            if (inRes instanceof Res) res = inRes
            else res = new Res(inRes.name)
            const split = miPrefixes.expand(res.name)
            if (!outedPrefixes.has(split.prefix)) {
                outedPrefixes.add(split.prefix)
                outHeader += `\n${split.prefix} mirdf:pre "${split.iri}`
            }
            return res
        }
        for (const stmt of stmts) {
            const [inSubj, inPred, inObj, act] = stmt
            const subj = fixPre(inSubj)
            const pred = fixPre(inPred)
            let obj: Res|string|undefined
            if (inObj && typeof inObj != "string") obj = fixPre(inObj)
            else obj = inObj
            if (!lastSubj || subj.name != lastSubj.name) {
                    lastSubj = subj
                    outStmts += `\n${subj}\n`
            }
            if (obj === undefined) {
                if (act == '-') {
                outStmts += `  ${pred} ${act} ${obj}\n`
                } else {
                console.error(`Stmt ${stmt} is missing an object`)
                }
            }
            if (obj instanceof Res) {
                outStmts += `  ${pred} ${act} ${obj}\n`
            }
            else if (typeof obj === "string") {
                const objLines = obj.split('\n')
                if (objLines.length > 1) {
                outStmts += '\n"' + objLines.join('"\n')
                } else {
                outStmts += `  ${pred} ${act} "${obj}\n`
                }
            }
            else {
                console.assert(obj === undefined, `Obj ${obj} is of unknown type ${typeof obj}`)
            }
        }
        return outHeader + outStmts
    }
}
