
namespace miPrefixes {
    /*
    $in:rst MiRdFormat
    Prefixes
    ========
    Namespace containing methods to manage prefix and iri for resources.

    Resources are managed in their short form as {prefix}:{name}, but when
    importing and exporting we need to make sure that the **prefix** matches
    the correct iri.

    Prefixes contains a map between prefixes and their iris (long name). This
    is build by mirdf:pre tags when reading the mirdf-files, or by defining
    them manually.

    Some known prefixes, with an example value and their iri:

    * **rdf:** ``http://www.w3.org/1999/02/22-rdf-syntax-ns#``

      Foundation for RDF, f.ex. ``rdf:type`` often replaced by "a".

    * **foaf:** ``http://xmlns.com/foaf/0.1/``
      
      Human relations (friend of a friend)
    
    * **schema:** ``https://schema.org/``
      
      Seach engine collaboration to describe things on internet
      f.ex. ``schema:Photograph`` (``https://schema.org/Photograph``)

    */
    type Distinct<T, DistinctName> = T & { __TYPE__: DistinctName };
    type Prefix = Distinct<string, 'Prefix'>
    type IRI = Distinct<string, 'IRI'>

    const prefixIri: Record<string, string> = {}
    const iriPrefix: Record<string, string> = {}

    export function define (prefix: string, iri: string): string {
        /*  $in:rst Prefixes
            define
            ======
            ``Prefixes.define(prefix, iri)``
            Connect a prefix with an iri.

            TODO: manage or warn when they do not match
        */
        //let curIri = prefixIri[prefix]
        //let curPre = iriPrefix[iri]
        if (prefix.endsWith(':')) {
            prefix = prefix.slice(0, -1)
        }
        if (!prefix.match(/^[\w\d_]+$/)) {
            console.warn('Prefix must only contain letters, digits or _, was', prefix)
        }
        prefixIri[prefix] = iri
        iriPrefix[iri] = prefix
        // TODO: if prefix and iri doesnt match current, what do we do?
        // create a new prefix and return that one so import can keep a map between
        // file prefixes and prefixes in current session.
        return prefix
    }
    export function getPrefix (iri: string): string|undefined {
        return iriPrefix[iri]
    }
    export function getIri(prefix: string): string|undefined {
        return prefixIri[prefix]
    }
    export function expand(res: string): {iri: IRI, prefix: Prefix, name: string, long: string} {
        /*  $in:rst Prefixes
            expand
            ======
            ``Prefixes.expand(res): {iri, prefix, name}``
            Convert from short form to an object with details of each part.
        */
        const [prefix_, name] = String(res).split(':', 2)
        const prefix = prefix_ + ':'
        const iri = getIri(prefix_)
        if (!iri) {
            console.warn(`Unknown prefix ${prefix}`)
        }
        return {iri: iri as IRI, prefix: prefix as Prefix, name: name, long: iri + name}
    }
    /*export function shorten(iri: IRI, res: Res): Res {
      return res
    }*/
}
